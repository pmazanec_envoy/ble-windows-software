﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace ACCLAIM_BLE_SERIAL
{
    public class BLE_SPI
    {
        //public string COMM_PORT = "asdf";
        public static SerialPort sp = new SerialPort();
        
        public uint InitSerialPort(string COMM_PORT)
        {
            sp.PortName = COMM_PORT;
            sp.BaudRate = 128000;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.StopBits = StopBits.One;
            sp.ReadTimeout = 5000;
            sp.WriteTimeout = 5000;

            sp.DtrEnable = true;
            sp.RtsEnable = true;
            sp.Open();

            if (sp.IsOpen) { return 1; } else { return 0; }

        }

        public static uint BLE_SPI_READ_32(byte[] addr, ref byte[] data)
        {
            //int t = 0;
            //return 1 if read was successful, 0 if it is not.
            if (addr.Length != 4) { return 0; }
            //if (data.Length != 4) { return 0; }


            byte[] bytestosend = { 0x02, 0x05, addr[3], addr[2], addr[1], addr[0], 0x04, 0x00, 0x00};

            sp.Write(bytestosend, 0, bytestosend.Length);


            int i = 0;
            while ((sp.BytesToRead < 2) && i <= 100)
            {
                System.Threading.Thread.Sleep(50);
                i++;
            }
            if (i == 101) { return 0; }


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];

            sp.Read(buffer, 0, bytes);

            string test = BitConverter.ToString(bytestosend);
            string test_out = BitConverter.ToString(buffer);

            data = buffer;

            if (buffer[3] == 0x00)
                {
                    return 1;
                }
             else
                {
                return 0;
                };

        }

        public static uint BLE_SPI_WRITE_32(byte[] addr, ref byte[] data)
        {

            //return 1 if read was successful, 0 if it is not.
            if (addr.Length != 4) { return 0; }
            if (data.Length != 4) { return 0; }


            byte[] bytestosend = { 0x01, 0x09, addr[3], addr[2], addr[1], addr[0], 0x04, data[3], data[2], data[1], data[0], 0x00, 0x00 };

            sp.Write(bytestosend, 0, bytestosend.Length);


            int i = 0;
            while ((sp.BytesToRead < 2) && i <= 100)
            {
                System.Threading.Thread.Sleep(5);
                i++;
            }
            if (i == 101) { return 0; }


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];

            sp.Read(buffer, 0, bytes);

            string test = BitConverter.ToString(bytestosend);
            string test_out = BitConverter.ToString(buffer);

            data = buffer;

            if (buffer[3] == 0x00)
            {
                return 1;
            }
            else
            {
                return 0;
            };

        }

        public static void BLE_SPI_WRITE_MAC(byte[] macAddress)
        {

            //return 1 if read was successful, 0 if it is not.
            //if (macAddress.Length != 6) { return 0; }
            //if (data.Length != 4) { return 0; }


            byte[] bytestosend = { 0x10, 0x06, macAddress[5], macAddress[4], macAddress[3], macAddress[2], macAddress[1], macAddress[0], 0x00, 0x00 };

            sp.Write(bytestosend, 0, bytestosend.Length);

            int i = 0;
            while ((sp.BytesToRead < 2) && i <= 100)
            {
                System.Threading.Thread.Sleep(5);
                i++;
            }


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];

            sp.Read(buffer, 0, bytes);

            //string test = BitConverter.ToString(bytestosend);
            //string test_out = BitConverter.ToString(buffer);




        }
        public static void BLE_SPI_DISABLE_LOOP()
        {

            //return 1 if read was successful, 0 if it is not.
            //if (macAddress.Length != 6) { return 0; }
            //if (data.Length != 4) { return 0; }

            
            byte[] bytestosend = { 0x11, 0x01,  0x00, 0x00, 0x00 };

            sp.Write(bytestosend, 0, bytestosend.Length);

            int i = 0;
            while ((sp.BytesToRead < 2) && i <= 100)
            {
                System.Threading.Thread.Sleep(5);
                i++;
            }


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];

            sp.Read(buffer, 0, bytes);

            //string test = BitConverter.ToString(bytestosend);
            //string test_out = BitConverter.ToString(buffer);


        }
        public static void BLE_SPI_ENABLE_LOOP()
        {

            //return 1 if read was successful, 0 if it is not.
            //if (macAddress.Length != 6) { return 0; }
            //if (data.Length != 4) { return 0; }


            byte[] bytestosend = { 0x11, 0x01, 0x07, 0x00, 0x00 };

            sp.Write(bytestosend, 0, bytestosend.Length);

            int i = 0;
            while ((sp.BytesToRead < 2) && i <= 100)
            {
                System.Threading.Thread.Sleep(5);
                i++;
            }


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];

            sp.Read(buffer, 0, bytes);

            //string test = BitConverter.ToString(bytestosend);
            //string test_out = BitConverter.ToString(buffer);


        }

        public static uint BLE_SPI_READ_8(byte addr, ref byte[] data)
        {
            //int t = 0;
            //return 1 if read was successful, 0 if it is not.
            //if (addr.Length != 1) { return 0; }
            //if (data.Length != 4) { return 0; }


            byte[] bytestosend = { 0x04, 0x02, addr, 0x01, 0x00, 0x00 };

            sp.Write(bytestosend, 0, bytestosend.Length);


            int i = 0;
            while ((sp.BytesToRead < 2) && i <= 100)
            {
                System.Threading.Thread.Sleep(50);
                i++;
            }
            if (i == 101) { return 0; }


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];

            sp.Read(buffer, 0, bytes);

            string test = BitConverter.ToString(bytestosend);
            string test_out = BitConverter.ToString(buffer);

            data = buffer;

            if (buffer[3] == 0x00)
            {
                return 1;
            }
            else
            {
                return 0;
            };

        }


        public static uint BLE_SPI_WRITE_8(byte addr, ref byte data)
        {

            //return 1 if read was successful, 0 if it is not.
            //if (addr.Length != 4) { return 0; }
            //if (data.Length != 4) { return 0; }


            byte[] bytestosend = { 0x03, 0x03, addr, 0x01, data, 0x00, 0x00 };

            sp.Write(bytestosend, 0, bytestosend.Length);


            int i = 0;
            while ((sp.BytesToRead < 2) && i <= 100)
            {
                System.Threading.Thread.Sleep(50);
                i++;
            }
            if (i == 101) { return 0; }


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];

            sp.Read(buffer, 0, bytes);

            string test = BitConverter.ToString(bytestosend);
            string test_out = BitConverter.ToString(buffer);

            //data = buffer;
            


            if (buffer[3] == 0x00)
            {
                return 1;
            }
            else
            {
                return 0;
            };

        }

        public static uint BLE_READ_BATTERY_VOLTAGE(ref byte[] data)
        {
            //int t = 0;
            //return 1 if read was successful, 0 if it is not.
            //if (addr.Length != 1) { return 0; }
            //if (data.Length != 4) { return 0; }


            byte[] bytestosend = { 0x05, 0x00, 0x00, 0x00 };

            sp.Write(bytestosend, 0, bytestosend.Length);


            int i = 0;
            while ((sp.BytesToRead < 2) && i <= 100)
            {
                System.Threading.Thread.Sleep(50);
                i++;
            }
            if (i == 101) { return 0; }


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];

            sp.Read(buffer, 0, bytes);


            data = buffer;

            if (buffer[2] == 0x00)
            {
                return 1;
            }
            else
            {
                return 0;
            };

        }

        public static uint BLE_READ_IMPLANT_TEMP(ref byte[] data)
        {
            //int t = 0;
            //return 1 if read was successful, 0 if it is not.
            //if (addr.Length != 1) { return 0; }
            //if (data.Length != 4) { return 0; }


            byte[] bytestosend = { 0x06, 0x00, 0x00, 0x00 };

            sp.Write(bytestosend, 0, bytestosend.Length);


            int i = 0;
            while ((sp.BytesToRead < 2) && i <= 100)
            {
                System.Threading.Thread.Sleep(50);
                i++;
            }
            if (i == 101) { return 0; }


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];

            sp.Read(buffer, 0, bytes);


            data = buffer;

            if (buffer[2] == 0x00)
            {
                return 1;
            }
            else
            {
                return 0;
            };

        }




        public void CloseSerialPort()
        {
            sp.Close();
            sp.Dispose();

        }

        
    }
}
