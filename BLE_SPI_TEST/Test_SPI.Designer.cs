﻿namespace BLE_SPI_TEST
{
    partial class Test_SPI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.lstCommPorts = new System.Windows.Forms.ListBox();
            this.btnResetComm = new System.Windows.Forms.Button();
            this.btnCommSet = new System.Windows.Forms.Button();
            this.txtReadStart = new System.Windows.Forms.TextBox();
            this.txtReadStop = new System.Windows.Forms.TextBox();
            this.btnReadASIC = new System.Windows.Forms.Button();
            this.btnLoadFile = new System.Windows.Forms.Button();
            this.btnClearData = new System.Windows.Forms.Button();
            this.btnWriteAll = new System.Windows.Forms.Button();
            this.btnSaveData = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.DataGridRead = new System.Windows.Forms.DataGridView();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnLoadCheckSum = new System.Windows.Forms.Button();
            this.txtPMICEEPROMFile = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnReadConfigEEPROM = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridEEPROM = new System.Windows.Forms.DataGridView();
            this.AddressEE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataEE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.e3_Register848 = new ASIC_Test_Interface.E3_Register8();
            this.diagPort01 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register833 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register82 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register834 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register83 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register835 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register84 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register836 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register85 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register837 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register86 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register838 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register812 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register839 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register811 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register840 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register810 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register841 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register89 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register842 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register88 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register843 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register87 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register844 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register816 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register845 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register815 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register846 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register814 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register847 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register813 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register832 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register817 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register831 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register818 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register830 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register819 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register829 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register820 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register828 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register821 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register827 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register822 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register826 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register823 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register825 = new ASIC_Test_Interface.E3_Register8();
            this.e3_Register824 = new ASIC_Test_Interface.E3_Register8();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.reg_1F = new ASIC_Test_Interface.E3_Register32();
            this.reg_1C = new ASIC_Test_Interface.E3_Register32();
            this.reg_1E = new ASIC_Test_Interface.E3_Register32();
            this.reg_00 = new ASIC_Test_Interface.E3_Register32();
            this.reg_1D = new ASIC_Test_Interface.E3_Register32();
            this.reg_01 = new ASIC_Test_Interface.E3_Register32();
            this.reg_02 = new ASIC_Test_Interface.E3_Register32();
            this.reg_1B = new ASIC_Test_Interface.E3_Register32();
            this.reg_03 = new ASIC_Test_Interface.E3_Register32();
            this.reg_1A = new ASIC_Test_Interface.E3_Register32();
            this.reg_04 = new ASIC_Test_Interface.E3_Register32();
            this.reg_19 = new ASIC_Test_Interface.E3_Register32();
            this.reg_05 = new ASIC_Test_Interface.E3_Register32();
            this.reg_18 = new ASIC_Test_Interface.E3_Register32();
            this.reg_06 = new ASIC_Test_Interface.E3_Register32();
            this.reg_17 = new ASIC_Test_Interface.E3_Register32();
            this.reg_07 = new ASIC_Test_Interface.E3_Register32();
            this.reg_16 = new ASIC_Test_Interface.E3_Register32();
            this.reg_08 = new ASIC_Test_Interface.E3_Register32();
            this.reg_15 = new ASIC_Test_Interface.E3_Register32();
            this.reg_09 = new ASIC_Test_Interface.E3_Register32();
            this.reg_14 = new ASIC_Test_Interface.E3_Register32();
            this.reg_0A = new ASIC_Test_Interface.E3_Register32();
            this.reg_13 = new ASIC_Test_Interface.E3_Register32();
            this.reg_0B = new ASIC_Test_Interface.E3_Register32();
            this.reg_12 = new ASIC_Test_Interface.E3_Register32();
            this.reg_0C = new ASIC_Test_Interface.E3_Register32();
            this.reg_11 = new ASIC_Test_Interface.E3_Register32();
            this.reg_0D = new ASIC_Test_Interface.E3_Register32();
            this.reg_10 = new ASIC_Test_Interface.E3_Register32();
            this.reg_0E = new ASIC_Test_Interface.E3_Register32();
            this.reg_0F = new ASIC_Test_Interface.E3_Register32();
            this.tabRegisters = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.txtFlashStatus = new System.Windows.Forms.TextBox();
            this.btnReadFlashStatus = new System.Windows.Forms.Button();
            this.btnEraseFlash = new System.Windows.Forms.Button();
            this.btnReadFlash = new System.Windows.Forms.Button();
            this.btnWriteFlash = new System.Windows.Forms.Button();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.regstim_1F = new ASIC_Test_Interface.E3_Register32();
            this.regstim_1C = new ASIC_Test_Interface.E3_Register32();
            this.regstim_1E = new ASIC_Test_Interface.E3_Register32();
            this.regstim_00 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_1D = new ASIC_Test_Interface.E3_Register32();
            this.regstim_01 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_02 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_1B = new ASIC_Test_Interface.E3_Register32();
            this.regstim_03 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_1A = new ASIC_Test_Interface.E3_Register32();
            this.regstim_04 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_19 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_05 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_18 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_06 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_17 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_07 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_16 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_08 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_15 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_09 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_14 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_0A = new ASIC_Test_Interface.E3_Register32();
            this.regstim_13 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_0B = new ASIC_Test_Interface.E3_Register32();
            this.regstim_12 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_0C = new ASIC_Test_Interface.E3_Register32();
            this.regstim_11 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_0D = new ASIC_Test_Interface.E3_Register32();
            this.regstim_10 = new ASIC_Test_Interface.E3_Register32();
            this.regstim_0E = new ASIC_Test_Interface.E3_Register32();
            this.regstim_0F = new ASIC_Test_Interface.E3_Register32();
            this.tabStimRegisters = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.lblTemp2MSE = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTemp2Mean = new System.Windows.Forms.Label();
            this.lblTemp2Meana = new System.Windows.Forms.Label();
            this.lblTemp1MSE = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTemp1Mean = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnTemperatureSensorsRun = new System.Windows.Forms.Button();
            this.chartTempSensors = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.txtMACAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblBatteryTemperature2 = new System.Windows.Forms.Label();
            this.lblBatteryTemperature1 = new System.Windows.Forms.Label();
            this.lblBatteryVoltage = new System.Windows.Forms.Label();
            this.btnRefreshImplantStatus = new System.Windows.Forms.Button();
            this.btnDisableLoop = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridRead)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEEPROM)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabRegisters.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.tabStimRegisters.SuspendLayout();
            this.tabPage21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTempSensors)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstCommPorts
            // 
            this.lstCommPorts.FormattingEnabled = true;
            this.lstCommPorts.Location = new System.Drawing.Point(158, 12);
            this.lstCommPorts.Name = "lstCommPorts";
            this.lstCommPorts.Size = new System.Drawing.Size(120, 69);
            this.lstCommPorts.TabIndex = 4;
            // 
            // btnResetComm
            // 
            this.btnResetComm.Location = new System.Drawing.Point(284, 55);
            this.btnResetComm.Name = "btnResetComm";
            this.btnResetComm.Size = new System.Drawing.Size(64, 26);
            this.btnResetComm.TabIndex = 5;
            this.btnResetComm.Text = "Reset";
            this.btnResetComm.UseVisualStyleBackColor = true;
            this.btnResetComm.Click += new System.EventHandler(this.btnResetComm_Click);
            // 
            // btnCommSet
            // 
            this.btnCommSet.Location = new System.Drawing.Point(284, 12);
            this.btnCommSet.Name = "btnCommSet";
            this.btnCommSet.Size = new System.Drawing.Size(63, 26);
            this.btnCommSet.TabIndex = 6;
            this.btnCommSet.Text = "Set";
            this.btnCommSet.UseVisualStyleBackColor = true;
            this.btnCommSet.Click += new System.EventHandler(this.btnCommSet_Click);
            // 
            // txtReadStart
            // 
            this.txtReadStart.Location = new System.Drawing.Point(89, 117);
            this.txtReadStart.Name = "txtReadStart";
            this.txtReadStart.Size = new System.Drawing.Size(100, 20);
            this.txtReadStart.TabIndex = 11;
            this.txtReadStart.Text = "20000000";
            // 
            // txtReadStop
            // 
            this.txtReadStop.Location = new System.Drawing.Point(89, 143);
            this.txtReadStop.Name = "txtReadStop";
            this.txtReadStop.Size = new System.Drawing.Size(100, 20);
            this.txtReadStop.TabIndex = 12;
            this.txtReadStop.Text = "2000000F";
            // 
            // btnReadASIC
            // 
            this.btnReadASIC.Location = new System.Drawing.Point(125, 169);
            this.btnReadASIC.Name = "btnReadASIC";
            this.btnReadASIC.Size = new System.Drawing.Size(64, 29);
            this.btnReadASIC.TabIndex = 13;
            this.btnReadASIC.Text = "Read";
            this.btnReadASIC.UseVisualStyleBackColor = true;
            this.btnReadASIC.Click += new System.EventHandler(this.btnReadASIC_Click);
            // 
            // btnLoadFile
            // 
            this.btnLoadFile.Location = new System.Drawing.Point(89, 58);
            this.btnLoadFile.Name = "btnLoadFile";
            this.btnLoadFile.Size = new System.Drawing.Size(64, 29);
            this.btnLoadFile.TabIndex = 14;
            this.btnLoadFile.Text = "Load";
            this.btnLoadFile.UseVisualStyleBackColor = true;
            this.btnLoadFile.Click += new System.EventHandler(this.btnLoadFile_Click);
            // 
            // btnClearData
            // 
            this.btnClearData.Location = new System.Drawing.Point(125, 239);
            this.btnClearData.Name = "btnClearData";
            this.btnClearData.Size = new System.Drawing.Size(64, 29);
            this.btnClearData.TabIndex = 15;
            this.btnClearData.Text = "Clear";
            this.btnClearData.UseVisualStyleBackColor = true;
            this.btnClearData.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnWriteAll
            // 
            this.btnWriteAll.Location = new System.Drawing.Point(125, 204);
            this.btnWriteAll.Name = "btnWriteAll";
            this.btnWriteAll.Size = new System.Drawing.Size(64, 29);
            this.btnWriteAll.TabIndex = 16;
            this.btnWriteAll.Text = "Write";
            this.btnWriteAll.UseVisualStyleBackColor = true;
            this.btnWriteAll.Click += new System.EventHandler(this.btnWriteAll_Click);
            // 
            // btnSaveData
            // 
            this.btnSaveData.Location = new System.Drawing.Point(159, 58);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(64, 29);
            this.btnSaveData.TabIndex = 17;
            this.btnSaveData.Text = "Save";
            this.btnSaveData.UseVisualStyleBackColor = true;
            this.btnSaveData.Click += new System.EventHandler(this.btnSaveData_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(89, 30);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(372, 20);
            this.txtFilePath.TabIndex = 18;
            // 
            // DataGridRead
            // 
            this.DataGridRead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridRead.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Address,
            this.Data});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridRead.DefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridRead.Location = new System.Drawing.Point(195, 117);
            this.DataGridRead.Name = "DataGridRead";
            this.DataGridRead.RowTemplate.Height = 31;
            this.DataGridRead.Size = new System.Drawing.Size(266, 554);
            this.DataGridRead.TabIndex = 19;
            // 
            // Address
            // 
            this.Address.HeaderText = "Address";
            this.Address.Name = "Address";
            // 
            // Data
            // 
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage21);
            this.tabControl1.Location = new System.Drawing.Point(12, 90);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1877, 787);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1869, 761);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Memory Map";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnLoadCheckSum);
            this.groupBox3.Controls.Add(this.txtPMICEEPROMFile);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.btnReadConfigEEPROM);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.dataGridEEPROM);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Location = new System.Drawing.Point(685, 40);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(476, 678);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PMIC EEPROM - Configuration Regs:";
            // 
            // btnLoadCheckSum
            // 
            this.btnLoadCheckSum.Location = new System.Drawing.Point(125, 274);
            this.btnLoadCheckSum.Name = "btnLoadCheckSum";
            this.btnLoadCheckSum.Size = new System.Drawing.Size(64, 58);
            this.btnLoadCheckSum.TabIndex = 23;
            this.btnLoadCheckSum.Text = "Load ChkSum";
            this.btnLoadCheckSum.UseVisualStyleBackColor = true;
            this.btnLoadCheckSum.Click += new System.EventHandler(this.btnLoadCheckSum_Click);
            // 
            // txtPMICEEPROMFile
            // 
            this.txtPMICEEPROMFile.Location = new System.Drawing.Point(89, 30);
            this.txtPMICEEPROMFile.Name = "txtPMICEEPROMFile";
            this.txtPMICEEPROMFile.Size = new System.Drawing.Size(372, 20);
            this.txtPMICEEPROMFile.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Config File:";
            // 
            // btnReadConfigEEPROM
            // 
            this.btnReadConfigEEPROM.Location = new System.Drawing.Point(125, 169);
            this.btnReadConfigEEPROM.Name = "btnReadConfigEEPROM";
            this.btnReadConfigEEPROM.Size = new System.Drawing.Size(64, 29);
            this.btnReadConfigEEPROM.TabIndex = 13;
            this.btnReadConfigEEPROM.Text = "Read";
            this.btnReadConfigEEPROM.UseVisualStyleBackColor = true;
            this.btnReadConfigEEPROM.Click += new System.EventHandler(this.btnReadConfigEEPROM_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(89, 58);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 29);
            this.button2.TabIndex = 14;
            this.button2.Text = "Load";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(125, 239);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(64, 29);
            this.button3.TabIndex = 15;
            this.button3.Text = "Clear";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGridEEPROM
            // 
            this.dataGridEEPROM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridEEPROM.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AddressEE,
            this.DataEE});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridEEPROM.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridEEPROM.Location = new System.Drawing.Point(195, 117);
            this.dataGridEEPROM.Name = "dataGridEEPROM";
            this.dataGridEEPROM.RowTemplate.Height = 31;
            this.dataGridEEPROM.Size = new System.Drawing.Size(266, 554);
            this.dataGridEEPROM.TabIndex = 19;
            // 
            // AddressEE
            // 
            this.AddressEE.HeaderText = "Address";
            this.AddressEE.Name = "AddressEE";
            // 
            // DataEE
            // 
            this.DataEE.HeaderText = "Data";
            this.DataEE.Name = "DataEE";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(125, 204);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(64, 29);
            this.button4.TabIndex = 16;
            this.button4.Text = "Write";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(159, 58);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(64, 29);
            this.button5.TabIndex = 17;
            this.button5.Text = "Save";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFilePath);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnReadASIC);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnLoadFile);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnClearData);
            this.groupBox1.Controls.Add(this.DataGridRead);
            this.groupBox1.Controls.Add(this.btnWriteAll);
            this.groupBox1.Controls.Add(this.btnSaveData);
            this.groupBox1.Controls.Add(this.txtReadStop);
            this.groupBox1.Controls.Add(this.txtReadStart);
            this.groupBox1.Location = new System.Drawing.Point(1285, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(476, 678);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Multiple Register Access:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "File Path:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Stop Address:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Start Address:";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.groupBox2);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(192, 74);
            this.tabPage10.TabIndex = 3;
            this.tabPage10.Text = "Diagnostic Port Interface";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.e3_Register848);
            this.groupBox2.Controls.Add(this.diagPort01);
            this.groupBox2.Controls.Add(this.e3_Register833);
            this.groupBox2.Controls.Add(this.e3_Register82);
            this.groupBox2.Controls.Add(this.e3_Register834);
            this.groupBox2.Controls.Add(this.e3_Register83);
            this.groupBox2.Controls.Add(this.e3_Register835);
            this.groupBox2.Controls.Add(this.e3_Register84);
            this.groupBox2.Controls.Add(this.e3_Register836);
            this.groupBox2.Controls.Add(this.e3_Register85);
            this.groupBox2.Controls.Add(this.e3_Register837);
            this.groupBox2.Controls.Add(this.e3_Register86);
            this.groupBox2.Controls.Add(this.e3_Register838);
            this.groupBox2.Controls.Add(this.e3_Register812);
            this.groupBox2.Controls.Add(this.e3_Register839);
            this.groupBox2.Controls.Add(this.e3_Register811);
            this.groupBox2.Controls.Add(this.e3_Register840);
            this.groupBox2.Controls.Add(this.e3_Register810);
            this.groupBox2.Controls.Add(this.e3_Register841);
            this.groupBox2.Controls.Add(this.e3_Register89);
            this.groupBox2.Controls.Add(this.e3_Register842);
            this.groupBox2.Controls.Add(this.e3_Register88);
            this.groupBox2.Controls.Add(this.e3_Register843);
            this.groupBox2.Controls.Add(this.e3_Register87);
            this.groupBox2.Controls.Add(this.e3_Register844);
            this.groupBox2.Controls.Add(this.e3_Register816);
            this.groupBox2.Controls.Add(this.e3_Register845);
            this.groupBox2.Controls.Add(this.e3_Register815);
            this.groupBox2.Controls.Add(this.e3_Register846);
            this.groupBox2.Controls.Add(this.e3_Register814);
            this.groupBox2.Controls.Add(this.e3_Register847);
            this.groupBox2.Controls.Add(this.e3_Register813);
            this.groupBox2.Controls.Add(this.e3_Register832);
            this.groupBox2.Controls.Add(this.e3_Register817);
            this.groupBox2.Controls.Add(this.e3_Register831);
            this.groupBox2.Controls.Add(this.e3_Register818);
            this.groupBox2.Controls.Add(this.e3_Register830);
            this.groupBox2.Controls.Add(this.e3_Register819);
            this.groupBox2.Controls.Add(this.e3_Register829);
            this.groupBox2.Controls.Add(this.e3_Register820);
            this.groupBox2.Controls.Add(this.e3_Register828);
            this.groupBox2.Controls.Add(this.e3_Register821);
            this.groupBox2.Controls.Add(this.e3_Register827);
            this.groupBox2.Controls.Add(this.e3_Register822);
            this.groupBox2.Controls.Add(this.e3_Register826);
            this.groupBox2.Controls.Add(this.e3_Register823);
            this.groupBox2.Controls.Add(this.e3_Register825);
            this.groupBox2.Controls.Add(this.e3_Register824);
            this.groupBox2.Location = new System.Drawing.Point(551, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(819, 733);
            this.groupBox2.TabIndex = 80;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Diagnostic Interface:";
            // 
            // e3_Register848
            // 
            this.e3_Register848.Address = ((ushort)(32));
            this.e3_Register848.bit1 = "bit1";
            this.e3_Register848.bit2 = "bit2";
            this.e3_Register848.bit3 = "bit3";
            this.e3_Register848.bit4 = "bit4";
            this.e3_Register848.bit5 = "bit5";
            this.e3_Register848.bit6 = "bit6";
            this.e3_Register848.bit7 = "bit7";
            this.e3_Register848.bit8 = "bit8";
            this.e3_Register848.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register848.Location = new System.Drawing.Point(544, 34);
            this.e3_Register848.Name = "e3_Register848";
            this.e3_Register848.RegisterName = null;
            this.e3_Register848.Size = new System.Drawing.Size(258, 38);
            this.e3_Register848.TabIndex = 63;
            // 
            // diagPort01
            // 
            this.diagPort01.Address = ((ushort)(0));
            this.diagPort01.bit1 = "bit1";
            this.diagPort01.bit2 = "bit2";
            this.diagPort01.bit3 = "bit3";
            this.diagPort01.bit4 = "bit4";
            this.diagPort01.bit5 = "bit5";
            this.diagPort01.bit6 = "bit6";
            this.diagPort01.bit7 = "bit7";
            this.diagPort01.bit8 = "bit8";
            this.diagPort01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.diagPort01.Location = new System.Drawing.Point(16, 34);
            this.diagPort01.Name = "diagPort01";
            this.diagPort01.RegisterName = "ASIC Rd Data Byte 3";
            this.diagPort01.Size = new System.Drawing.Size(258, 38);
            this.diagPort01.TabIndex = 31;
            // 
            // e3_Register833
            // 
            this.e3_Register833.Address = ((ushort)(47));
            this.e3_Register833.bit1 = "bit1";
            this.e3_Register833.bit2 = "bit2";
            this.e3_Register833.bit3 = "bit3";
            this.e3_Register833.bit4 = "bit4";
            this.e3_Register833.bit5 = "bit5";
            this.e3_Register833.bit6 = "bit6";
            this.e3_Register833.bit7 = "bit7";
            this.e3_Register833.bit8 = "bit8";
            this.e3_Register833.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register833.Location = new System.Drawing.Point(544, 684);
            this.e3_Register833.Name = "e3_Register833";
            this.e3_Register833.RegisterName = null;
            this.e3_Register833.Size = new System.Drawing.Size(258, 38);
            this.e3_Register833.TabIndex = 78;
            // 
            // e3_Register82
            // 
            this.e3_Register82.Address = ((ushort)(1));
            this.e3_Register82.bit1 = "bit1";
            this.e3_Register82.bit2 = "bit2";
            this.e3_Register82.bit3 = "bit3";
            this.e3_Register82.bit4 = "bit4";
            this.e3_Register82.bit5 = "bit5";
            this.e3_Register82.bit6 = "bit6";
            this.e3_Register82.bit7 = "bit7";
            this.e3_Register82.bit8 = "bit8";
            this.e3_Register82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register82.Location = new System.Drawing.Point(16, 76);
            this.e3_Register82.Name = "e3_Register82";
            this.e3_Register82.RegisterName = null;
            this.e3_Register82.Size = new System.Drawing.Size(258, 38);
            this.e3_Register82.TabIndex = 32;
            // 
            // e3_Register834
            // 
            this.e3_Register834.Address = ((ushort)(46));
            this.e3_Register834.bit1 = "bit1";
            this.e3_Register834.bit2 = "bit2";
            this.e3_Register834.bit3 = "bit3";
            this.e3_Register834.bit4 = "bit4";
            this.e3_Register834.bit5 = "bit5";
            this.e3_Register834.bit6 = "bit6";
            this.e3_Register834.bit7 = "bit7";
            this.e3_Register834.bit8 = "bit8";
            this.e3_Register834.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register834.Location = new System.Drawing.Point(544, 640);
            this.e3_Register834.Name = "e3_Register834";
            this.e3_Register834.RegisterName = null;
            this.e3_Register834.Size = new System.Drawing.Size(258, 38);
            this.e3_Register834.TabIndex = 77;
            // 
            // e3_Register83
            // 
            this.e3_Register83.Address = ((ushort)(2));
            this.e3_Register83.bit1 = "bit1";
            this.e3_Register83.bit2 = "bit2";
            this.e3_Register83.bit3 = "bit3";
            this.e3_Register83.bit4 = "bit4";
            this.e3_Register83.bit5 = "bit5";
            this.e3_Register83.bit6 = "bit6";
            this.e3_Register83.bit7 = "bit7";
            this.e3_Register83.bit8 = "bit8";
            this.e3_Register83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register83.Location = new System.Drawing.Point(16, 117);
            this.e3_Register83.Name = "e3_Register83";
            this.e3_Register83.RegisterName = null;
            this.e3_Register83.Size = new System.Drawing.Size(258, 38);
            this.e3_Register83.TabIndex = 33;
            // 
            // e3_Register835
            // 
            this.e3_Register835.Address = ((ushort)(45));
            this.e3_Register835.bit1 = "bit1";
            this.e3_Register835.bit2 = "bit2";
            this.e3_Register835.bit3 = "bit3";
            this.e3_Register835.bit4 = "bit4";
            this.e3_Register835.bit5 = "bit5";
            this.e3_Register835.bit6 = "bit6";
            this.e3_Register835.bit7 = "bit7";
            this.e3_Register835.bit8 = "bit8";
            this.e3_Register835.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register835.Location = new System.Drawing.Point(544, 596);
            this.e3_Register835.Name = "e3_Register835";
            this.e3_Register835.RegisterName = null;
            this.e3_Register835.Size = new System.Drawing.Size(258, 38);
            this.e3_Register835.TabIndex = 76;
            // 
            // e3_Register84
            // 
            this.e3_Register84.Address = ((ushort)(3));
            this.e3_Register84.bit1 = "bit1";
            this.e3_Register84.bit2 = "bit2";
            this.e3_Register84.bit3 = "bit3";
            this.e3_Register84.bit4 = "bit4";
            this.e3_Register84.bit5 = "bit5";
            this.e3_Register84.bit6 = "bit6";
            this.e3_Register84.bit7 = "bit7";
            this.e3_Register84.bit8 = "bit8";
            this.e3_Register84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register84.Location = new System.Drawing.Point(16, 161);
            this.e3_Register84.Name = "e3_Register84";
            this.e3_Register84.RegisterName = null;
            this.e3_Register84.Size = new System.Drawing.Size(258, 38);
            this.e3_Register84.TabIndex = 34;
            // 
            // e3_Register836
            // 
            this.e3_Register836.Address = ((ushort)(44));
            this.e3_Register836.bit1 = "bit1";
            this.e3_Register836.bit2 = "bit2";
            this.e3_Register836.bit3 = "bit3";
            this.e3_Register836.bit4 = "bit4";
            this.e3_Register836.bit5 = "bit5";
            this.e3_Register836.bit6 = "bit6";
            this.e3_Register836.bit7 = "bit7";
            this.e3_Register836.bit8 = "bit8";
            this.e3_Register836.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register836.Location = new System.Drawing.Point(544, 552);
            this.e3_Register836.Name = "e3_Register836";
            this.e3_Register836.RegisterName = null;
            this.e3_Register836.Size = new System.Drawing.Size(258, 38);
            this.e3_Register836.TabIndex = 75;
            // 
            // e3_Register85
            // 
            this.e3_Register85.Address = ((ushort)(4));
            this.e3_Register85.bit1 = "bit1";
            this.e3_Register85.bit2 = "bit2";
            this.e3_Register85.bit3 = "bit3";
            this.e3_Register85.bit4 = "bit4";
            this.e3_Register85.bit5 = "bit5";
            this.e3_Register85.bit6 = "bit6";
            this.e3_Register85.bit7 = "bit7";
            this.e3_Register85.bit8 = "bit8";
            this.e3_Register85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register85.Location = new System.Drawing.Point(16, 205);
            this.e3_Register85.Name = "e3_Register85";
            this.e3_Register85.RegisterName = null;
            this.e3_Register85.Size = new System.Drawing.Size(258, 38);
            this.e3_Register85.TabIndex = 35;
            // 
            // e3_Register837
            // 
            this.e3_Register837.Address = ((ushort)(43));
            this.e3_Register837.bit1 = "bit1";
            this.e3_Register837.bit2 = "bit2";
            this.e3_Register837.bit3 = "bit3";
            this.e3_Register837.bit4 = "bit4";
            this.e3_Register837.bit5 = "bit5";
            this.e3_Register837.bit6 = "bit6";
            this.e3_Register837.bit7 = "bit7";
            this.e3_Register837.bit8 = "bit8";
            this.e3_Register837.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register837.Location = new System.Drawing.Point(544, 508);
            this.e3_Register837.Name = "e3_Register837";
            this.e3_Register837.RegisterName = null;
            this.e3_Register837.Size = new System.Drawing.Size(258, 38);
            this.e3_Register837.TabIndex = 74;
            // 
            // e3_Register86
            // 
            this.e3_Register86.Address = ((ushort)(5));
            this.e3_Register86.bit1 = "bit1";
            this.e3_Register86.bit2 = "bit2";
            this.e3_Register86.bit3 = "bit3";
            this.e3_Register86.bit4 = "bit4";
            this.e3_Register86.bit5 = "bit5";
            this.e3_Register86.bit6 = "bit6";
            this.e3_Register86.bit7 = "bit7";
            this.e3_Register86.bit8 = "bit8";
            this.e3_Register86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register86.Location = new System.Drawing.Point(16, 249);
            this.e3_Register86.Name = "e3_Register86";
            this.e3_Register86.RegisterName = null;
            this.e3_Register86.Size = new System.Drawing.Size(258, 38);
            this.e3_Register86.TabIndex = 36;
            // 
            // e3_Register838
            // 
            this.e3_Register838.Address = ((ushort)(42));
            this.e3_Register838.bit1 = "bit1";
            this.e3_Register838.bit2 = "bit2";
            this.e3_Register838.bit3 = "bit3";
            this.e3_Register838.bit4 = "bit4";
            this.e3_Register838.bit5 = "bit5";
            this.e3_Register838.bit6 = "bit6";
            this.e3_Register838.bit7 = "bit7";
            this.e3_Register838.bit8 = "bit8";
            this.e3_Register838.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register838.Location = new System.Drawing.Point(544, 464);
            this.e3_Register838.Name = "e3_Register838";
            this.e3_Register838.RegisterName = null;
            this.e3_Register838.Size = new System.Drawing.Size(258, 38);
            this.e3_Register838.TabIndex = 73;
            // 
            // e3_Register812
            // 
            this.e3_Register812.Address = ((ushort)(6));
            this.e3_Register812.bit1 = "bit1";
            this.e3_Register812.bit2 = "bit2";
            this.e3_Register812.bit3 = "bit3";
            this.e3_Register812.bit4 = "bit4";
            this.e3_Register812.bit5 = "bit5";
            this.e3_Register812.bit6 = "bit6";
            this.e3_Register812.bit7 = "bit7";
            this.e3_Register812.bit8 = "bit8";
            this.e3_Register812.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register812.Location = new System.Drawing.Point(16, 293);
            this.e3_Register812.Name = "e3_Register812";
            this.e3_Register812.RegisterName = null;
            this.e3_Register812.Size = new System.Drawing.Size(258, 38);
            this.e3_Register812.TabIndex = 37;
            // 
            // e3_Register839
            // 
            this.e3_Register839.Address = ((ushort)(41));
            this.e3_Register839.bit1 = "bit1";
            this.e3_Register839.bit2 = "bit2";
            this.e3_Register839.bit3 = "bit3";
            this.e3_Register839.bit4 = "bit4";
            this.e3_Register839.bit5 = "bit5";
            this.e3_Register839.bit6 = "bit6";
            this.e3_Register839.bit7 = "bit7";
            this.e3_Register839.bit8 = "bit8";
            this.e3_Register839.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register839.Location = new System.Drawing.Point(544, 420);
            this.e3_Register839.Name = "e3_Register839";
            this.e3_Register839.RegisterName = null;
            this.e3_Register839.Size = new System.Drawing.Size(258, 38);
            this.e3_Register839.TabIndex = 72;
            // 
            // e3_Register811
            // 
            this.e3_Register811.Address = ((ushort)(7));
            this.e3_Register811.bit1 = "bit1";
            this.e3_Register811.bit2 = "bit2";
            this.e3_Register811.bit3 = "bit3";
            this.e3_Register811.bit4 = "bit4";
            this.e3_Register811.bit5 = "bit5";
            this.e3_Register811.bit6 = "bit6";
            this.e3_Register811.bit7 = "bit7";
            this.e3_Register811.bit8 = "bit8";
            this.e3_Register811.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register811.Location = new System.Drawing.Point(16, 335);
            this.e3_Register811.Name = "e3_Register811";
            this.e3_Register811.RegisterName = null;
            this.e3_Register811.Size = new System.Drawing.Size(258, 38);
            this.e3_Register811.TabIndex = 38;
            // 
            // e3_Register840
            // 
            this.e3_Register840.Address = ((ushort)(40));
            this.e3_Register840.bit1 = "bit1";
            this.e3_Register840.bit2 = "bit2";
            this.e3_Register840.bit3 = "bit3";
            this.e3_Register840.bit4 = "bit4";
            this.e3_Register840.bit5 = "bit5";
            this.e3_Register840.bit6 = "bit6";
            this.e3_Register840.bit7 = "bit7";
            this.e3_Register840.bit8 = "bit8";
            this.e3_Register840.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register840.Location = new System.Drawing.Point(544, 376);
            this.e3_Register840.Name = "e3_Register840";
            this.e3_Register840.RegisterName = null;
            this.e3_Register840.Size = new System.Drawing.Size(258, 38);
            this.e3_Register840.TabIndex = 71;
            // 
            // e3_Register810
            // 
            this.e3_Register810.Address = ((ushort)(8));
            this.e3_Register810.bit1 = "bit1";
            this.e3_Register810.bit2 = "bit2";
            this.e3_Register810.bit3 = "bit3";
            this.e3_Register810.bit4 = "bit4";
            this.e3_Register810.bit5 = "bit5";
            this.e3_Register810.bit6 = "bit6";
            this.e3_Register810.bit7 = "bit7";
            this.e3_Register810.bit8 = "bit8";
            this.e3_Register810.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register810.Location = new System.Drawing.Point(16, 376);
            this.e3_Register810.Name = "e3_Register810";
            this.e3_Register810.RegisterName = null;
            this.e3_Register810.Size = new System.Drawing.Size(258, 38);
            this.e3_Register810.TabIndex = 39;
            // 
            // e3_Register841
            // 
            this.e3_Register841.Address = ((ushort)(39));
            this.e3_Register841.bit1 = "bit1";
            this.e3_Register841.bit2 = "bit2";
            this.e3_Register841.bit3 = "bit3";
            this.e3_Register841.bit4 = "bit4";
            this.e3_Register841.bit5 = "bit5";
            this.e3_Register841.bit6 = "bit6";
            this.e3_Register841.bit7 = "bit7";
            this.e3_Register841.bit8 = "bit8";
            this.e3_Register841.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register841.Location = new System.Drawing.Point(544, 335);
            this.e3_Register841.Name = "e3_Register841";
            this.e3_Register841.RegisterName = null;
            this.e3_Register841.Size = new System.Drawing.Size(258, 38);
            this.e3_Register841.TabIndex = 70;
            // 
            // e3_Register89
            // 
            this.e3_Register89.Address = ((ushort)(9));
            this.e3_Register89.bit1 = "bit1";
            this.e3_Register89.bit2 = "bit2";
            this.e3_Register89.bit3 = "bit3";
            this.e3_Register89.bit4 = "bit4";
            this.e3_Register89.bit5 = "bit5";
            this.e3_Register89.bit6 = "bit6";
            this.e3_Register89.bit7 = "bit7";
            this.e3_Register89.bit8 = "bit8";
            this.e3_Register89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register89.Location = new System.Drawing.Point(16, 420);
            this.e3_Register89.Name = "e3_Register89";
            this.e3_Register89.RegisterName = null;
            this.e3_Register89.Size = new System.Drawing.Size(258, 38);
            this.e3_Register89.TabIndex = 40;
            // 
            // e3_Register842
            // 
            this.e3_Register842.Address = ((ushort)(38));
            this.e3_Register842.bit1 = "bit1";
            this.e3_Register842.bit2 = "bit2";
            this.e3_Register842.bit3 = "bit3";
            this.e3_Register842.bit4 = "bit4";
            this.e3_Register842.bit5 = "bit5";
            this.e3_Register842.bit6 = "bit6";
            this.e3_Register842.bit7 = "bit7";
            this.e3_Register842.bit8 = "bit8";
            this.e3_Register842.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register842.Location = new System.Drawing.Point(544, 293);
            this.e3_Register842.Name = "e3_Register842";
            this.e3_Register842.RegisterName = null;
            this.e3_Register842.Size = new System.Drawing.Size(258, 38);
            this.e3_Register842.TabIndex = 69;
            // 
            // e3_Register88
            // 
            this.e3_Register88.Address = ((ushort)(10));
            this.e3_Register88.bit1 = "bit1";
            this.e3_Register88.bit2 = "bit2";
            this.e3_Register88.bit3 = "bit3";
            this.e3_Register88.bit4 = "bit4";
            this.e3_Register88.bit5 = "bit5";
            this.e3_Register88.bit6 = "bit6";
            this.e3_Register88.bit7 = "bit7";
            this.e3_Register88.bit8 = "bit8";
            this.e3_Register88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register88.Location = new System.Drawing.Point(16, 464);
            this.e3_Register88.Name = "e3_Register88";
            this.e3_Register88.RegisterName = null;
            this.e3_Register88.Size = new System.Drawing.Size(258, 38);
            this.e3_Register88.TabIndex = 41;
            // 
            // e3_Register843
            // 
            this.e3_Register843.Address = ((ushort)(37));
            this.e3_Register843.bit1 = "bit1";
            this.e3_Register843.bit2 = "bit2";
            this.e3_Register843.bit3 = "bit3";
            this.e3_Register843.bit4 = "bit4";
            this.e3_Register843.bit5 = "bit5";
            this.e3_Register843.bit6 = "bit6";
            this.e3_Register843.bit7 = "bit7";
            this.e3_Register843.bit8 = "bit8";
            this.e3_Register843.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register843.Location = new System.Drawing.Point(544, 249);
            this.e3_Register843.Name = "e3_Register843";
            this.e3_Register843.RegisterName = null;
            this.e3_Register843.Size = new System.Drawing.Size(258, 38);
            this.e3_Register843.TabIndex = 68;
            // 
            // e3_Register87
            // 
            this.e3_Register87.Address = ((ushort)(11));
            this.e3_Register87.bit1 = "bit1";
            this.e3_Register87.bit2 = "bit2";
            this.e3_Register87.bit3 = "bit3";
            this.e3_Register87.bit4 = "bit4";
            this.e3_Register87.bit5 = "bit5";
            this.e3_Register87.bit6 = "bit6";
            this.e3_Register87.bit7 = "bit7";
            this.e3_Register87.bit8 = "bit8";
            this.e3_Register87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register87.Location = new System.Drawing.Point(16, 508);
            this.e3_Register87.Name = "e3_Register87";
            this.e3_Register87.RegisterName = null;
            this.e3_Register87.Size = new System.Drawing.Size(258, 38);
            this.e3_Register87.TabIndex = 42;
            // 
            // e3_Register844
            // 
            this.e3_Register844.Address = ((ushort)(36));
            this.e3_Register844.bit1 = "bit1";
            this.e3_Register844.bit2 = "bit2";
            this.e3_Register844.bit3 = "bit3";
            this.e3_Register844.bit4 = "bit4";
            this.e3_Register844.bit5 = "bit5";
            this.e3_Register844.bit6 = "bit6";
            this.e3_Register844.bit7 = "bit7";
            this.e3_Register844.bit8 = "bit8";
            this.e3_Register844.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register844.Location = new System.Drawing.Point(544, 205);
            this.e3_Register844.Name = "e3_Register844";
            this.e3_Register844.RegisterName = null;
            this.e3_Register844.Size = new System.Drawing.Size(258, 38);
            this.e3_Register844.TabIndex = 67;
            // 
            // e3_Register816
            // 
            this.e3_Register816.Address = ((ushort)(12));
            this.e3_Register816.bit1 = "bit1";
            this.e3_Register816.bit2 = "bit2";
            this.e3_Register816.bit3 = "bit3";
            this.e3_Register816.bit4 = "bit4";
            this.e3_Register816.bit5 = "bit5";
            this.e3_Register816.bit6 = "bit6";
            this.e3_Register816.bit7 = "bit7";
            this.e3_Register816.bit8 = "bit8";
            this.e3_Register816.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register816.Location = new System.Drawing.Point(16, 552);
            this.e3_Register816.Name = "e3_Register816";
            this.e3_Register816.RegisterName = null;
            this.e3_Register816.Size = new System.Drawing.Size(258, 38);
            this.e3_Register816.TabIndex = 43;
            // 
            // e3_Register845
            // 
            this.e3_Register845.Address = ((ushort)(35));
            this.e3_Register845.bit1 = "bit1";
            this.e3_Register845.bit2 = "bit2";
            this.e3_Register845.bit3 = "bit3";
            this.e3_Register845.bit4 = "bit4";
            this.e3_Register845.bit5 = "bit5";
            this.e3_Register845.bit6 = "bit6";
            this.e3_Register845.bit7 = "bit7";
            this.e3_Register845.bit8 = "bit8";
            this.e3_Register845.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register845.Location = new System.Drawing.Point(544, 161);
            this.e3_Register845.Name = "e3_Register845";
            this.e3_Register845.RegisterName = null;
            this.e3_Register845.Size = new System.Drawing.Size(258, 38);
            this.e3_Register845.TabIndex = 66;
            // 
            // e3_Register815
            // 
            this.e3_Register815.Address = ((ushort)(13));
            this.e3_Register815.bit1 = "bit1";
            this.e3_Register815.bit2 = "bit2";
            this.e3_Register815.bit3 = "bit3";
            this.e3_Register815.bit4 = "bit4";
            this.e3_Register815.bit5 = "bit5";
            this.e3_Register815.bit6 = "bit6";
            this.e3_Register815.bit7 = "bit7";
            this.e3_Register815.bit8 = "bit8";
            this.e3_Register815.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register815.Location = new System.Drawing.Point(16, 596);
            this.e3_Register815.Name = "e3_Register815";
            this.e3_Register815.RegisterName = null;
            this.e3_Register815.Size = new System.Drawing.Size(258, 38);
            this.e3_Register815.TabIndex = 44;
            // 
            // e3_Register846
            // 
            this.e3_Register846.Address = ((ushort)(34));
            this.e3_Register846.bit1 = "bit1";
            this.e3_Register846.bit2 = "bit2";
            this.e3_Register846.bit3 = "bit3";
            this.e3_Register846.bit4 = "bit4";
            this.e3_Register846.bit5 = "bit5";
            this.e3_Register846.bit6 = "bit6";
            this.e3_Register846.bit7 = "bit7";
            this.e3_Register846.bit8 = "bit8";
            this.e3_Register846.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register846.Location = new System.Drawing.Point(544, 117);
            this.e3_Register846.Name = "e3_Register846";
            this.e3_Register846.RegisterName = null;
            this.e3_Register846.Size = new System.Drawing.Size(258, 38);
            this.e3_Register846.TabIndex = 65;
            // 
            // e3_Register814
            // 
            this.e3_Register814.Address = ((ushort)(14));
            this.e3_Register814.bit1 = "bit1";
            this.e3_Register814.bit2 = "bit2";
            this.e3_Register814.bit3 = "bit3";
            this.e3_Register814.bit4 = "bit4";
            this.e3_Register814.bit5 = "bit5";
            this.e3_Register814.bit6 = "bit6";
            this.e3_Register814.bit7 = "bit7";
            this.e3_Register814.bit8 = "bit8";
            this.e3_Register814.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register814.Location = new System.Drawing.Point(16, 640);
            this.e3_Register814.Name = "e3_Register814";
            this.e3_Register814.RegisterName = null;
            this.e3_Register814.Size = new System.Drawing.Size(258, 38);
            this.e3_Register814.TabIndex = 45;
            // 
            // e3_Register847
            // 
            this.e3_Register847.Address = ((ushort)(33));
            this.e3_Register847.bit1 = "bit1";
            this.e3_Register847.bit2 = "bit2";
            this.e3_Register847.bit3 = "bit3";
            this.e3_Register847.bit4 = "bit4";
            this.e3_Register847.bit5 = "bit5";
            this.e3_Register847.bit6 = "bit6";
            this.e3_Register847.bit7 = "bit7";
            this.e3_Register847.bit8 = "bit8";
            this.e3_Register847.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register847.Location = new System.Drawing.Point(544, 76);
            this.e3_Register847.Name = "e3_Register847";
            this.e3_Register847.RegisterName = null;
            this.e3_Register847.Size = new System.Drawing.Size(258, 38);
            this.e3_Register847.TabIndex = 64;
            // 
            // e3_Register813
            // 
            this.e3_Register813.Address = ((ushort)(15));
            this.e3_Register813.bit1 = "bit1";
            this.e3_Register813.bit2 = "bit2";
            this.e3_Register813.bit3 = "bit3";
            this.e3_Register813.bit4 = "bit4";
            this.e3_Register813.bit5 = "bit5";
            this.e3_Register813.bit6 = "bit6";
            this.e3_Register813.bit7 = "bit7";
            this.e3_Register813.bit8 = "bit8";
            this.e3_Register813.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register813.Location = new System.Drawing.Point(16, 684);
            this.e3_Register813.Name = "e3_Register813";
            this.e3_Register813.RegisterName = null;
            this.e3_Register813.Size = new System.Drawing.Size(258, 38);
            this.e3_Register813.TabIndex = 46;
            // 
            // e3_Register832
            // 
            this.e3_Register832.Address = ((ushort)(16));
            this.e3_Register832.bit1 = "bit1";
            this.e3_Register832.bit2 = "bit2";
            this.e3_Register832.bit3 = "bit3";
            this.e3_Register832.bit4 = "bit4";
            this.e3_Register832.bit5 = "bit5";
            this.e3_Register832.bit6 = "bit6";
            this.e3_Register832.bit7 = "bit7";
            this.e3_Register832.bit8 = "bit8";
            this.e3_Register832.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register832.Location = new System.Drawing.Point(280, 34);
            this.e3_Register832.Name = "e3_Register832";
            this.e3_Register832.RegisterName = null;
            this.e3_Register832.Size = new System.Drawing.Size(258, 38);
            this.e3_Register832.TabIndex = 47;
            // 
            // e3_Register817
            // 
            this.e3_Register817.Address = ((ushort)(31));
            this.e3_Register817.bit1 = "bit1";
            this.e3_Register817.bit2 = "bit2";
            this.e3_Register817.bit3 = "bit3";
            this.e3_Register817.bit4 = "bit4";
            this.e3_Register817.bit5 = "bit5";
            this.e3_Register817.bit6 = "bit6";
            this.e3_Register817.bit7 = "bit7";
            this.e3_Register817.bit8 = "bit8";
            this.e3_Register817.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register817.Location = new System.Drawing.Point(280, 684);
            this.e3_Register817.Name = "e3_Register817";
            this.e3_Register817.RegisterName = null;
            this.e3_Register817.Size = new System.Drawing.Size(258, 38);
            this.e3_Register817.TabIndex = 62;
            // 
            // e3_Register831
            // 
            this.e3_Register831.Address = ((ushort)(17));
            this.e3_Register831.bit1 = "bit1";
            this.e3_Register831.bit2 = "bit2";
            this.e3_Register831.bit3 = "bit3";
            this.e3_Register831.bit4 = "bit4";
            this.e3_Register831.bit5 = "bit5";
            this.e3_Register831.bit6 = "bit6";
            this.e3_Register831.bit7 = "bit7";
            this.e3_Register831.bit8 = "bit8";
            this.e3_Register831.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register831.Location = new System.Drawing.Point(280, 76);
            this.e3_Register831.Name = "e3_Register831";
            this.e3_Register831.RegisterName = null;
            this.e3_Register831.Size = new System.Drawing.Size(258, 38);
            this.e3_Register831.TabIndex = 48;
            // 
            // e3_Register818
            // 
            this.e3_Register818.Address = ((ushort)(30));
            this.e3_Register818.bit1 = "bit1";
            this.e3_Register818.bit2 = "bit2";
            this.e3_Register818.bit3 = "bit3";
            this.e3_Register818.bit4 = "bit4";
            this.e3_Register818.bit5 = "bit5";
            this.e3_Register818.bit6 = "bit6";
            this.e3_Register818.bit7 = "bit7";
            this.e3_Register818.bit8 = "bit8";
            this.e3_Register818.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register818.Location = new System.Drawing.Point(280, 640);
            this.e3_Register818.Name = "e3_Register818";
            this.e3_Register818.RegisterName = null;
            this.e3_Register818.Size = new System.Drawing.Size(258, 38);
            this.e3_Register818.TabIndex = 61;
            // 
            // e3_Register830
            // 
            this.e3_Register830.Address = ((ushort)(18));
            this.e3_Register830.bit1 = "bit1";
            this.e3_Register830.bit2 = "bit2";
            this.e3_Register830.bit3 = "bit3";
            this.e3_Register830.bit4 = "bit4";
            this.e3_Register830.bit5 = "bit5";
            this.e3_Register830.bit6 = "bit6";
            this.e3_Register830.bit7 = "bit7";
            this.e3_Register830.bit8 = "bit8";
            this.e3_Register830.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register830.Location = new System.Drawing.Point(280, 117);
            this.e3_Register830.Name = "e3_Register830";
            this.e3_Register830.RegisterName = null;
            this.e3_Register830.Size = new System.Drawing.Size(258, 38);
            this.e3_Register830.TabIndex = 49;
            // 
            // e3_Register819
            // 
            this.e3_Register819.Address = ((ushort)(29));
            this.e3_Register819.bit1 = "bit1";
            this.e3_Register819.bit2 = "bit2";
            this.e3_Register819.bit3 = "bit3";
            this.e3_Register819.bit4 = "bit4";
            this.e3_Register819.bit5 = "bit5";
            this.e3_Register819.bit6 = "bit6";
            this.e3_Register819.bit7 = "bit7";
            this.e3_Register819.bit8 = "bit8";
            this.e3_Register819.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register819.Location = new System.Drawing.Point(280, 596);
            this.e3_Register819.Name = "e3_Register819";
            this.e3_Register819.RegisterName = null;
            this.e3_Register819.Size = new System.Drawing.Size(258, 38);
            this.e3_Register819.TabIndex = 60;
            // 
            // e3_Register829
            // 
            this.e3_Register829.Address = ((ushort)(19));
            this.e3_Register829.bit1 = "bit1";
            this.e3_Register829.bit2 = "bit2";
            this.e3_Register829.bit3 = "bit3";
            this.e3_Register829.bit4 = "bit4";
            this.e3_Register829.bit5 = "bit5";
            this.e3_Register829.bit6 = "bit6";
            this.e3_Register829.bit7 = "bit7";
            this.e3_Register829.bit8 = "bit8";
            this.e3_Register829.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register829.Location = new System.Drawing.Point(280, 161);
            this.e3_Register829.Name = "e3_Register829";
            this.e3_Register829.RegisterName = null;
            this.e3_Register829.Size = new System.Drawing.Size(258, 38);
            this.e3_Register829.TabIndex = 50;
            // 
            // e3_Register820
            // 
            this.e3_Register820.Address = ((ushort)(28));
            this.e3_Register820.bit1 = "bit1";
            this.e3_Register820.bit2 = "bit2";
            this.e3_Register820.bit3 = "bit3";
            this.e3_Register820.bit4 = "bit4";
            this.e3_Register820.bit5 = "bit5";
            this.e3_Register820.bit6 = "bit6";
            this.e3_Register820.bit7 = "bit7";
            this.e3_Register820.bit8 = "bit8";
            this.e3_Register820.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register820.Location = new System.Drawing.Point(280, 552);
            this.e3_Register820.Name = "e3_Register820";
            this.e3_Register820.RegisterName = null;
            this.e3_Register820.Size = new System.Drawing.Size(258, 38);
            this.e3_Register820.TabIndex = 59;
            // 
            // e3_Register828
            // 
            this.e3_Register828.Address = ((ushort)(20));
            this.e3_Register828.bit1 = "bit1";
            this.e3_Register828.bit2 = "bit2";
            this.e3_Register828.bit3 = "bit3";
            this.e3_Register828.bit4 = "bit4";
            this.e3_Register828.bit5 = "bit5";
            this.e3_Register828.bit6 = "bit6";
            this.e3_Register828.bit7 = "bit7";
            this.e3_Register828.bit8 = "bit8";
            this.e3_Register828.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register828.Location = new System.Drawing.Point(280, 205);
            this.e3_Register828.Name = "e3_Register828";
            this.e3_Register828.RegisterName = null;
            this.e3_Register828.Size = new System.Drawing.Size(258, 38);
            this.e3_Register828.TabIndex = 51;
            // 
            // e3_Register821
            // 
            this.e3_Register821.Address = ((ushort)(27));
            this.e3_Register821.bit1 = "bit1";
            this.e3_Register821.bit2 = "bit2";
            this.e3_Register821.bit3 = "bit3";
            this.e3_Register821.bit4 = "bit4";
            this.e3_Register821.bit5 = "bit5";
            this.e3_Register821.bit6 = "bit6";
            this.e3_Register821.bit7 = "bit7";
            this.e3_Register821.bit8 = "bit8";
            this.e3_Register821.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register821.Location = new System.Drawing.Point(280, 508);
            this.e3_Register821.Name = "e3_Register821";
            this.e3_Register821.RegisterName = null;
            this.e3_Register821.Size = new System.Drawing.Size(258, 38);
            this.e3_Register821.TabIndex = 58;
            // 
            // e3_Register827
            // 
            this.e3_Register827.Address = ((ushort)(21));
            this.e3_Register827.bit1 = "bit1";
            this.e3_Register827.bit2 = "bit2";
            this.e3_Register827.bit3 = "bit3";
            this.e3_Register827.bit4 = "bit4";
            this.e3_Register827.bit5 = "bit5";
            this.e3_Register827.bit6 = "bit6";
            this.e3_Register827.bit7 = "bit7";
            this.e3_Register827.bit8 = "bit8";
            this.e3_Register827.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register827.Location = new System.Drawing.Point(280, 249);
            this.e3_Register827.Name = "e3_Register827";
            this.e3_Register827.RegisterName = null;
            this.e3_Register827.Size = new System.Drawing.Size(258, 38);
            this.e3_Register827.TabIndex = 52;
            // 
            // e3_Register822
            // 
            this.e3_Register822.Address = ((ushort)(26));
            this.e3_Register822.bit1 = "bit1";
            this.e3_Register822.bit2 = "bit2";
            this.e3_Register822.bit3 = "bit3";
            this.e3_Register822.bit4 = "bit4";
            this.e3_Register822.bit5 = "bit5";
            this.e3_Register822.bit6 = "bit6";
            this.e3_Register822.bit7 = "bit7";
            this.e3_Register822.bit8 = "bit8";
            this.e3_Register822.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register822.Location = new System.Drawing.Point(280, 464);
            this.e3_Register822.Name = "e3_Register822";
            this.e3_Register822.RegisterName = null;
            this.e3_Register822.Size = new System.Drawing.Size(258, 38);
            this.e3_Register822.TabIndex = 57;
            // 
            // e3_Register826
            // 
            this.e3_Register826.Address = ((ushort)(22));
            this.e3_Register826.bit1 = "bit1";
            this.e3_Register826.bit2 = "bit2";
            this.e3_Register826.bit3 = "bit3";
            this.e3_Register826.bit4 = "bit4";
            this.e3_Register826.bit5 = "bit5";
            this.e3_Register826.bit6 = "bit6";
            this.e3_Register826.bit7 = "bit7";
            this.e3_Register826.bit8 = "bit8";
            this.e3_Register826.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register826.Location = new System.Drawing.Point(280, 293);
            this.e3_Register826.Name = "e3_Register826";
            this.e3_Register826.RegisterName = null;
            this.e3_Register826.Size = new System.Drawing.Size(258, 38);
            this.e3_Register826.TabIndex = 53;
            // 
            // e3_Register823
            // 
            this.e3_Register823.Address = ((ushort)(25));
            this.e3_Register823.bit1 = "bit1";
            this.e3_Register823.bit2 = "bit2";
            this.e3_Register823.bit3 = "bit3";
            this.e3_Register823.bit4 = "bit4";
            this.e3_Register823.bit5 = "bit5";
            this.e3_Register823.bit6 = "bit6";
            this.e3_Register823.bit7 = "bit7";
            this.e3_Register823.bit8 = "bit8";
            this.e3_Register823.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register823.Location = new System.Drawing.Point(280, 420);
            this.e3_Register823.Name = "e3_Register823";
            this.e3_Register823.RegisterName = null;
            this.e3_Register823.Size = new System.Drawing.Size(258, 38);
            this.e3_Register823.TabIndex = 56;
            // 
            // e3_Register825
            // 
            this.e3_Register825.Address = ((ushort)(23));
            this.e3_Register825.bit1 = "bit1";
            this.e3_Register825.bit2 = "bit2";
            this.e3_Register825.bit3 = "bit3";
            this.e3_Register825.bit4 = "bit4";
            this.e3_Register825.bit5 = "bit5";
            this.e3_Register825.bit6 = "bit6";
            this.e3_Register825.bit7 = "bit7";
            this.e3_Register825.bit8 = "bit8";
            this.e3_Register825.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register825.Location = new System.Drawing.Point(280, 335);
            this.e3_Register825.Name = "e3_Register825";
            this.e3_Register825.RegisterName = null;
            this.e3_Register825.Size = new System.Drawing.Size(258, 38);
            this.e3_Register825.TabIndex = 54;
            // 
            // e3_Register824
            // 
            this.e3_Register824.Address = ((ushort)(24));
            this.e3_Register824.bit1 = "bit1";
            this.e3_Register824.bit2 = "bit2";
            this.e3_Register824.bit3 = "bit3";
            this.e3_Register824.bit4 = "bit4";
            this.e3_Register824.bit5 = "bit5";
            this.e3_Register824.bit6 = "bit6";
            this.e3_Register824.bit7 = "bit7";
            this.e3_Register824.bit8 = "bit8";
            this.e3_Register824.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.e3_Register824.Location = new System.Drawing.Point(280, 376);
            this.e3_Register824.Name = "e3_Register824";
            this.e3_Register824.RegisterName = null;
            this.e3_Register824.Size = new System.Drawing.Size(258, 38);
            this.e3_Register824.TabIndex = 55;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.reg_1F);
            this.tabPage2.Controls.Add(this.reg_1C);
            this.tabPage2.Controls.Add(this.reg_1E);
            this.tabPage2.Controls.Add(this.reg_00);
            this.tabPage2.Controls.Add(this.reg_1D);
            this.tabPage2.Controls.Add(this.reg_01);
            this.tabPage2.Controls.Add(this.reg_02);
            this.tabPage2.Controls.Add(this.reg_1B);
            this.tabPage2.Controls.Add(this.reg_03);
            this.tabPage2.Controls.Add(this.reg_1A);
            this.tabPage2.Controls.Add(this.reg_04);
            this.tabPage2.Controls.Add(this.reg_19);
            this.tabPage2.Controls.Add(this.reg_05);
            this.tabPage2.Controls.Add(this.reg_18);
            this.tabPage2.Controls.Add(this.reg_06);
            this.tabPage2.Controls.Add(this.reg_17);
            this.tabPage2.Controls.Add(this.reg_07);
            this.tabPage2.Controls.Add(this.reg_16);
            this.tabPage2.Controls.Add(this.reg_08);
            this.tabPage2.Controls.Add(this.reg_15);
            this.tabPage2.Controls.Add(this.reg_09);
            this.tabPage2.Controls.Add(this.reg_14);
            this.tabPage2.Controls.Add(this.reg_0A);
            this.tabPage2.Controls.Add(this.reg_13);
            this.tabPage2.Controls.Add(this.reg_0B);
            this.tabPage2.Controls.Add(this.reg_12);
            this.tabPage2.Controls.Add(this.reg_0C);
            this.tabPage2.Controls.Add(this.reg_11);
            this.tabPage2.Controls.Add(this.reg_0D);
            this.tabPage2.Controls.Add(this.reg_10);
            this.tabPage2.Controls.Add(this.reg_0E);
            this.tabPage2.Controls.Add(this.reg_0F);
            this.tabPage2.Controls.Add(this.tabRegisters);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(192, 74);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Acclaim Battery Pack - Registers";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // reg_1F
            // 
            this.reg_1F.Address = null;
            this.reg_1F.bit0 = "bit0";
            this.reg_1F.bit1 = "bit1";
            this.reg_1F.bit10 = "bit10";
            this.reg_1F.bit11 = "bit11";
            this.reg_1F.bit12 = "bit12";
            this.reg_1F.bit13 = "bit13";
            this.reg_1F.bit14 = "bit14";
            this.reg_1F.bit15 = "bit15";
            this.reg_1F.bit16 = "bit16";
            this.reg_1F.bit17 = "bit17";
            this.reg_1F.bit2 = "bit2";
            this.reg_1F.bit3 = "bit3";
            this.reg_1F.bit4 = "bit4";
            this.reg_1F.bit5 = "bit5";
            this.reg_1F.bit6 = "bit6";
            this.reg_1F.bit7 = "bit7";
            this.reg_1F.bit8 = "bit8";
            this.reg_1F.bit9 = "bit9";
            this.reg_1F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_1F.Location = new System.Drawing.Point(958, 691);
            this.reg_1F.Name = "reg_1F";
            this.reg_1F.RegisterName = null;
            this.reg_1F.Size = new System.Drawing.Size(890, 38);
            this.reg_1F.TabIndex = 149;
            // 
            // reg_1C
            // 
            this.reg_1C.Address = null;
            this.reg_1C.bit0 = "bit0";
            this.reg_1C.bit1 = "bit1";
            this.reg_1C.bit10 = "bit10";
            this.reg_1C.bit11 = "bit11";
            this.reg_1C.bit12 = "bit12";
            this.reg_1C.bit13 = "bit13";
            this.reg_1C.bit14 = "bit14";
            this.reg_1C.bit15 = "bit15";
            this.reg_1C.bit16 = "bit16";
            this.reg_1C.bit17 = "bit17";
            this.reg_1C.bit2 = "bit2";
            this.reg_1C.bit3 = "bit3";
            this.reg_1C.bit4 = "bit4";
            this.reg_1C.bit5 = "bit5";
            this.reg_1C.bit6 = "bit6";
            this.reg_1C.bit7 = "bit7";
            this.reg_1C.bit8 = "bit8";
            this.reg_1C.bit9 = "bit9";
            this.reg_1C.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_1C.Location = new System.Drawing.Point(958, 559);
            this.reg_1C.Name = "reg_1C";
            this.reg_1C.RegisterName = null;
            this.reg_1C.Size = new System.Drawing.Size(890, 38);
            this.reg_1C.TabIndex = 146;
            // 
            // reg_1E
            // 
            this.reg_1E.Address = null;
            this.reg_1E.bit0 = "bit0";
            this.reg_1E.bit1 = "bit1";
            this.reg_1E.bit10 = "bit10";
            this.reg_1E.bit11 = "bit11";
            this.reg_1E.bit12 = "bit12";
            this.reg_1E.bit13 = "bit13";
            this.reg_1E.bit14 = "bit14";
            this.reg_1E.bit15 = "bit15";
            this.reg_1E.bit16 = "bit16";
            this.reg_1E.bit17 = "bit17";
            this.reg_1E.bit2 = "bit2";
            this.reg_1E.bit3 = "bit3";
            this.reg_1E.bit4 = "bit4";
            this.reg_1E.bit5 = "bit5";
            this.reg_1E.bit6 = "bit6";
            this.reg_1E.bit7 = "bit7";
            this.reg_1E.bit8 = "bit8";
            this.reg_1E.bit9 = "bit9";
            this.reg_1E.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_1E.Location = new System.Drawing.Point(958, 647);
            this.reg_1E.Name = "reg_1E";
            this.reg_1E.RegisterName = null;
            this.reg_1E.Size = new System.Drawing.Size(890, 38);
            this.reg_1E.TabIndex = 148;
            // 
            // reg_00
            // 
            this.reg_00.Address = "00000000";
            this.reg_00.bit0 = "bit0";
            this.reg_00.bit1 = "bit1";
            this.reg_00.bit10 = "bit10";
            this.reg_00.bit11 = "bit11";
            this.reg_00.bit12 = "bit12";
            this.reg_00.bit13 = "bit13";
            this.reg_00.bit14 = "bit14";
            this.reg_00.bit15 = "bit15";
            this.reg_00.bit16 = "bit16";
            this.reg_00.bit17 = "bit17";
            this.reg_00.bit2 = "bit2";
            this.reg_00.bit3 = "bit3";
            this.reg_00.bit4 = "bit4";
            this.reg_00.bit5 = "bit5";
            this.reg_00.bit6 = "bit6";
            this.reg_00.bit7 = "bit7";
            this.reg_00.bit8 = "bit8";
            this.reg_00.bit9 = "bit9";
            this.reg_00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_00.Location = new System.Drawing.Point(20, 31);
            this.reg_00.Name = "reg_00";
            this.reg_00.RegisterName = "Test Register Name";
            this.reg_00.Size = new System.Drawing.Size(890, 38);
            this.reg_00.TabIndex = 118;
            // 
            // reg_1D
            // 
            this.reg_1D.Address = null;
            this.reg_1D.bit0 = "bit0";
            this.reg_1D.bit1 = "bit1";
            this.reg_1D.bit10 = "bit10";
            this.reg_1D.bit11 = "bit11";
            this.reg_1D.bit12 = "bit12";
            this.reg_1D.bit13 = "bit13";
            this.reg_1D.bit14 = "bit14";
            this.reg_1D.bit15 = "bit15";
            this.reg_1D.bit16 = "bit16";
            this.reg_1D.bit17 = "bit17";
            this.reg_1D.bit2 = "bit2";
            this.reg_1D.bit3 = "bit3";
            this.reg_1D.bit4 = "bit4";
            this.reg_1D.bit5 = "bit5";
            this.reg_1D.bit6 = "bit6";
            this.reg_1D.bit7 = "bit7";
            this.reg_1D.bit8 = "bit8";
            this.reg_1D.bit9 = "bit9";
            this.reg_1D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_1D.Location = new System.Drawing.Point(958, 603);
            this.reg_1D.Name = "reg_1D";
            this.reg_1D.RegisterName = null;
            this.reg_1D.Size = new System.Drawing.Size(890, 38);
            this.reg_1D.TabIndex = 147;
            // 
            // reg_01
            // 
            this.reg_01.Address = null;
            this.reg_01.bit0 = "bit0";
            this.reg_01.bit1 = "bit1";
            this.reg_01.bit10 = "bit10";
            this.reg_01.bit11 = "bit11";
            this.reg_01.bit12 = "bit12";
            this.reg_01.bit13 = "bit13";
            this.reg_01.bit14 = "bit14";
            this.reg_01.bit15 = "bit15";
            this.reg_01.bit16 = "bit16";
            this.reg_01.bit17 = "bit17";
            this.reg_01.bit2 = "bit2";
            this.reg_01.bit3 = "bit3";
            this.reg_01.bit4 = "bit4";
            this.reg_01.bit5 = "bit5";
            this.reg_01.bit6 = "bit6";
            this.reg_01.bit7 = "bit7";
            this.reg_01.bit8 = "bit8";
            this.reg_01.bit9 = "bit9";
            this.reg_01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_01.Location = new System.Drawing.Point(20, 75);
            this.reg_01.Name = "reg_01";
            this.reg_01.RegisterName = null;
            this.reg_01.Size = new System.Drawing.Size(890, 38);
            this.reg_01.TabIndex = 119;
            // 
            // reg_02
            // 
            this.reg_02.Address = null;
            this.reg_02.bit0 = "bit0";
            this.reg_02.bit1 = "bit1";
            this.reg_02.bit10 = "bit10";
            this.reg_02.bit11 = "bit11";
            this.reg_02.bit12 = "bit12";
            this.reg_02.bit13 = "bit13";
            this.reg_02.bit14 = "bit14";
            this.reg_02.bit15 = "bit15";
            this.reg_02.bit16 = "bit16";
            this.reg_02.bit17 = "bit17";
            this.reg_02.bit2 = "bit2";
            this.reg_02.bit3 = "bit3";
            this.reg_02.bit4 = "bit4";
            this.reg_02.bit5 = "bit5";
            this.reg_02.bit6 = "bit6";
            this.reg_02.bit7 = "bit7";
            this.reg_02.bit8 = "bit8";
            this.reg_02.bit9 = "bit9";
            this.reg_02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_02.Location = new System.Drawing.Point(20, 119);
            this.reg_02.Name = "reg_02";
            this.reg_02.RegisterName = null;
            this.reg_02.Size = new System.Drawing.Size(890, 38);
            this.reg_02.TabIndex = 120;
            // 
            // reg_1B
            // 
            this.reg_1B.Address = null;
            this.reg_1B.bit0 = "bit0";
            this.reg_1B.bit1 = "bit1";
            this.reg_1B.bit10 = "bit10";
            this.reg_1B.bit11 = "bit11";
            this.reg_1B.bit12 = "bit12";
            this.reg_1B.bit13 = "bit13";
            this.reg_1B.bit14 = "bit14";
            this.reg_1B.bit15 = "bit15";
            this.reg_1B.bit16 = "bit16";
            this.reg_1B.bit17 = "bit17";
            this.reg_1B.bit2 = "bit2";
            this.reg_1B.bit3 = "bit3";
            this.reg_1B.bit4 = "bit4";
            this.reg_1B.bit5 = "bit5";
            this.reg_1B.bit6 = "bit6";
            this.reg_1B.bit7 = "bit7";
            this.reg_1B.bit8 = "bit8";
            this.reg_1B.bit9 = "bit9";
            this.reg_1B.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_1B.Location = new System.Drawing.Point(958, 515);
            this.reg_1B.Name = "reg_1B";
            this.reg_1B.RegisterName = null;
            this.reg_1B.Size = new System.Drawing.Size(890, 38);
            this.reg_1B.TabIndex = 145;
            // 
            // reg_03
            // 
            this.reg_03.Address = null;
            this.reg_03.bit0 = "bit0";
            this.reg_03.bit1 = "bit1";
            this.reg_03.bit10 = "bit10";
            this.reg_03.bit11 = "bit11";
            this.reg_03.bit12 = "bit12";
            this.reg_03.bit13 = "bit13";
            this.reg_03.bit14 = "bit14";
            this.reg_03.bit15 = "bit15";
            this.reg_03.bit16 = "bit16";
            this.reg_03.bit17 = "bit17";
            this.reg_03.bit2 = "bit2";
            this.reg_03.bit3 = "bit3";
            this.reg_03.bit4 = "bit4";
            this.reg_03.bit5 = "bit5";
            this.reg_03.bit6 = "bit6";
            this.reg_03.bit7 = "bit7";
            this.reg_03.bit8 = "bit8";
            this.reg_03.bit9 = "bit9";
            this.reg_03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_03.Location = new System.Drawing.Point(20, 163);
            this.reg_03.Name = "reg_03";
            this.reg_03.RegisterName = null;
            this.reg_03.Size = new System.Drawing.Size(890, 38);
            this.reg_03.TabIndex = 121;
            // 
            // reg_1A
            // 
            this.reg_1A.Address = null;
            this.reg_1A.bit0 = "bit0";
            this.reg_1A.bit1 = "bit1";
            this.reg_1A.bit10 = "bit10";
            this.reg_1A.bit11 = "bit11";
            this.reg_1A.bit12 = "bit12";
            this.reg_1A.bit13 = "bit13";
            this.reg_1A.bit14 = "bit14";
            this.reg_1A.bit15 = "bit15";
            this.reg_1A.bit16 = "bit16";
            this.reg_1A.bit17 = "bit17";
            this.reg_1A.bit2 = "bit2";
            this.reg_1A.bit3 = "bit3";
            this.reg_1A.bit4 = "bit4";
            this.reg_1A.bit5 = "bit5";
            this.reg_1A.bit6 = "bit6";
            this.reg_1A.bit7 = "bit7";
            this.reg_1A.bit8 = "bit8";
            this.reg_1A.bit9 = "bit9";
            this.reg_1A.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_1A.Location = new System.Drawing.Point(958, 471);
            this.reg_1A.Name = "reg_1A";
            this.reg_1A.RegisterName = null;
            this.reg_1A.Size = new System.Drawing.Size(890, 38);
            this.reg_1A.TabIndex = 144;
            // 
            // reg_04
            // 
            this.reg_04.Address = null;
            this.reg_04.bit0 = "bit0";
            this.reg_04.bit1 = "bit1";
            this.reg_04.bit10 = "bit10";
            this.reg_04.bit11 = "bit11";
            this.reg_04.bit12 = "bit12";
            this.reg_04.bit13 = "bit13";
            this.reg_04.bit14 = "bit14";
            this.reg_04.bit15 = "bit15";
            this.reg_04.bit16 = "bit16";
            this.reg_04.bit17 = "bit17";
            this.reg_04.bit2 = "bit2";
            this.reg_04.bit3 = "bit3";
            this.reg_04.bit4 = "bit4";
            this.reg_04.bit5 = "bit5";
            this.reg_04.bit6 = "bit6";
            this.reg_04.bit7 = "bit7";
            this.reg_04.bit8 = "bit8";
            this.reg_04.bit9 = "bit9";
            this.reg_04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_04.Location = new System.Drawing.Point(20, 207);
            this.reg_04.Name = "reg_04";
            this.reg_04.RegisterName = null;
            this.reg_04.Size = new System.Drawing.Size(890, 38);
            this.reg_04.TabIndex = 122;
            // 
            // reg_19
            // 
            this.reg_19.Address = null;
            this.reg_19.bit0 = "bit0";
            this.reg_19.bit1 = "bit1";
            this.reg_19.bit10 = "bit10";
            this.reg_19.bit11 = "bit11";
            this.reg_19.bit12 = "bit12";
            this.reg_19.bit13 = "bit13";
            this.reg_19.bit14 = "bit14";
            this.reg_19.bit15 = "bit15";
            this.reg_19.bit16 = "bit16";
            this.reg_19.bit17 = "bit17";
            this.reg_19.bit2 = "bit2";
            this.reg_19.bit3 = "bit3";
            this.reg_19.bit4 = "bit4";
            this.reg_19.bit5 = "bit5";
            this.reg_19.bit6 = "bit6";
            this.reg_19.bit7 = "bit7";
            this.reg_19.bit8 = "bit8";
            this.reg_19.bit9 = "bit9";
            this.reg_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_19.Location = new System.Drawing.Point(958, 427);
            this.reg_19.Name = "reg_19";
            this.reg_19.RegisterName = null;
            this.reg_19.Size = new System.Drawing.Size(890, 38);
            this.reg_19.TabIndex = 143;
            // 
            // reg_05
            // 
            this.reg_05.Address = null;
            this.reg_05.bit0 = "bit0";
            this.reg_05.bit1 = "bit1";
            this.reg_05.bit10 = "bit10";
            this.reg_05.bit11 = "bit11";
            this.reg_05.bit12 = "bit12";
            this.reg_05.bit13 = "bit13";
            this.reg_05.bit14 = "bit14";
            this.reg_05.bit15 = "bit15";
            this.reg_05.bit16 = "bit16";
            this.reg_05.bit17 = "bit17";
            this.reg_05.bit2 = "bit2";
            this.reg_05.bit3 = "bit3";
            this.reg_05.bit4 = "bit4";
            this.reg_05.bit5 = "bit5";
            this.reg_05.bit6 = "bit6";
            this.reg_05.bit7 = "bit7";
            this.reg_05.bit8 = "bit8";
            this.reg_05.bit9 = "bit9";
            this.reg_05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_05.Location = new System.Drawing.Point(20, 251);
            this.reg_05.Name = "reg_05";
            this.reg_05.RegisterName = null;
            this.reg_05.Size = new System.Drawing.Size(890, 38);
            this.reg_05.TabIndex = 123;
            // 
            // reg_18
            // 
            this.reg_18.Address = null;
            this.reg_18.bit0 = "bit0";
            this.reg_18.bit1 = "bit1";
            this.reg_18.bit10 = "bit10";
            this.reg_18.bit11 = "bit11";
            this.reg_18.bit12 = "bit12";
            this.reg_18.bit13 = "bit13";
            this.reg_18.bit14 = "bit14";
            this.reg_18.bit15 = "bit15";
            this.reg_18.bit16 = "bit16";
            this.reg_18.bit17 = "bit17";
            this.reg_18.bit2 = "bit2";
            this.reg_18.bit3 = "bit3";
            this.reg_18.bit4 = "bit4";
            this.reg_18.bit5 = "bit5";
            this.reg_18.bit6 = "bit6";
            this.reg_18.bit7 = "bit7";
            this.reg_18.bit8 = "bit8";
            this.reg_18.bit9 = "bit9";
            this.reg_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_18.Location = new System.Drawing.Point(958, 383);
            this.reg_18.Name = "reg_18";
            this.reg_18.RegisterName = null;
            this.reg_18.Size = new System.Drawing.Size(890, 38);
            this.reg_18.TabIndex = 142;
            // 
            // reg_06
            // 
            this.reg_06.Address = null;
            this.reg_06.bit0 = "bit0";
            this.reg_06.bit1 = "bit1";
            this.reg_06.bit10 = "bit10";
            this.reg_06.bit11 = "bit11";
            this.reg_06.bit12 = "bit12";
            this.reg_06.bit13 = "bit13";
            this.reg_06.bit14 = "bit14";
            this.reg_06.bit15 = "bit15";
            this.reg_06.bit16 = "bit16";
            this.reg_06.bit17 = "bit17";
            this.reg_06.bit2 = "bit2";
            this.reg_06.bit3 = "bit3";
            this.reg_06.bit4 = "bit4";
            this.reg_06.bit5 = "bit5";
            this.reg_06.bit6 = "bit6";
            this.reg_06.bit7 = "bit7";
            this.reg_06.bit8 = "bit8";
            this.reg_06.bit9 = "bit9";
            this.reg_06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_06.Location = new System.Drawing.Point(20, 295);
            this.reg_06.Name = "reg_06";
            this.reg_06.RegisterName = null;
            this.reg_06.Size = new System.Drawing.Size(890, 38);
            this.reg_06.TabIndex = 124;
            // 
            // reg_17
            // 
            this.reg_17.Address = null;
            this.reg_17.bit0 = "bit0";
            this.reg_17.bit1 = "bit1";
            this.reg_17.bit10 = "bit10";
            this.reg_17.bit11 = "bit11";
            this.reg_17.bit12 = "bit12";
            this.reg_17.bit13 = "bit13";
            this.reg_17.bit14 = "bit14";
            this.reg_17.bit15 = "bit15";
            this.reg_17.bit16 = "bit16";
            this.reg_17.bit17 = "bit17";
            this.reg_17.bit2 = "bit2";
            this.reg_17.bit3 = "bit3";
            this.reg_17.bit4 = "bit4";
            this.reg_17.bit5 = "bit5";
            this.reg_17.bit6 = "bit6";
            this.reg_17.bit7 = "bit7";
            this.reg_17.bit8 = "bit8";
            this.reg_17.bit9 = "bit9";
            this.reg_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_17.Location = new System.Drawing.Point(958, 339);
            this.reg_17.Name = "reg_17";
            this.reg_17.RegisterName = null;
            this.reg_17.Size = new System.Drawing.Size(890, 38);
            this.reg_17.TabIndex = 141;
            // 
            // reg_07
            // 
            this.reg_07.Address = null;
            this.reg_07.bit0 = "bit0";
            this.reg_07.bit1 = "bit1";
            this.reg_07.bit10 = "bit10";
            this.reg_07.bit11 = "bit11";
            this.reg_07.bit12 = "bit12";
            this.reg_07.bit13 = "bit13";
            this.reg_07.bit14 = "bit14";
            this.reg_07.bit15 = "bit15";
            this.reg_07.bit16 = "bit16";
            this.reg_07.bit17 = "bit17";
            this.reg_07.bit2 = "bit2";
            this.reg_07.bit3 = "bit3";
            this.reg_07.bit4 = "bit4";
            this.reg_07.bit5 = "bit5";
            this.reg_07.bit6 = "bit6";
            this.reg_07.bit7 = "bit7";
            this.reg_07.bit8 = "bit8";
            this.reg_07.bit9 = "bit9";
            this.reg_07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_07.Location = new System.Drawing.Point(20, 339);
            this.reg_07.Name = "reg_07";
            this.reg_07.RegisterName = null;
            this.reg_07.Size = new System.Drawing.Size(890, 38);
            this.reg_07.TabIndex = 125;
            // 
            // reg_16
            // 
            this.reg_16.Address = null;
            this.reg_16.bit0 = "bit0";
            this.reg_16.bit1 = "bit1";
            this.reg_16.bit10 = "bit10";
            this.reg_16.bit11 = "bit11";
            this.reg_16.bit12 = "bit12";
            this.reg_16.bit13 = "bit13";
            this.reg_16.bit14 = "bit14";
            this.reg_16.bit15 = "bit15";
            this.reg_16.bit16 = "bit16";
            this.reg_16.bit17 = "bit17";
            this.reg_16.bit2 = "bit2";
            this.reg_16.bit3 = "bit3";
            this.reg_16.bit4 = "bit4";
            this.reg_16.bit5 = "bit5";
            this.reg_16.bit6 = "bit6";
            this.reg_16.bit7 = "bit7";
            this.reg_16.bit8 = "bit8";
            this.reg_16.bit9 = "bit9";
            this.reg_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_16.Location = new System.Drawing.Point(958, 295);
            this.reg_16.Name = "reg_16";
            this.reg_16.RegisterName = null;
            this.reg_16.Size = new System.Drawing.Size(890, 38);
            this.reg_16.TabIndex = 140;
            // 
            // reg_08
            // 
            this.reg_08.Address = null;
            this.reg_08.bit0 = "bit0";
            this.reg_08.bit1 = "bit1";
            this.reg_08.bit10 = "bit10";
            this.reg_08.bit11 = "bit11";
            this.reg_08.bit12 = "bit12";
            this.reg_08.bit13 = "bit13";
            this.reg_08.bit14 = "bit14";
            this.reg_08.bit15 = "bit15";
            this.reg_08.bit16 = "bit16";
            this.reg_08.bit17 = "bit17";
            this.reg_08.bit2 = "bit2";
            this.reg_08.bit3 = "bit3";
            this.reg_08.bit4 = "bit4";
            this.reg_08.bit5 = "bit5";
            this.reg_08.bit6 = "bit6";
            this.reg_08.bit7 = "bit7";
            this.reg_08.bit8 = "bit8";
            this.reg_08.bit9 = "bit9";
            this.reg_08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_08.Location = new System.Drawing.Point(20, 383);
            this.reg_08.Name = "reg_08";
            this.reg_08.RegisterName = null;
            this.reg_08.Size = new System.Drawing.Size(890, 38);
            this.reg_08.TabIndex = 126;
            // 
            // reg_15
            // 
            this.reg_15.Address = null;
            this.reg_15.bit0 = "bit0";
            this.reg_15.bit1 = "bit1";
            this.reg_15.bit10 = "bit10";
            this.reg_15.bit11 = "bit11";
            this.reg_15.bit12 = "bit12";
            this.reg_15.bit13 = "bit13";
            this.reg_15.bit14 = "bit14";
            this.reg_15.bit15 = "bit15";
            this.reg_15.bit16 = "bit16";
            this.reg_15.bit17 = "bit17";
            this.reg_15.bit2 = "bit2";
            this.reg_15.bit3 = "bit3";
            this.reg_15.bit4 = "bit4";
            this.reg_15.bit5 = "bit5";
            this.reg_15.bit6 = "bit6";
            this.reg_15.bit7 = "bit7";
            this.reg_15.bit8 = "bit8";
            this.reg_15.bit9 = "bit9";
            this.reg_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_15.Location = new System.Drawing.Point(958, 251);
            this.reg_15.Name = "reg_15";
            this.reg_15.RegisterName = null;
            this.reg_15.Size = new System.Drawing.Size(890, 38);
            this.reg_15.TabIndex = 139;
            // 
            // reg_09
            // 
            this.reg_09.Address = null;
            this.reg_09.bit0 = "bit0";
            this.reg_09.bit1 = "bit1";
            this.reg_09.bit10 = "bit10";
            this.reg_09.bit11 = "bit11";
            this.reg_09.bit12 = "bit12";
            this.reg_09.bit13 = "bit13";
            this.reg_09.bit14 = "bit14";
            this.reg_09.bit15 = "bit15";
            this.reg_09.bit16 = "bit16";
            this.reg_09.bit17 = "bit17";
            this.reg_09.bit2 = "bit2";
            this.reg_09.bit3 = "bit3";
            this.reg_09.bit4 = "bit4";
            this.reg_09.bit5 = "bit5";
            this.reg_09.bit6 = "bit6";
            this.reg_09.bit7 = "bit7";
            this.reg_09.bit8 = "bit8";
            this.reg_09.bit9 = "bit9";
            this.reg_09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_09.Location = new System.Drawing.Point(20, 427);
            this.reg_09.Name = "reg_09";
            this.reg_09.RegisterName = null;
            this.reg_09.Size = new System.Drawing.Size(890, 38);
            this.reg_09.TabIndex = 127;
            // 
            // reg_14
            // 
            this.reg_14.Address = null;
            this.reg_14.bit0 = "bit0";
            this.reg_14.bit1 = "bit1";
            this.reg_14.bit10 = "bit10";
            this.reg_14.bit11 = "bit11";
            this.reg_14.bit12 = "bit12";
            this.reg_14.bit13 = "bit13";
            this.reg_14.bit14 = "bit14";
            this.reg_14.bit15 = "bit15";
            this.reg_14.bit16 = "bit16";
            this.reg_14.bit17 = "bit17";
            this.reg_14.bit2 = "bit2";
            this.reg_14.bit3 = "bit3";
            this.reg_14.bit4 = "bit4";
            this.reg_14.bit5 = "bit5";
            this.reg_14.bit6 = "bit6";
            this.reg_14.bit7 = "bit7";
            this.reg_14.bit8 = "bit8";
            this.reg_14.bit9 = "bit9";
            this.reg_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_14.Location = new System.Drawing.Point(958, 207);
            this.reg_14.Name = "reg_14";
            this.reg_14.RegisterName = null;
            this.reg_14.Size = new System.Drawing.Size(890, 38);
            this.reg_14.TabIndex = 138;
            // 
            // reg_0A
            // 
            this.reg_0A.Address = null;
            this.reg_0A.bit0 = "bit0";
            this.reg_0A.bit1 = "bit1";
            this.reg_0A.bit10 = "bit10";
            this.reg_0A.bit11 = "bit11";
            this.reg_0A.bit12 = "bit12";
            this.reg_0A.bit13 = "bit13";
            this.reg_0A.bit14 = "bit14";
            this.reg_0A.bit15 = "bit15";
            this.reg_0A.bit16 = "bit16";
            this.reg_0A.bit17 = "bit17";
            this.reg_0A.bit2 = "bit2";
            this.reg_0A.bit3 = "bit3";
            this.reg_0A.bit4 = "bit4";
            this.reg_0A.bit5 = "bit5";
            this.reg_0A.bit6 = "bit6";
            this.reg_0A.bit7 = "bit7";
            this.reg_0A.bit8 = "bit8";
            this.reg_0A.bit9 = "bit9";
            this.reg_0A.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_0A.Location = new System.Drawing.Point(20, 471);
            this.reg_0A.Name = "reg_0A";
            this.reg_0A.RegisterName = null;
            this.reg_0A.Size = new System.Drawing.Size(890, 38);
            this.reg_0A.TabIndex = 128;
            // 
            // reg_13
            // 
            this.reg_13.Address = null;
            this.reg_13.bit0 = "bit0";
            this.reg_13.bit1 = "bit1";
            this.reg_13.bit10 = "bit10";
            this.reg_13.bit11 = "bit11";
            this.reg_13.bit12 = "bit12";
            this.reg_13.bit13 = "bit13";
            this.reg_13.bit14 = "bit14";
            this.reg_13.bit15 = "bit15";
            this.reg_13.bit16 = "bit16";
            this.reg_13.bit17 = "bit17";
            this.reg_13.bit2 = "bit2";
            this.reg_13.bit3 = "bit3";
            this.reg_13.bit4 = "bit4";
            this.reg_13.bit5 = "bit5";
            this.reg_13.bit6 = "bit6";
            this.reg_13.bit7 = "bit7";
            this.reg_13.bit8 = "bit8";
            this.reg_13.bit9 = "bit9";
            this.reg_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_13.Location = new System.Drawing.Point(958, 163);
            this.reg_13.Name = "reg_13";
            this.reg_13.RegisterName = null;
            this.reg_13.Size = new System.Drawing.Size(890, 38);
            this.reg_13.TabIndex = 137;
            // 
            // reg_0B
            // 
            this.reg_0B.Address = null;
            this.reg_0B.bit0 = "bit0";
            this.reg_0B.bit1 = "bit1";
            this.reg_0B.bit10 = "bit10";
            this.reg_0B.bit11 = "bit11";
            this.reg_0B.bit12 = "bit12";
            this.reg_0B.bit13 = "bit13";
            this.reg_0B.bit14 = "bit14";
            this.reg_0B.bit15 = "bit15";
            this.reg_0B.bit16 = "bit16";
            this.reg_0B.bit17 = "bit17";
            this.reg_0B.bit2 = "bit2";
            this.reg_0B.bit3 = "bit3";
            this.reg_0B.bit4 = "bit4";
            this.reg_0B.bit5 = "bit5";
            this.reg_0B.bit6 = "bit6";
            this.reg_0B.bit7 = "bit7";
            this.reg_0B.bit8 = "bit8";
            this.reg_0B.bit9 = "bit9";
            this.reg_0B.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_0B.Location = new System.Drawing.Point(20, 515);
            this.reg_0B.Name = "reg_0B";
            this.reg_0B.RegisterName = null;
            this.reg_0B.Size = new System.Drawing.Size(890, 38);
            this.reg_0B.TabIndex = 129;
            // 
            // reg_12
            // 
            this.reg_12.Address = null;
            this.reg_12.bit0 = "bit0";
            this.reg_12.bit1 = "bit1";
            this.reg_12.bit10 = "bit10";
            this.reg_12.bit11 = "bit11";
            this.reg_12.bit12 = "bit12";
            this.reg_12.bit13 = "bit13";
            this.reg_12.bit14 = "bit14";
            this.reg_12.bit15 = "bit15";
            this.reg_12.bit16 = "bit16";
            this.reg_12.bit17 = "bit17";
            this.reg_12.bit2 = "bit2";
            this.reg_12.bit3 = "bit3";
            this.reg_12.bit4 = "bit4";
            this.reg_12.bit5 = "bit5";
            this.reg_12.bit6 = "bit6";
            this.reg_12.bit7 = "bit7";
            this.reg_12.bit8 = "bit8";
            this.reg_12.bit9 = "bit9";
            this.reg_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_12.Location = new System.Drawing.Point(958, 119);
            this.reg_12.Name = "reg_12";
            this.reg_12.RegisterName = null;
            this.reg_12.Size = new System.Drawing.Size(890, 38);
            this.reg_12.TabIndex = 136;
            // 
            // reg_0C
            // 
            this.reg_0C.Address = null;
            this.reg_0C.bit0 = "bit0";
            this.reg_0C.bit1 = "bit1";
            this.reg_0C.bit10 = "bit10";
            this.reg_0C.bit11 = "bit11";
            this.reg_0C.bit12 = "bit12";
            this.reg_0C.bit13 = "bit13";
            this.reg_0C.bit14 = "bit14";
            this.reg_0C.bit15 = "bit15";
            this.reg_0C.bit16 = "bit16";
            this.reg_0C.bit17 = "bit17";
            this.reg_0C.bit2 = "bit2";
            this.reg_0C.bit3 = "bit3";
            this.reg_0C.bit4 = "bit4";
            this.reg_0C.bit5 = "bit5";
            this.reg_0C.bit6 = "bit6";
            this.reg_0C.bit7 = "bit7";
            this.reg_0C.bit8 = "bit8";
            this.reg_0C.bit9 = "bit9";
            this.reg_0C.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_0C.Location = new System.Drawing.Point(20, 559);
            this.reg_0C.Name = "reg_0C";
            this.reg_0C.RegisterName = null;
            this.reg_0C.Size = new System.Drawing.Size(890, 38);
            this.reg_0C.TabIndex = 130;
            // 
            // reg_11
            // 
            this.reg_11.Address = null;
            this.reg_11.bit0 = "bit0";
            this.reg_11.bit1 = "bit1";
            this.reg_11.bit10 = "bit10";
            this.reg_11.bit11 = "bit11";
            this.reg_11.bit12 = "bit12";
            this.reg_11.bit13 = "bit13";
            this.reg_11.bit14 = "bit14";
            this.reg_11.bit15 = "bit15";
            this.reg_11.bit16 = "bit16";
            this.reg_11.bit17 = "bit17";
            this.reg_11.bit2 = "bit2";
            this.reg_11.bit3 = "bit3";
            this.reg_11.bit4 = "bit4";
            this.reg_11.bit5 = "bit5";
            this.reg_11.bit6 = "bit6";
            this.reg_11.bit7 = "bit7";
            this.reg_11.bit8 = "bit8";
            this.reg_11.bit9 = "bit9";
            this.reg_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_11.Location = new System.Drawing.Point(958, 75);
            this.reg_11.Name = "reg_11";
            this.reg_11.RegisterName = null;
            this.reg_11.Size = new System.Drawing.Size(890, 38);
            this.reg_11.TabIndex = 135;
            // 
            // reg_0D
            // 
            this.reg_0D.Address = null;
            this.reg_0D.bit0 = "bit0";
            this.reg_0D.bit1 = "bit1";
            this.reg_0D.bit10 = "bit10";
            this.reg_0D.bit11 = "bit11";
            this.reg_0D.bit12 = "bit12";
            this.reg_0D.bit13 = "bit13";
            this.reg_0D.bit14 = "bit14";
            this.reg_0D.bit15 = "bit15";
            this.reg_0D.bit16 = "bit16";
            this.reg_0D.bit17 = "bit17";
            this.reg_0D.bit2 = "bit2";
            this.reg_0D.bit3 = "bit3";
            this.reg_0D.bit4 = "bit4";
            this.reg_0D.bit5 = "bit5";
            this.reg_0D.bit6 = "bit6";
            this.reg_0D.bit7 = "bit7";
            this.reg_0D.bit8 = "bit8";
            this.reg_0D.bit9 = "bit9";
            this.reg_0D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_0D.Location = new System.Drawing.Point(20, 603);
            this.reg_0D.Name = "reg_0D";
            this.reg_0D.RegisterName = null;
            this.reg_0D.Size = new System.Drawing.Size(890, 38);
            this.reg_0D.TabIndex = 131;
            // 
            // reg_10
            // 
            this.reg_10.Address = null;
            this.reg_10.bit0 = "bit0";
            this.reg_10.bit1 = "bit1";
            this.reg_10.bit10 = "bit10";
            this.reg_10.bit11 = "bit11";
            this.reg_10.bit12 = "bit12";
            this.reg_10.bit13 = "bit13";
            this.reg_10.bit14 = "bit14";
            this.reg_10.bit15 = "bit15";
            this.reg_10.bit16 = "bit16";
            this.reg_10.bit17 = "bit17";
            this.reg_10.bit2 = "bit2";
            this.reg_10.bit3 = "bit3";
            this.reg_10.bit4 = "bit4";
            this.reg_10.bit5 = "bit5";
            this.reg_10.bit6 = "bit6";
            this.reg_10.bit7 = "bit7";
            this.reg_10.bit8 = "bit8";
            this.reg_10.bit9 = "bit9";
            this.reg_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_10.Location = new System.Drawing.Point(958, 31);
            this.reg_10.Name = "reg_10";
            this.reg_10.RegisterName = null;
            this.reg_10.Size = new System.Drawing.Size(890, 38);
            this.reg_10.TabIndex = 134;
            // 
            // reg_0E
            // 
            this.reg_0E.Address = null;
            this.reg_0E.bit0 = "bit0";
            this.reg_0E.bit1 = "bit1";
            this.reg_0E.bit10 = "bit10";
            this.reg_0E.bit11 = "bit11";
            this.reg_0E.bit12 = "bit12";
            this.reg_0E.bit13 = "bit13";
            this.reg_0E.bit14 = "bit14";
            this.reg_0E.bit15 = "bit15";
            this.reg_0E.bit16 = "bit16";
            this.reg_0E.bit17 = "bit17";
            this.reg_0E.bit2 = "bit2";
            this.reg_0E.bit3 = "bit3";
            this.reg_0E.bit4 = "bit4";
            this.reg_0E.bit5 = "bit5";
            this.reg_0E.bit6 = "bit6";
            this.reg_0E.bit7 = "bit7";
            this.reg_0E.bit8 = "bit8";
            this.reg_0E.bit9 = "bit9";
            this.reg_0E.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_0E.Location = new System.Drawing.Point(20, 647);
            this.reg_0E.Name = "reg_0E";
            this.reg_0E.RegisterName = null;
            this.reg_0E.Size = new System.Drawing.Size(890, 38);
            this.reg_0E.TabIndex = 132;
            // 
            // reg_0F
            // 
            this.reg_0F.Address = null;
            this.reg_0F.bit0 = "bit0";
            this.reg_0F.bit1 = "bit1";
            this.reg_0F.bit10 = "bit10";
            this.reg_0F.bit11 = "bit11";
            this.reg_0F.bit12 = "bit12";
            this.reg_0F.bit13 = "bit13";
            this.reg_0F.bit14 = "bit14";
            this.reg_0F.bit15 = "bit15";
            this.reg_0F.bit16 = "bit16";
            this.reg_0F.bit17 = "bit17";
            this.reg_0F.bit2 = "bit2";
            this.reg_0F.bit3 = "bit3";
            this.reg_0F.bit4 = "bit4";
            this.reg_0F.bit5 = "bit5";
            this.reg_0F.bit6 = "bit6";
            this.reg_0F.bit7 = "bit7";
            this.reg_0F.bit8 = "bit8";
            this.reg_0F.bit9 = "bit9";
            this.reg_0F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reg_0F.Location = new System.Drawing.Point(20, 691);
            this.reg_0F.Name = "reg_0F";
            this.reg_0F.RegisterName = null;
            this.reg_0F.Size = new System.Drawing.Size(890, 38);
            this.reg_0F.TabIndex = 133;
            // 
            // tabRegisters
            // 
            this.tabRegisters.Controls.Add(this.tabPage3);
            this.tabRegisters.Controls.Add(this.tabPage4);
            this.tabRegisters.Controls.Add(this.tabPage5);
            this.tabRegisters.Controls.Add(this.tabPage6);
            this.tabRegisters.Controls.Add(this.tabPage7);
            this.tabRegisters.Controls.Add(this.tabPage9);
            this.tabRegisters.HotTrack = true;
            this.tabRegisters.Location = new System.Drawing.Point(15, 6);
            this.tabRegisters.Name = "tabRegisters";
            this.tabRegisters.SelectedIndex = 0;
            this.tabRegisters.Size = new System.Drawing.Size(1848, 749);
            this.tabRegisters.TabIndex = 0;
            this.tabRegisters.SelectedIndexChanged += new System.EventHandler(this.tabRegistersUpdate);
            this.tabRegisters.Enter += new System.EventHandler(this.tabRegistersFocus);
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1840, 723);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "0x2000_0000 - 0x2000_001F";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1840, 723);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "0x3000_0000 - 0x3000_001F";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1840, 723);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "0x4000_0000 - 0x4000_002A";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1840, 723);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "0x6000_0000 - 0x6000_001F";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(1840, 723);
            this.tabPage7.TabIndex = 4;
            this.tabPage7.Text = "0x6000_0020 - 0x6000_0037";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tabPage9
            // 
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(1840, 723);
            this.tabPage9.TabIndex = 6;
            this.tabPage9.Text = "0x7000_0000 - 0x7000_001F";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.txtFlashStatus);
            this.tabPage8.Controls.Add(this.btnReadFlashStatus);
            this.tabPage8.Controls.Add(this.btnEraseFlash);
            this.tabPage8.Controls.Add(this.btnReadFlash);
            this.tabPage8.Controls.Add(this.btnWriteFlash);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(192, 74);
            this.tabPage8.TabIndex = 2;
            this.tabPage8.Text = "Acclaim Battery Pack - FLASH";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // txtFlashStatus
            // 
            this.txtFlashStatus.Location = new System.Drawing.Point(234, 225);
            this.txtFlashStatus.Name = "txtFlashStatus";
            this.txtFlashStatus.Size = new System.Drawing.Size(115, 20);
            this.txtFlashStatus.TabIndex = 23;
            // 
            // btnReadFlashStatus
            // 
            this.btnReadFlashStatus.Location = new System.Drawing.Point(106, 223);
            this.btnReadFlashStatus.Name = "btnReadFlashStatus";
            this.btnReadFlashStatus.Size = new System.Drawing.Size(112, 23);
            this.btnReadFlashStatus.TabIndex = 3;
            this.btnReadFlashStatus.Text = "Read Flash Status";
            this.btnReadFlashStatus.UseVisualStyleBackColor = true;
            this.btnReadFlashStatus.Click += new System.EventHandler(this.btnReadFlashStatus_Click);
            // 
            // btnEraseFlash
            // 
            this.btnEraseFlash.Location = new System.Drawing.Point(106, 146);
            this.btnEraseFlash.Name = "btnEraseFlash";
            this.btnEraseFlash.Size = new System.Drawing.Size(75, 23);
            this.btnEraseFlash.TabIndex = 2;
            this.btnEraseFlash.Text = "Erase";
            this.btnEraseFlash.UseVisualStyleBackColor = true;
            this.btnEraseFlash.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // btnReadFlash
            // 
            this.btnReadFlash.Location = new System.Drawing.Point(187, 50);
            this.btnReadFlash.Name = "btnReadFlash";
            this.btnReadFlash.Size = new System.Drawing.Size(75, 23);
            this.btnReadFlash.TabIndex = 1;
            this.btnReadFlash.Text = "Read";
            this.btnReadFlash.UseVisualStyleBackColor = true;
            this.btnReadFlash.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // btnWriteFlash
            // 
            this.btnWriteFlash.Location = new System.Drawing.Point(83, 50);
            this.btnWriteFlash.Name = "btnWriteFlash";
            this.btnWriteFlash.Size = new System.Drawing.Size(75, 23);
            this.btnWriteFlash.TabIndex = 0;
            this.btnWriteFlash.Text = "Write Flash";
            this.btnWriteFlash.UseVisualStyleBackColor = true;
            this.btnWriteFlash.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.regstim_1F);
            this.tabPage12.Controls.Add(this.regstim_1C);
            this.tabPage12.Controls.Add(this.regstim_1E);
            this.tabPage12.Controls.Add(this.regstim_00);
            this.tabPage12.Controls.Add(this.regstim_1D);
            this.tabPage12.Controls.Add(this.regstim_01);
            this.tabPage12.Controls.Add(this.regstim_02);
            this.tabPage12.Controls.Add(this.regstim_1B);
            this.tabPage12.Controls.Add(this.regstim_03);
            this.tabPage12.Controls.Add(this.regstim_1A);
            this.tabPage12.Controls.Add(this.regstim_04);
            this.tabPage12.Controls.Add(this.regstim_19);
            this.tabPage12.Controls.Add(this.regstim_05);
            this.tabPage12.Controls.Add(this.regstim_18);
            this.tabPage12.Controls.Add(this.regstim_06);
            this.tabPage12.Controls.Add(this.regstim_17);
            this.tabPage12.Controls.Add(this.regstim_07);
            this.tabPage12.Controls.Add(this.regstim_16);
            this.tabPage12.Controls.Add(this.regstim_08);
            this.tabPage12.Controls.Add(this.regstim_15);
            this.tabPage12.Controls.Add(this.regstim_09);
            this.tabPage12.Controls.Add(this.regstim_14);
            this.tabPage12.Controls.Add(this.regstim_0A);
            this.tabPage12.Controls.Add(this.regstim_13);
            this.tabPage12.Controls.Add(this.regstim_0B);
            this.tabPage12.Controls.Add(this.regstim_12);
            this.tabPage12.Controls.Add(this.regstim_0C);
            this.tabPage12.Controls.Add(this.regstim_11);
            this.tabPage12.Controls.Add(this.regstim_0D);
            this.tabPage12.Controls.Add(this.regstim_10);
            this.tabPage12.Controls.Add(this.regstim_0E);
            this.tabPage12.Controls.Add(this.regstim_0F);
            this.tabPage12.Controls.Add(this.tabStimRegisters);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(192, 74);
            this.tabPage12.TabIndex = 5;
            this.tabPage12.Text = "Acclaim Processor - Registers";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // regstim_1F
            // 
            this.regstim_1F.Address = null;
            this.regstim_1F.bit0 = "bit0";
            this.regstim_1F.bit1 = "bit1";
            this.regstim_1F.bit10 = "bit10";
            this.regstim_1F.bit11 = "bit11";
            this.regstim_1F.bit12 = "bit12";
            this.regstim_1F.bit13 = "bit13";
            this.regstim_1F.bit14 = "bit14";
            this.regstim_1F.bit15 = "bit15";
            this.regstim_1F.bit16 = "bit16";
            this.regstim_1F.bit17 = "bit17";
            this.regstim_1F.bit2 = "bit2";
            this.regstim_1F.bit3 = "bit3";
            this.regstim_1F.bit4 = "bit4";
            this.regstim_1F.bit5 = "bit5";
            this.regstim_1F.bit6 = "bit6";
            this.regstim_1F.bit7 = "bit7";
            this.regstim_1F.bit8 = "bit8";
            this.regstim_1F.bit9 = "bit9";
            this.regstim_1F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_1F.Location = new System.Drawing.Point(958, 691);
            this.regstim_1F.Name = "regstim_1F";
            this.regstim_1F.RegisterName = null;
            this.regstim_1F.Size = new System.Drawing.Size(890, 38);
            this.regstim_1F.TabIndex = 181;
            // 
            // regstim_1C
            // 
            this.regstim_1C.Address = null;
            this.regstim_1C.bit0 = "bit0";
            this.regstim_1C.bit1 = "bit1";
            this.regstim_1C.bit10 = "bit10";
            this.regstim_1C.bit11 = "bit11";
            this.regstim_1C.bit12 = "bit12";
            this.regstim_1C.bit13 = "bit13";
            this.regstim_1C.bit14 = "bit14";
            this.regstim_1C.bit15 = "bit15";
            this.regstim_1C.bit16 = "bit16";
            this.regstim_1C.bit17 = "bit17";
            this.regstim_1C.bit2 = "bit2";
            this.regstim_1C.bit3 = "bit3";
            this.regstim_1C.bit4 = "bit4";
            this.regstim_1C.bit5 = "bit5";
            this.regstim_1C.bit6 = "bit6";
            this.regstim_1C.bit7 = "bit7";
            this.regstim_1C.bit8 = "bit8";
            this.regstim_1C.bit9 = "bit9";
            this.regstim_1C.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_1C.Location = new System.Drawing.Point(958, 559);
            this.regstim_1C.Name = "regstim_1C";
            this.regstim_1C.RegisterName = null;
            this.regstim_1C.Size = new System.Drawing.Size(890, 38);
            this.regstim_1C.TabIndex = 178;
            // 
            // regstim_1E
            // 
            this.regstim_1E.Address = null;
            this.regstim_1E.bit0 = "bit0";
            this.regstim_1E.bit1 = "bit1";
            this.regstim_1E.bit10 = "bit10";
            this.regstim_1E.bit11 = "bit11";
            this.regstim_1E.bit12 = "bit12";
            this.regstim_1E.bit13 = "bit13";
            this.regstim_1E.bit14 = "bit14";
            this.regstim_1E.bit15 = "bit15";
            this.regstim_1E.bit16 = "bit16";
            this.regstim_1E.bit17 = "bit17";
            this.regstim_1E.bit2 = "bit2";
            this.regstim_1E.bit3 = "bit3";
            this.regstim_1E.bit4 = "bit4";
            this.regstim_1E.bit5 = "bit5";
            this.regstim_1E.bit6 = "bit6";
            this.regstim_1E.bit7 = "bit7";
            this.regstim_1E.bit8 = "bit8";
            this.regstim_1E.bit9 = "bit9";
            this.regstim_1E.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_1E.Location = new System.Drawing.Point(958, 647);
            this.regstim_1E.Name = "regstim_1E";
            this.regstim_1E.RegisterName = null;
            this.regstim_1E.Size = new System.Drawing.Size(890, 38);
            this.regstim_1E.TabIndex = 180;
            // 
            // regstim_00
            // 
            this.regstim_00.Address = "00000000";
            this.regstim_00.bit0 = "bit0";
            this.regstim_00.bit1 = "bit1";
            this.regstim_00.bit10 = "bit10";
            this.regstim_00.bit11 = "bit11";
            this.regstim_00.bit12 = "bit12";
            this.regstim_00.bit13 = "bit13";
            this.regstim_00.bit14 = "bit14";
            this.regstim_00.bit15 = "bit15";
            this.regstim_00.bit16 = "bit16";
            this.regstim_00.bit17 = "bit17";
            this.regstim_00.bit2 = "bit2";
            this.regstim_00.bit3 = "bit3";
            this.regstim_00.bit4 = "bit4";
            this.regstim_00.bit5 = "bit5";
            this.regstim_00.bit6 = "bit6";
            this.regstim_00.bit7 = "bit7";
            this.regstim_00.bit8 = "bit8";
            this.regstim_00.bit9 = "bit9";
            this.regstim_00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_00.Location = new System.Drawing.Point(20, 31);
            this.regstim_00.Name = "regstim_00";
            this.regstim_00.RegisterName = "Test Register Name";
            this.regstim_00.Size = new System.Drawing.Size(890, 38);
            this.regstim_00.TabIndex = 150;
            // 
            // regstim_1D
            // 
            this.regstim_1D.Address = null;
            this.regstim_1D.bit0 = "bit0";
            this.regstim_1D.bit1 = "bit1";
            this.regstim_1D.bit10 = "bit10";
            this.regstim_1D.bit11 = "bit11";
            this.regstim_1D.bit12 = "bit12";
            this.regstim_1D.bit13 = "bit13";
            this.regstim_1D.bit14 = "bit14";
            this.regstim_1D.bit15 = "bit15";
            this.regstim_1D.bit16 = "bit16";
            this.regstim_1D.bit17 = "bit17";
            this.regstim_1D.bit2 = "bit2";
            this.regstim_1D.bit3 = "bit3";
            this.regstim_1D.bit4 = "bit4";
            this.regstim_1D.bit5 = "bit5";
            this.regstim_1D.bit6 = "bit6";
            this.regstim_1D.bit7 = "bit7";
            this.regstim_1D.bit8 = "bit8";
            this.regstim_1D.bit9 = "bit9";
            this.regstim_1D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_1D.Location = new System.Drawing.Point(958, 603);
            this.regstim_1D.Name = "regstim_1D";
            this.regstim_1D.RegisterName = null;
            this.regstim_1D.Size = new System.Drawing.Size(890, 38);
            this.regstim_1D.TabIndex = 179;
            // 
            // regstim_01
            // 
            this.regstim_01.Address = null;
            this.regstim_01.bit0 = "bit0";
            this.regstim_01.bit1 = "bit1";
            this.regstim_01.bit10 = "bit10";
            this.regstim_01.bit11 = "bit11";
            this.regstim_01.bit12 = "bit12";
            this.regstim_01.bit13 = "bit13";
            this.regstim_01.bit14 = "bit14";
            this.regstim_01.bit15 = "bit15";
            this.regstim_01.bit16 = "bit16";
            this.regstim_01.bit17 = "bit17";
            this.regstim_01.bit2 = "bit2";
            this.regstim_01.bit3 = "bit3";
            this.regstim_01.bit4 = "bit4";
            this.regstim_01.bit5 = "bit5";
            this.regstim_01.bit6 = "bit6";
            this.regstim_01.bit7 = "bit7";
            this.regstim_01.bit8 = "bit8";
            this.regstim_01.bit9 = "bit9";
            this.regstim_01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_01.Location = new System.Drawing.Point(20, 75);
            this.regstim_01.Name = "regstim_01";
            this.regstim_01.RegisterName = null;
            this.regstim_01.Size = new System.Drawing.Size(890, 38);
            this.regstim_01.TabIndex = 151;
            // 
            // regstim_02
            // 
            this.regstim_02.Address = null;
            this.regstim_02.bit0 = "bit0";
            this.regstim_02.bit1 = "bit1";
            this.regstim_02.bit10 = "bit10";
            this.regstim_02.bit11 = "bit11";
            this.regstim_02.bit12 = "bit12";
            this.regstim_02.bit13 = "bit13";
            this.regstim_02.bit14 = "bit14";
            this.regstim_02.bit15 = "bit15";
            this.regstim_02.bit16 = "bit16";
            this.regstim_02.bit17 = "bit17";
            this.regstim_02.bit2 = "bit2";
            this.regstim_02.bit3 = "bit3";
            this.regstim_02.bit4 = "bit4";
            this.regstim_02.bit5 = "bit5";
            this.regstim_02.bit6 = "bit6";
            this.regstim_02.bit7 = "bit7";
            this.regstim_02.bit8 = "bit8";
            this.regstim_02.bit9 = "bit9";
            this.regstim_02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_02.Location = new System.Drawing.Point(20, 119);
            this.regstim_02.Name = "regstim_02";
            this.regstim_02.RegisterName = null;
            this.regstim_02.Size = new System.Drawing.Size(890, 38);
            this.regstim_02.TabIndex = 152;
            // 
            // regstim_1B
            // 
            this.regstim_1B.Address = null;
            this.regstim_1B.bit0 = "bit0";
            this.regstim_1B.bit1 = "bit1";
            this.regstim_1B.bit10 = "bit10";
            this.regstim_1B.bit11 = "bit11";
            this.regstim_1B.bit12 = "bit12";
            this.regstim_1B.bit13 = "bit13";
            this.regstim_1B.bit14 = "bit14";
            this.regstim_1B.bit15 = "bit15";
            this.regstim_1B.bit16 = "bit16";
            this.regstim_1B.bit17 = "bit17";
            this.regstim_1B.bit2 = "bit2";
            this.regstim_1B.bit3 = "bit3";
            this.regstim_1B.bit4 = "bit4";
            this.regstim_1B.bit5 = "bit5";
            this.regstim_1B.bit6 = "bit6";
            this.regstim_1B.bit7 = "bit7";
            this.regstim_1B.bit8 = "bit8";
            this.regstim_1B.bit9 = "bit9";
            this.regstim_1B.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_1B.Location = new System.Drawing.Point(958, 515);
            this.regstim_1B.Name = "regstim_1B";
            this.regstim_1B.RegisterName = null;
            this.regstim_1B.Size = new System.Drawing.Size(890, 38);
            this.regstim_1B.TabIndex = 177;
            // 
            // regstim_03
            // 
            this.regstim_03.Address = null;
            this.regstim_03.bit0 = "bit0";
            this.regstim_03.bit1 = "bit1";
            this.regstim_03.bit10 = "bit10";
            this.regstim_03.bit11 = "bit11";
            this.regstim_03.bit12 = "bit12";
            this.regstim_03.bit13 = "bit13";
            this.regstim_03.bit14 = "bit14";
            this.regstim_03.bit15 = "bit15";
            this.regstim_03.bit16 = "bit16";
            this.regstim_03.bit17 = "bit17";
            this.regstim_03.bit2 = "bit2";
            this.regstim_03.bit3 = "bit3";
            this.regstim_03.bit4 = "bit4";
            this.regstim_03.bit5 = "bit5";
            this.regstim_03.bit6 = "bit6";
            this.regstim_03.bit7 = "bit7";
            this.regstim_03.bit8 = "bit8";
            this.regstim_03.bit9 = "bit9";
            this.regstim_03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_03.Location = new System.Drawing.Point(20, 163);
            this.regstim_03.Name = "regstim_03";
            this.regstim_03.RegisterName = null;
            this.regstim_03.Size = new System.Drawing.Size(890, 38);
            this.regstim_03.TabIndex = 153;
            // 
            // regstim_1A
            // 
            this.regstim_1A.Address = null;
            this.regstim_1A.bit0 = "bit0";
            this.regstim_1A.bit1 = "bit1";
            this.regstim_1A.bit10 = "bit10";
            this.regstim_1A.bit11 = "bit11";
            this.regstim_1A.bit12 = "bit12";
            this.regstim_1A.bit13 = "bit13";
            this.regstim_1A.bit14 = "bit14";
            this.regstim_1A.bit15 = "bit15";
            this.regstim_1A.bit16 = "bit16";
            this.regstim_1A.bit17 = "bit17";
            this.regstim_1A.bit2 = "bit2";
            this.regstim_1A.bit3 = "bit3";
            this.regstim_1A.bit4 = "bit4";
            this.regstim_1A.bit5 = "bit5";
            this.regstim_1A.bit6 = "bit6";
            this.regstim_1A.bit7 = "bit7";
            this.regstim_1A.bit8 = "bit8";
            this.regstim_1A.bit9 = "bit9";
            this.regstim_1A.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_1A.Location = new System.Drawing.Point(958, 471);
            this.regstim_1A.Name = "regstim_1A";
            this.regstim_1A.RegisterName = null;
            this.regstim_1A.Size = new System.Drawing.Size(890, 38);
            this.regstim_1A.TabIndex = 176;
            // 
            // regstim_04
            // 
            this.regstim_04.Address = null;
            this.regstim_04.bit0 = "bit0";
            this.regstim_04.bit1 = "bit1";
            this.regstim_04.bit10 = "bit10";
            this.regstim_04.bit11 = "bit11";
            this.regstim_04.bit12 = "bit12";
            this.regstim_04.bit13 = "bit13";
            this.regstim_04.bit14 = "bit14";
            this.regstim_04.bit15 = "bit15";
            this.regstim_04.bit16 = "bit16";
            this.regstim_04.bit17 = "bit17";
            this.regstim_04.bit2 = "bit2";
            this.regstim_04.bit3 = "bit3";
            this.regstim_04.bit4 = "bit4";
            this.regstim_04.bit5 = "bit5";
            this.regstim_04.bit6 = "bit6";
            this.regstim_04.bit7 = "bit7";
            this.regstim_04.bit8 = "bit8";
            this.regstim_04.bit9 = "bit9";
            this.regstim_04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_04.Location = new System.Drawing.Point(20, 207);
            this.regstim_04.Name = "regstim_04";
            this.regstim_04.RegisterName = null;
            this.regstim_04.Size = new System.Drawing.Size(890, 38);
            this.regstim_04.TabIndex = 154;
            // 
            // regstim_19
            // 
            this.regstim_19.Address = null;
            this.regstim_19.bit0 = "bit0";
            this.regstim_19.bit1 = "bit1";
            this.regstim_19.bit10 = "bit10";
            this.regstim_19.bit11 = "bit11";
            this.regstim_19.bit12 = "bit12";
            this.regstim_19.bit13 = "bit13";
            this.regstim_19.bit14 = "bit14";
            this.regstim_19.bit15 = "bit15";
            this.regstim_19.bit16 = "bit16";
            this.regstim_19.bit17 = "bit17";
            this.regstim_19.bit2 = "bit2";
            this.regstim_19.bit3 = "bit3";
            this.regstim_19.bit4 = "bit4";
            this.regstim_19.bit5 = "bit5";
            this.regstim_19.bit6 = "bit6";
            this.regstim_19.bit7 = "bit7";
            this.regstim_19.bit8 = "bit8";
            this.regstim_19.bit9 = "bit9";
            this.regstim_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_19.Location = new System.Drawing.Point(958, 427);
            this.regstim_19.Name = "regstim_19";
            this.regstim_19.RegisterName = null;
            this.regstim_19.Size = new System.Drawing.Size(890, 38);
            this.regstim_19.TabIndex = 175;
            // 
            // regstim_05
            // 
            this.regstim_05.Address = null;
            this.regstim_05.bit0 = "bit0";
            this.regstim_05.bit1 = "bit1";
            this.regstim_05.bit10 = "bit10";
            this.regstim_05.bit11 = "bit11";
            this.regstim_05.bit12 = "bit12";
            this.regstim_05.bit13 = "bit13";
            this.regstim_05.bit14 = "bit14";
            this.regstim_05.bit15 = "bit15";
            this.regstim_05.bit16 = "bit16";
            this.regstim_05.bit17 = "bit17";
            this.regstim_05.bit2 = "bit2";
            this.regstim_05.bit3 = "bit3";
            this.regstim_05.bit4 = "bit4";
            this.regstim_05.bit5 = "bit5";
            this.regstim_05.bit6 = "bit6";
            this.regstim_05.bit7 = "bit7";
            this.regstim_05.bit8 = "bit8";
            this.regstim_05.bit9 = "bit9";
            this.regstim_05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_05.Location = new System.Drawing.Point(20, 251);
            this.regstim_05.Name = "regstim_05";
            this.regstim_05.RegisterName = null;
            this.regstim_05.Size = new System.Drawing.Size(890, 38);
            this.regstim_05.TabIndex = 155;
            // 
            // regstim_18
            // 
            this.regstim_18.Address = null;
            this.regstim_18.bit0 = "bit0";
            this.regstim_18.bit1 = "bit1";
            this.regstim_18.bit10 = "bit10";
            this.regstim_18.bit11 = "bit11";
            this.regstim_18.bit12 = "bit12";
            this.regstim_18.bit13 = "bit13";
            this.regstim_18.bit14 = "bit14";
            this.regstim_18.bit15 = "bit15";
            this.regstim_18.bit16 = "bit16";
            this.regstim_18.bit17 = "bit17";
            this.regstim_18.bit2 = "bit2";
            this.regstim_18.bit3 = "bit3";
            this.regstim_18.bit4 = "bit4";
            this.regstim_18.bit5 = "bit5";
            this.regstim_18.bit6 = "bit6";
            this.regstim_18.bit7 = "bit7";
            this.regstim_18.bit8 = "bit8";
            this.regstim_18.bit9 = "bit9";
            this.regstim_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_18.Location = new System.Drawing.Point(958, 383);
            this.regstim_18.Name = "regstim_18";
            this.regstim_18.RegisterName = null;
            this.regstim_18.Size = new System.Drawing.Size(890, 38);
            this.regstim_18.TabIndex = 174;
            // 
            // regstim_06
            // 
            this.regstim_06.Address = null;
            this.regstim_06.bit0 = "bit0";
            this.regstim_06.bit1 = "bit1";
            this.regstim_06.bit10 = "bit10";
            this.regstim_06.bit11 = "bit11";
            this.regstim_06.bit12 = "bit12";
            this.regstim_06.bit13 = "bit13";
            this.regstim_06.bit14 = "bit14";
            this.regstim_06.bit15 = "bit15";
            this.regstim_06.bit16 = "bit16";
            this.regstim_06.bit17 = "bit17";
            this.regstim_06.bit2 = "bit2";
            this.regstim_06.bit3 = "bit3";
            this.regstim_06.bit4 = "bit4";
            this.regstim_06.bit5 = "bit5";
            this.regstim_06.bit6 = "bit6";
            this.regstim_06.bit7 = "bit7";
            this.regstim_06.bit8 = "bit8";
            this.regstim_06.bit9 = "bit9";
            this.regstim_06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_06.Location = new System.Drawing.Point(20, 295);
            this.regstim_06.Name = "regstim_06";
            this.regstim_06.RegisterName = null;
            this.regstim_06.Size = new System.Drawing.Size(890, 38);
            this.regstim_06.TabIndex = 156;
            // 
            // regstim_17
            // 
            this.regstim_17.Address = null;
            this.regstim_17.bit0 = "bit0";
            this.regstim_17.bit1 = "bit1";
            this.regstim_17.bit10 = "bit10";
            this.regstim_17.bit11 = "bit11";
            this.regstim_17.bit12 = "bit12";
            this.regstim_17.bit13 = "bit13";
            this.regstim_17.bit14 = "bit14";
            this.regstim_17.bit15 = "bit15";
            this.regstim_17.bit16 = "bit16";
            this.regstim_17.bit17 = "bit17";
            this.regstim_17.bit2 = "bit2";
            this.regstim_17.bit3 = "bit3";
            this.regstim_17.bit4 = "bit4";
            this.regstim_17.bit5 = "bit5";
            this.regstim_17.bit6 = "bit6";
            this.regstim_17.bit7 = "bit7";
            this.regstim_17.bit8 = "bit8";
            this.regstim_17.bit9 = "bit9";
            this.regstim_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_17.Location = new System.Drawing.Point(958, 339);
            this.regstim_17.Name = "regstim_17";
            this.regstim_17.RegisterName = null;
            this.regstim_17.Size = new System.Drawing.Size(890, 38);
            this.regstim_17.TabIndex = 173;
            // 
            // regstim_07
            // 
            this.regstim_07.Address = null;
            this.regstim_07.bit0 = "bit0";
            this.regstim_07.bit1 = "bit1";
            this.regstim_07.bit10 = "bit10";
            this.regstim_07.bit11 = "bit11";
            this.regstim_07.bit12 = "bit12";
            this.regstim_07.bit13 = "bit13";
            this.regstim_07.bit14 = "bit14";
            this.regstim_07.bit15 = "bit15";
            this.regstim_07.bit16 = "bit16";
            this.regstim_07.bit17 = "bit17";
            this.regstim_07.bit2 = "bit2";
            this.regstim_07.bit3 = "bit3";
            this.regstim_07.bit4 = "bit4";
            this.regstim_07.bit5 = "bit5";
            this.regstim_07.bit6 = "bit6";
            this.regstim_07.bit7 = "bit7";
            this.regstim_07.bit8 = "bit8";
            this.regstim_07.bit9 = "bit9";
            this.regstim_07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_07.Location = new System.Drawing.Point(20, 339);
            this.regstim_07.Name = "regstim_07";
            this.regstim_07.RegisterName = null;
            this.regstim_07.Size = new System.Drawing.Size(890, 38);
            this.regstim_07.TabIndex = 157;
            // 
            // regstim_16
            // 
            this.regstim_16.Address = null;
            this.regstim_16.bit0 = "bit0";
            this.regstim_16.bit1 = "bit1";
            this.regstim_16.bit10 = "bit10";
            this.regstim_16.bit11 = "bit11";
            this.regstim_16.bit12 = "bit12";
            this.regstim_16.bit13 = "bit13";
            this.regstim_16.bit14 = "bit14";
            this.regstim_16.bit15 = "bit15";
            this.regstim_16.bit16 = "bit16";
            this.regstim_16.bit17 = "bit17";
            this.regstim_16.bit2 = "bit2";
            this.regstim_16.bit3 = "bit3";
            this.regstim_16.bit4 = "bit4";
            this.regstim_16.bit5 = "bit5";
            this.regstim_16.bit6 = "bit6";
            this.regstim_16.bit7 = "bit7";
            this.regstim_16.bit8 = "bit8";
            this.regstim_16.bit9 = "bit9";
            this.regstim_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_16.Location = new System.Drawing.Point(958, 295);
            this.regstim_16.Name = "regstim_16";
            this.regstim_16.RegisterName = null;
            this.regstim_16.Size = new System.Drawing.Size(890, 38);
            this.regstim_16.TabIndex = 172;
            // 
            // regstim_08
            // 
            this.regstim_08.Address = null;
            this.regstim_08.bit0 = "bit0";
            this.regstim_08.bit1 = "bit1";
            this.regstim_08.bit10 = "bit10";
            this.regstim_08.bit11 = "bit11";
            this.regstim_08.bit12 = "bit12";
            this.regstim_08.bit13 = "bit13";
            this.regstim_08.bit14 = "bit14";
            this.regstim_08.bit15 = "bit15";
            this.regstim_08.bit16 = "bit16";
            this.regstim_08.bit17 = "bit17";
            this.regstim_08.bit2 = "bit2";
            this.regstim_08.bit3 = "bit3";
            this.regstim_08.bit4 = "bit4";
            this.regstim_08.bit5 = "bit5";
            this.regstim_08.bit6 = "bit6";
            this.regstim_08.bit7 = "bit7";
            this.regstim_08.bit8 = "bit8";
            this.regstim_08.bit9 = "bit9";
            this.regstim_08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_08.Location = new System.Drawing.Point(20, 383);
            this.regstim_08.Name = "regstim_08";
            this.regstim_08.RegisterName = null;
            this.regstim_08.Size = new System.Drawing.Size(890, 38);
            this.regstim_08.TabIndex = 158;
            // 
            // regstim_15
            // 
            this.regstim_15.Address = null;
            this.regstim_15.bit0 = "bit0";
            this.regstim_15.bit1 = "bit1";
            this.regstim_15.bit10 = "bit10";
            this.regstim_15.bit11 = "bit11";
            this.regstim_15.bit12 = "bit12";
            this.regstim_15.bit13 = "bit13";
            this.regstim_15.bit14 = "bit14";
            this.regstim_15.bit15 = "bit15";
            this.regstim_15.bit16 = "bit16";
            this.regstim_15.bit17 = "bit17";
            this.regstim_15.bit2 = "bit2";
            this.regstim_15.bit3 = "bit3";
            this.regstim_15.bit4 = "bit4";
            this.regstim_15.bit5 = "bit5";
            this.regstim_15.bit6 = "bit6";
            this.regstim_15.bit7 = "bit7";
            this.regstim_15.bit8 = "bit8";
            this.regstim_15.bit9 = "bit9";
            this.regstim_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_15.Location = new System.Drawing.Point(958, 251);
            this.regstim_15.Name = "regstim_15";
            this.regstim_15.RegisterName = null;
            this.regstim_15.Size = new System.Drawing.Size(890, 38);
            this.regstim_15.TabIndex = 171;
            // 
            // regstim_09
            // 
            this.regstim_09.Address = null;
            this.regstim_09.bit0 = "bit0";
            this.regstim_09.bit1 = "bit1";
            this.regstim_09.bit10 = "bit10";
            this.regstim_09.bit11 = "bit11";
            this.regstim_09.bit12 = "bit12";
            this.regstim_09.bit13 = "bit13";
            this.regstim_09.bit14 = "bit14";
            this.regstim_09.bit15 = "bit15";
            this.regstim_09.bit16 = "bit16";
            this.regstim_09.bit17 = "bit17";
            this.regstim_09.bit2 = "bit2";
            this.regstim_09.bit3 = "bit3";
            this.regstim_09.bit4 = "bit4";
            this.regstim_09.bit5 = "bit5";
            this.regstim_09.bit6 = "bit6";
            this.regstim_09.bit7 = "bit7";
            this.regstim_09.bit8 = "bit8";
            this.regstim_09.bit9 = "bit9";
            this.regstim_09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_09.Location = new System.Drawing.Point(20, 427);
            this.regstim_09.Name = "regstim_09";
            this.regstim_09.RegisterName = null;
            this.regstim_09.Size = new System.Drawing.Size(890, 38);
            this.regstim_09.TabIndex = 159;
            // 
            // regstim_14
            // 
            this.regstim_14.Address = null;
            this.regstim_14.bit0 = "bit0";
            this.regstim_14.bit1 = "bit1";
            this.regstim_14.bit10 = "bit10";
            this.regstim_14.bit11 = "bit11";
            this.regstim_14.bit12 = "bit12";
            this.regstim_14.bit13 = "bit13";
            this.regstim_14.bit14 = "bit14";
            this.regstim_14.bit15 = "bit15";
            this.regstim_14.bit16 = "bit16";
            this.regstim_14.bit17 = "bit17";
            this.regstim_14.bit2 = "bit2";
            this.regstim_14.bit3 = "bit3";
            this.regstim_14.bit4 = "bit4";
            this.regstim_14.bit5 = "bit5";
            this.regstim_14.bit6 = "bit6";
            this.regstim_14.bit7 = "bit7";
            this.regstim_14.bit8 = "bit8";
            this.regstim_14.bit9 = "bit9";
            this.regstim_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_14.Location = new System.Drawing.Point(958, 207);
            this.regstim_14.Name = "regstim_14";
            this.regstim_14.RegisterName = null;
            this.regstim_14.Size = new System.Drawing.Size(890, 38);
            this.regstim_14.TabIndex = 170;
            // 
            // regstim_0A
            // 
            this.regstim_0A.Address = null;
            this.regstim_0A.bit0 = "bit0";
            this.regstim_0A.bit1 = "bit1";
            this.regstim_0A.bit10 = "bit10";
            this.regstim_0A.bit11 = "bit11";
            this.regstim_0A.bit12 = "bit12";
            this.regstim_0A.bit13 = "bit13";
            this.regstim_0A.bit14 = "bit14";
            this.regstim_0A.bit15 = "bit15";
            this.regstim_0A.bit16 = "bit16";
            this.regstim_0A.bit17 = "bit17";
            this.regstim_0A.bit2 = "bit2";
            this.regstim_0A.bit3 = "bit3";
            this.regstim_0A.bit4 = "bit4";
            this.regstim_0A.bit5 = "bit5";
            this.regstim_0A.bit6 = "bit6";
            this.regstim_0A.bit7 = "bit7";
            this.regstim_0A.bit8 = "bit8";
            this.regstim_0A.bit9 = "bit9";
            this.regstim_0A.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_0A.Location = new System.Drawing.Point(20, 471);
            this.regstim_0A.Name = "regstim_0A";
            this.regstim_0A.RegisterName = null;
            this.regstim_0A.Size = new System.Drawing.Size(890, 38);
            this.regstim_0A.TabIndex = 160;
            // 
            // regstim_13
            // 
            this.regstim_13.Address = null;
            this.regstim_13.bit0 = "bit0";
            this.regstim_13.bit1 = "bit1";
            this.regstim_13.bit10 = "bit10";
            this.regstim_13.bit11 = "bit11";
            this.regstim_13.bit12 = "bit12";
            this.regstim_13.bit13 = "bit13";
            this.regstim_13.bit14 = "bit14";
            this.regstim_13.bit15 = "bit15";
            this.regstim_13.bit16 = "bit16";
            this.regstim_13.bit17 = "bit17";
            this.regstim_13.bit2 = "bit2";
            this.regstim_13.bit3 = "bit3";
            this.regstim_13.bit4 = "bit4";
            this.regstim_13.bit5 = "bit5";
            this.regstim_13.bit6 = "bit6";
            this.regstim_13.bit7 = "bit7";
            this.regstim_13.bit8 = "bit8";
            this.regstim_13.bit9 = "bit9";
            this.regstim_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_13.Location = new System.Drawing.Point(958, 163);
            this.regstim_13.Name = "regstim_13";
            this.regstim_13.RegisterName = null;
            this.regstim_13.Size = new System.Drawing.Size(890, 38);
            this.regstim_13.TabIndex = 169;
            // 
            // regstim_0B
            // 
            this.regstim_0B.Address = null;
            this.regstim_0B.bit0 = "bit0";
            this.regstim_0B.bit1 = "bit1";
            this.regstim_0B.bit10 = "bit10";
            this.regstim_0B.bit11 = "bit11";
            this.regstim_0B.bit12 = "bit12";
            this.regstim_0B.bit13 = "bit13";
            this.regstim_0B.bit14 = "bit14";
            this.regstim_0B.bit15 = "bit15";
            this.regstim_0B.bit16 = "bit16";
            this.regstim_0B.bit17 = "bit17";
            this.regstim_0B.bit2 = "bit2";
            this.regstim_0B.bit3 = "bit3";
            this.regstim_0B.bit4 = "bit4";
            this.regstim_0B.bit5 = "bit5";
            this.regstim_0B.bit6 = "bit6";
            this.regstim_0B.bit7 = "bit7";
            this.regstim_0B.bit8 = "bit8";
            this.regstim_0B.bit9 = "bit9";
            this.regstim_0B.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_0B.Location = new System.Drawing.Point(20, 515);
            this.regstim_0B.Name = "regstim_0B";
            this.regstim_0B.RegisterName = null;
            this.regstim_0B.Size = new System.Drawing.Size(890, 38);
            this.regstim_0B.TabIndex = 161;
            // 
            // regstim_12
            // 
            this.regstim_12.Address = null;
            this.regstim_12.bit0 = "bit0";
            this.regstim_12.bit1 = "bit1";
            this.regstim_12.bit10 = "bit10";
            this.regstim_12.bit11 = "bit11";
            this.regstim_12.bit12 = "bit12";
            this.regstim_12.bit13 = "bit13";
            this.regstim_12.bit14 = "bit14";
            this.regstim_12.bit15 = "bit15";
            this.regstim_12.bit16 = "bit16";
            this.regstim_12.bit17 = "bit17";
            this.regstim_12.bit2 = "bit2";
            this.regstim_12.bit3 = "bit3";
            this.regstim_12.bit4 = "bit4";
            this.regstim_12.bit5 = "bit5";
            this.regstim_12.bit6 = "bit6";
            this.regstim_12.bit7 = "bit7";
            this.regstim_12.bit8 = "bit8";
            this.regstim_12.bit9 = "bit9";
            this.regstim_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_12.Location = new System.Drawing.Point(958, 119);
            this.regstim_12.Name = "regstim_12";
            this.regstim_12.RegisterName = null;
            this.regstim_12.Size = new System.Drawing.Size(890, 38);
            this.regstim_12.TabIndex = 168;
            // 
            // regstim_0C
            // 
            this.regstim_0C.Address = null;
            this.regstim_0C.bit0 = "bit0";
            this.regstim_0C.bit1 = "bit1";
            this.regstim_0C.bit10 = "bit10";
            this.regstim_0C.bit11 = "bit11";
            this.regstim_0C.bit12 = "bit12";
            this.regstim_0C.bit13 = "bit13";
            this.regstim_0C.bit14 = "bit14";
            this.regstim_0C.bit15 = "bit15";
            this.regstim_0C.bit16 = "bit16";
            this.regstim_0C.bit17 = "bit17";
            this.regstim_0C.bit2 = "bit2";
            this.regstim_0C.bit3 = "bit3";
            this.regstim_0C.bit4 = "bit4";
            this.regstim_0C.bit5 = "bit5";
            this.regstim_0C.bit6 = "bit6";
            this.regstim_0C.bit7 = "bit7";
            this.regstim_0C.bit8 = "bit8";
            this.regstim_0C.bit9 = "bit9";
            this.regstim_0C.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_0C.Location = new System.Drawing.Point(20, 559);
            this.regstim_0C.Name = "regstim_0C";
            this.regstim_0C.RegisterName = null;
            this.regstim_0C.Size = new System.Drawing.Size(890, 38);
            this.regstim_0C.TabIndex = 162;
            // 
            // regstim_11
            // 
            this.regstim_11.Address = null;
            this.regstim_11.bit0 = "bit0";
            this.regstim_11.bit1 = "bit1";
            this.regstim_11.bit10 = "bit10";
            this.regstim_11.bit11 = "bit11";
            this.regstim_11.bit12 = "bit12";
            this.regstim_11.bit13 = "bit13";
            this.regstim_11.bit14 = "bit14";
            this.regstim_11.bit15 = "bit15";
            this.regstim_11.bit16 = "bit16";
            this.regstim_11.bit17 = "bit17";
            this.regstim_11.bit2 = "bit2";
            this.regstim_11.bit3 = "bit3";
            this.regstim_11.bit4 = "bit4";
            this.regstim_11.bit5 = "bit5";
            this.regstim_11.bit6 = "bit6";
            this.regstim_11.bit7 = "bit7";
            this.regstim_11.bit8 = "bit8";
            this.regstim_11.bit9 = "bit9";
            this.regstim_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_11.Location = new System.Drawing.Point(958, 75);
            this.regstim_11.Name = "regstim_11";
            this.regstim_11.RegisterName = null;
            this.regstim_11.Size = new System.Drawing.Size(890, 38);
            this.regstim_11.TabIndex = 167;
            // 
            // regstim_0D
            // 
            this.regstim_0D.Address = null;
            this.regstim_0D.bit0 = "bit0";
            this.regstim_0D.bit1 = "bit1";
            this.regstim_0D.bit10 = "bit10";
            this.regstim_0D.bit11 = "bit11";
            this.regstim_0D.bit12 = "bit12";
            this.regstim_0D.bit13 = "bit13";
            this.regstim_0D.bit14 = "bit14";
            this.regstim_0D.bit15 = "bit15";
            this.regstim_0D.bit16 = "bit16";
            this.regstim_0D.bit17 = "bit17";
            this.regstim_0D.bit2 = "bit2";
            this.regstim_0D.bit3 = "bit3";
            this.regstim_0D.bit4 = "bit4";
            this.regstim_0D.bit5 = "bit5";
            this.regstim_0D.bit6 = "bit6";
            this.regstim_0D.bit7 = "bit7";
            this.regstim_0D.bit8 = "bit8";
            this.regstim_0D.bit9 = "bit9";
            this.regstim_0D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_0D.Location = new System.Drawing.Point(20, 603);
            this.regstim_0D.Name = "regstim_0D";
            this.regstim_0D.RegisterName = null;
            this.regstim_0D.Size = new System.Drawing.Size(890, 38);
            this.regstim_0D.TabIndex = 163;
            // 
            // regstim_10
            // 
            this.regstim_10.Address = null;
            this.regstim_10.bit0 = "bit0";
            this.regstim_10.bit1 = "bit1";
            this.regstim_10.bit10 = "bit10";
            this.regstim_10.bit11 = "bit11";
            this.regstim_10.bit12 = "bit12";
            this.regstim_10.bit13 = "bit13";
            this.regstim_10.bit14 = "bit14";
            this.regstim_10.bit15 = "bit15";
            this.regstim_10.bit16 = "bit16";
            this.regstim_10.bit17 = "bit17";
            this.regstim_10.bit2 = "bit2";
            this.regstim_10.bit3 = "bit3";
            this.regstim_10.bit4 = "bit4";
            this.regstim_10.bit5 = "bit5";
            this.regstim_10.bit6 = "bit6";
            this.regstim_10.bit7 = "bit7";
            this.regstim_10.bit8 = "bit8";
            this.regstim_10.bit9 = "bit9";
            this.regstim_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_10.Location = new System.Drawing.Point(958, 31);
            this.regstim_10.Name = "regstim_10";
            this.regstim_10.RegisterName = null;
            this.regstim_10.Size = new System.Drawing.Size(890, 38);
            this.regstim_10.TabIndex = 166;
            // 
            // regstim_0E
            // 
            this.regstim_0E.Address = null;
            this.regstim_0E.bit0 = "bit0";
            this.regstim_0E.bit1 = "bit1";
            this.regstim_0E.bit10 = "bit10";
            this.regstim_0E.bit11 = "bit11";
            this.regstim_0E.bit12 = "bit12";
            this.regstim_0E.bit13 = "bit13";
            this.regstim_0E.bit14 = "bit14";
            this.regstim_0E.bit15 = "bit15";
            this.regstim_0E.bit16 = "bit16";
            this.regstim_0E.bit17 = "bit17";
            this.regstim_0E.bit2 = "bit2";
            this.regstim_0E.bit3 = "bit3";
            this.regstim_0E.bit4 = "bit4";
            this.regstim_0E.bit5 = "bit5";
            this.regstim_0E.bit6 = "bit6";
            this.regstim_0E.bit7 = "bit7";
            this.regstim_0E.bit8 = "bit8";
            this.regstim_0E.bit9 = "bit9";
            this.regstim_0E.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_0E.Location = new System.Drawing.Point(20, 647);
            this.regstim_0E.Name = "regstim_0E";
            this.regstim_0E.RegisterName = null;
            this.regstim_0E.Size = new System.Drawing.Size(890, 38);
            this.regstim_0E.TabIndex = 164;
            // 
            // regstim_0F
            // 
            this.regstim_0F.Address = null;
            this.regstim_0F.bit0 = "bit0";
            this.regstim_0F.bit1 = "bit1";
            this.regstim_0F.bit10 = "bit10";
            this.regstim_0F.bit11 = "bit11";
            this.regstim_0F.bit12 = "bit12";
            this.regstim_0F.bit13 = "bit13";
            this.regstim_0F.bit14 = "bit14";
            this.regstim_0F.bit15 = "bit15";
            this.regstim_0F.bit16 = "bit16";
            this.regstim_0F.bit17 = "bit17";
            this.regstim_0F.bit2 = "bit2";
            this.regstim_0F.bit3 = "bit3";
            this.regstim_0F.bit4 = "bit4";
            this.regstim_0F.bit5 = "bit5";
            this.regstim_0F.bit6 = "bit6";
            this.regstim_0F.bit7 = "bit7";
            this.regstim_0F.bit8 = "bit8";
            this.regstim_0F.bit9 = "bit9";
            this.regstim_0F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regstim_0F.Location = new System.Drawing.Point(20, 691);
            this.regstim_0F.Name = "regstim_0F";
            this.regstim_0F.RegisterName = null;
            this.regstim_0F.Size = new System.Drawing.Size(890, 38);
            this.regstim_0F.TabIndex = 165;
            // 
            // tabStimRegisters
            // 
            this.tabStimRegisters.Controls.Add(this.tabPage11);
            this.tabStimRegisters.Controls.Add(this.tabPage13);
            this.tabStimRegisters.Controls.Add(this.tabPage14);
            this.tabStimRegisters.Controls.Add(this.tabPage15);
            this.tabStimRegisters.Controls.Add(this.tabPage16);
            this.tabStimRegisters.Controls.Add(this.tabPage17);
            this.tabStimRegisters.Controls.Add(this.tabPage18);
            this.tabStimRegisters.Controls.Add(this.tabPage19);
            this.tabStimRegisters.Controls.Add(this.tabPage20);
            this.tabStimRegisters.HotTrack = true;
            this.tabStimRegisters.Location = new System.Drawing.Point(10, 6);
            this.tabStimRegisters.Name = "tabStimRegisters";
            this.tabStimRegisters.SelectedIndex = 0;
            this.tabStimRegisters.Size = new System.Drawing.Size(1848, 749);
            this.tabStimRegisters.TabIndex = 1;
            this.tabStimRegisters.SelectedIndexChanged += new System.EventHandler(this.tabStimRegistersUpdate);
            this.tabStimRegisters.Enter += new System.EventHandler(this.tabStimRegistersFocus);
            // 
            // tabPage11
            // 
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(1840, 723);
            this.tabPage11.TabIndex = 0;
            this.tabPage11.Text = "0x8000_0000 - 0x8000_001F";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // tabPage13
            // 
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(1840, 723);
            this.tabPage13.TabIndex = 1;
            this.tabPage13.Text = "0x8000_0020 - 0x8000_003F";
            this.tabPage13.UseVisualStyleBackColor = true;
            this.tabPage13.Click += new System.EventHandler(this.tabPage13_Click);
            // 
            // tabPage14
            // 
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(1840, 723);
            this.tabPage14.TabIndex = 2;
            this.tabPage14.Text = "0x8000_0040 - 0x8000_005F";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // tabPage15
            // 
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(1840, 723);
            this.tabPage15.TabIndex = 3;
            this.tabPage15.Text = "0x9000_0000 - 0x9000_001F";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // tabPage16
            // 
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Size = new System.Drawing.Size(1840, 723);
            this.tabPage16.TabIndex = 4;
            this.tabPage16.Text = "0x9000_0100 - 0x9000_011F";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // tabPage17
            // 
            this.tabPage17.Location = new System.Drawing.Point(4, 22);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Size = new System.Drawing.Size(1840, 723);
            this.tabPage17.TabIndex = 6;
            this.tabPage17.Text = "0x9000_0200 - 0x9000_020F";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // tabPage18
            // 
            this.tabPage18.Location = new System.Drawing.Point(4, 22);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Size = new System.Drawing.Size(1840, 723);
            this.tabPage18.TabIndex = 7;
            this.tabPage18.Text = "0xC000_0000 - 0xC000_001F";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // tabPage19
            // 
            this.tabPage19.Location = new System.Drawing.Point(4, 22);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Size = new System.Drawing.Size(1840, 723);
            this.tabPage19.TabIndex = 8;
            this.tabPage19.Text = "0xC000_0020 - 0xE000_001F";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // tabPage20
            // 
            this.tabPage20.Location = new System.Drawing.Point(4, 22);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Size = new System.Drawing.Size(1840, 723);
            this.tabPage20.TabIndex = 9;
            this.tabPage20.Text = "0xF000_0000 - 0xF000_001F";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.lblTemp2MSE);
            this.tabPage21.Controls.Add(this.label8);
            this.tabPage21.Controls.Add(this.lblTemp2Mean);
            this.tabPage21.Controls.Add(this.lblTemp2Meana);
            this.tabPage21.Controls.Add(this.lblTemp1MSE);
            this.tabPage21.Controls.Add(this.label9);
            this.tabPage21.Controls.Add(this.lblTemp1Mean);
            this.tabPage21.Controls.Add(this.label6);
            this.tabPage21.Controls.Add(this.btnTemperatureSensorsRun);
            this.tabPage21.Controls.Add(this.chartTempSensors);
            this.tabPage21.Location = new System.Drawing.Point(4, 22);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Size = new System.Drawing.Size(192, 74);
            this.tabPage21.TabIndex = 6;
            this.tabPage21.Text = "Temperature Sensors";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // lblTemp2MSE
            // 
            this.lblTemp2MSE.AutoSize = true;
            this.lblTemp2MSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemp2MSE.Location = new System.Drawing.Point(1181, 719);
            this.lblTemp2MSE.Name = "lblTemp2MSE";
            this.lblTemp2MSE.Size = new System.Drawing.Size(0, 25);
            this.lblTemp2MSE.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1033, 719);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 25);
            this.label8.TabIndex = 9;
            this.label8.Text = "Temp 2 MSE:";
            // 
            // lblTemp2Mean
            // 
            this.lblTemp2Mean.AutoSize = true;
            this.lblTemp2Mean.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemp2Mean.Location = new System.Drawing.Point(1181, 694);
            this.lblTemp2Mean.Name = "lblTemp2Mean";
            this.lblTemp2Mean.Size = new System.Drawing.Size(0, 25);
            this.lblTemp2Mean.TabIndex = 8;
            // 
            // lblTemp2Meana
            // 
            this.lblTemp2Meana.AutoSize = true;
            this.lblTemp2Meana.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemp2Meana.Location = new System.Drawing.Point(1024, 694);
            this.lblTemp2Meana.Name = "lblTemp2Meana";
            this.lblTemp2Meana.Size = new System.Drawing.Size(162, 25);
            this.lblTemp2Meana.TabIndex = 7;
            this.lblTemp2Meana.Text = "Temp 2 Mean:";
            // 
            // lblTemp1MSE
            // 
            this.lblTemp1MSE.AutoSize = true;
            this.lblTemp1MSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemp1MSE.Location = new System.Drawing.Point(892, 719);
            this.lblTemp1MSE.Name = "lblTemp1MSE";
            this.lblTemp1MSE.Size = new System.Drawing.Size(0, 25);
            this.lblTemp1MSE.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(744, 719);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(153, 25);
            this.label9.TabIndex = 5;
            this.label9.Text = "Temp 1 MSE:";
            // 
            // lblTemp1Mean
            // 
            this.lblTemp1Mean.AutoSize = true;
            this.lblTemp1Mean.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemp1Mean.Location = new System.Drawing.Point(892, 694);
            this.lblTemp1Mean.Name = "lblTemp1Mean";
            this.lblTemp1Mean.Size = new System.Drawing.Size(0, 25);
            this.lblTemp1Mean.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(735, 694);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(162, 25);
            this.label6.TabIndex = 3;
            this.label6.Text = "Temp 1 Mean:";
            // 
            // btnTemperatureSensorsRun
            // 
            this.btnTemperatureSensorsRun.Location = new System.Drawing.Point(486, 25);
            this.btnTemperatureSensorsRun.Name = "btnTemperatureSensorsRun";
            this.btnTemperatureSensorsRun.Size = new System.Drawing.Size(147, 55);
            this.btnTemperatureSensorsRun.TabIndex = 1;
            this.btnTemperatureSensorsRun.Text = "Run 60 Minutes";
            this.btnTemperatureSensorsRun.UseVisualStyleBackColor = true;
            this.btnTemperatureSensorsRun.Click += new System.EventHandler(this.btnTemperatureSensorsRun_Click);
            // 
            // chartTempSensors
            // 
            this.chartTempSensors.BorderlineColor = System.Drawing.Color.Black;
            this.chartTempSensors.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chartTempSensors.BorderlineWidth = 3;
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LabelAutoFitMaxFontSize = 16;
            chartArea1.AxisX.LabelAutoFitMinFontSize = 10;
            chartArea1.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY.Interval = 1D;
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.IsStartedFromZero = false;
            chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY.Minimum = 20D;
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.Name = "ChartArea1";
            this.chartTempSensors.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartTempSensors.Legends.Add(legend1);
            this.chartTempSensors.Location = new System.Drawing.Point(698, 25);
            this.chartTempSensors.Name = "chartTempSensors";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartTempSensors.Series.Add(series1);
            this.chartTempSensors.Size = new System.Drawing.Size(1089, 666);
            this.chartTempSensors.TabIndex = 0;
            this.chartTempSensors.Text = "chart1";
            // 
            // txtMACAddress
            // 
            this.txtMACAddress.Location = new System.Drawing.Point(25, 34);
            this.txtMACAddress.Name = "txtMACAddress";
            this.txtMACAddress.Size = new System.Drawing.Size(115, 20);
            this.txtMACAddress.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Implant MAC Address:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblBatteryTemperature2);
            this.groupBox4.Controls.Add(this.lblBatteryTemperature1);
            this.groupBox4.Controls.Add(this.lblBatteryVoltage);
            this.groupBox4.Controls.Add(this.btnRefreshImplantStatus);
            this.groupBox4.Location = new System.Drawing.Point(913, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(976, 100);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Implant Status:";
            // 
            // lblBatteryTemperature2
            // 
            this.lblBatteryTemperature2.AutoSize = true;
            this.lblBatteryTemperature2.Location = new System.Drawing.Point(250, 62);
            this.lblBatteryTemperature2.Name = "lblBatteryTemperature2";
            this.lblBatteryTemperature2.Size = new System.Drawing.Size(14, 13);
            this.lblBatteryTemperature2.TabIndex = 3;
            this.lblBatteryTemperature2.Text = "C";
            // 
            // lblBatteryTemperature1
            // 
            this.lblBatteryTemperature1.AutoSize = true;
            this.lblBatteryTemperature1.Location = new System.Drawing.Point(129, 62);
            this.lblBatteryTemperature1.Name = "lblBatteryTemperature1";
            this.lblBatteryTemperature1.Size = new System.Drawing.Size(14, 13);
            this.lblBatteryTemperature1.TabIndex = 2;
            this.lblBatteryTemperature1.Text = "C";
            // 
            // lblBatteryVoltage
            // 
            this.lblBatteryVoltage.AutoSize = true;
            this.lblBatteryVoltage.Location = new System.Drawing.Point(129, 28);
            this.lblBatteryVoltage.Name = "lblBatteryVoltage";
            this.lblBatteryVoltage.Size = new System.Drawing.Size(39, 13);
            this.lblBatteryVoltage.TabIndex = 1;
            this.lblBatteryVoltage.Text = "0 Volts";
            // 
            // btnRefreshImplantStatus
            // 
            this.btnRefreshImplantStatus.Location = new System.Drawing.Point(27, 28);
            this.btnRefreshImplantStatus.Name = "btnRefreshImplantStatus";
            this.btnRefreshImplantStatus.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshImplantStatus.TabIndex = 0;
            this.btnRefreshImplantStatus.Text = "Refresh";
            this.btnRefreshImplantStatus.UseVisualStyleBackColor = true;
            this.btnRefreshImplantStatus.Click += new System.EventHandler(this.btnRefreshImplantStatus_Click);
            // 
            // btnDisableLoop
            // 
            this.btnDisableLoop.Location = new System.Drawing.Point(391, 12);
            this.btnDisableLoop.Name = "btnDisableLoop";
            this.btnDisableLoop.Size = new System.Drawing.Size(106, 26);
            this.btnDisableLoop.TabIndex = 25;
            this.btnDisableLoop.Text = "Disable Loop";
            this.btnDisableLoop.UseVisualStyleBackColor = true;
            this.btnDisableLoop.Click += new System.EventHandler(this.btnDisableLoop_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(391, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 26);
            this.button1.TabIndex = 26;
            this.button1.Text = "Enable Loop";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_4);
            // 
            // Test_SPI
            // 
            this.ClientSize = new System.Drawing.Size(1935, 889);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnDisableLoop);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtMACAddress);
            this.Controls.Add(this.btnResetComm);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lstCommPorts);
            this.Controls.Add(this.btnCommSet);
            this.Name = "Test_SPI";
            this.Text = "Acclaim Fitting Commander 0.0.4A";
            this.Load += new System.EventHandler(this.Test_SPI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridRead)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridEEPROM)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabRegisters.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage12.ResumeLayout(false);
            this.tabStimRegisters.ResumeLayout(false);
            this.tabPage21.ResumeLayout(false);
            this.tabPage21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTempSensors)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lstCommPorts;
        private System.Windows.Forms.Button btnResetComm;
        private System.Windows.Forms.Button btnCommSet;
        private System.Windows.Forms.TextBox txtReadStart;
        private System.Windows.Forms.TextBox txtReadStop;
        private System.Windows.Forms.Button btnReadASIC;
        private System.Windows.Forms.Button btnLoadFile;
        private System.Windows.Forms.Button btnClearData;
        private System.Windows.Forms.Button btnWriteAll;
        private System.Windows.Forms.Button btnSaveData;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.DataGridView DataGridRead;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private ASIC_Test_Interface.E3_Register32 reg_1F;
        private ASIC_Test_Interface.E3_Register32 reg_1C;
        private ASIC_Test_Interface.E3_Register32 reg_1E;
        private ASIC_Test_Interface.E3_Register32 reg_00;
        private ASIC_Test_Interface.E3_Register32 reg_1D;
        private ASIC_Test_Interface.E3_Register32 reg_01;
        private ASIC_Test_Interface.E3_Register32 reg_02;
        private ASIC_Test_Interface.E3_Register32 reg_1B;
        private ASIC_Test_Interface.E3_Register32 reg_03;
        private ASIC_Test_Interface.E3_Register32 reg_1A;
        private ASIC_Test_Interface.E3_Register32 reg_04;
        private ASIC_Test_Interface.E3_Register32 reg_19;
        private ASIC_Test_Interface.E3_Register32 reg_05;
        private ASIC_Test_Interface.E3_Register32 reg_18;
        private ASIC_Test_Interface.E3_Register32 reg_06;
        private ASIC_Test_Interface.E3_Register32 reg_17;
        private ASIC_Test_Interface.E3_Register32 reg_07;
        private ASIC_Test_Interface.E3_Register32 reg_16;
        private ASIC_Test_Interface.E3_Register32 reg_08;
        private ASIC_Test_Interface.E3_Register32 reg_15;
        private ASIC_Test_Interface.E3_Register32 reg_09;
        private ASIC_Test_Interface.E3_Register32 reg_14;
        private ASIC_Test_Interface.E3_Register32 reg_0A;
        private ASIC_Test_Interface.E3_Register32 reg_13;
        private ASIC_Test_Interface.E3_Register32 reg_0B;
        private ASIC_Test_Interface.E3_Register32 reg_12;
        private ASIC_Test_Interface.E3_Register32 reg_0C;
        private ASIC_Test_Interface.E3_Register32 reg_11;
        private ASIC_Test_Interface.E3_Register32 reg_0D;
        private ASIC_Test_Interface.E3_Register32 reg_10;
        private ASIC_Test_Interface.E3_Register32 reg_0E;
        private ASIC_Test_Interface.E3_Register32 reg_0F;
        private System.Windows.Forms.TabControl tabRegisters;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage10;
        private ASIC_Test_Interface.E3_Register8 e3_Register833;
        private ASIC_Test_Interface.E3_Register8 e3_Register834;
        private ASIC_Test_Interface.E3_Register8 e3_Register835;
        private ASIC_Test_Interface.E3_Register8 e3_Register836;
        private ASIC_Test_Interface.E3_Register8 e3_Register837;
        private ASIC_Test_Interface.E3_Register8 e3_Register838;
        private ASIC_Test_Interface.E3_Register8 e3_Register839;
        private ASIC_Test_Interface.E3_Register8 e3_Register840;
        private ASIC_Test_Interface.E3_Register8 e3_Register841;
        private ASIC_Test_Interface.E3_Register8 e3_Register842;
        private ASIC_Test_Interface.E3_Register8 e3_Register843;
        private ASIC_Test_Interface.E3_Register8 e3_Register844;
        private ASIC_Test_Interface.E3_Register8 e3_Register845;
        private ASIC_Test_Interface.E3_Register8 e3_Register846;
        private ASIC_Test_Interface.E3_Register8 e3_Register847;
        private ASIC_Test_Interface.E3_Register8 e3_Register848;
        private ASIC_Test_Interface.E3_Register8 e3_Register817;
        private ASIC_Test_Interface.E3_Register8 e3_Register818;
        private ASIC_Test_Interface.E3_Register8 e3_Register819;
        private ASIC_Test_Interface.E3_Register8 e3_Register820;
        private ASIC_Test_Interface.E3_Register8 e3_Register821;
        private ASIC_Test_Interface.E3_Register8 e3_Register822;
        private ASIC_Test_Interface.E3_Register8 e3_Register823;
        private ASIC_Test_Interface.E3_Register8 e3_Register824;
        private ASIC_Test_Interface.E3_Register8 e3_Register825;
        private ASIC_Test_Interface.E3_Register8 e3_Register826;
        private ASIC_Test_Interface.E3_Register8 e3_Register827;
        private ASIC_Test_Interface.E3_Register8 e3_Register828;
        private ASIC_Test_Interface.E3_Register8 e3_Register829;
        private ASIC_Test_Interface.E3_Register8 e3_Register830;
        private ASIC_Test_Interface.E3_Register8 e3_Register831;
        private ASIC_Test_Interface.E3_Register8 e3_Register832;
        private ASIC_Test_Interface.E3_Register8 e3_Register813;
        private ASIC_Test_Interface.E3_Register8 e3_Register814;
        private ASIC_Test_Interface.E3_Register8 e3_Register815;
        private ASIC_Test_Interface.E3_Register8 e3_Register816;
        private ASIC_Test_Interface.E3_Register8 e3_Register87;
        private ASIC_Test_Interface.E3_Register8 e3_Register88;
        private ASIC_Test_Interface.E3_Register8 e3_Register89;
        private ASIC_Test_Interface.E3_Register8 e3_Register810;
        private ASIC_Test_Interface.E3_Register8 e3_Register811;
        private ASIC_Test_Interface.E3_Register8 e3_Register812;
        private ASIC_Test_Interface.E3_Register8 e3_Register86;
        private ASIC_Test_Interface.E3_Register8 e3_Register85;
        private ASIC_Test_Interface.E3_Register8 e3_Register84;
        private ASIC_Test_Interface.E3_Register8 e3_Register83;
        private ASIC_Test_Interface.E3_Register8 e3_Register82;
        private ASIC_Test_Interface.E3_Register8 diagPort01;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnWriteFlash;
        private System.Windows.Forms.Button btnReadFlash;
        private System.Windows.Forms.TextBox txtMACAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnEraseFlash;
        private System.Windows.Forms.Button btnReadFlashStatus;
        private System.Windows.Forms.TextBox txtFlashStatus;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtPMICEEPROMFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnReadConfigEEPROM;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dataGridEEPROM;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnLoadCheckSum;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddressEE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataEE;
        private ASIC_Test_Interface.E3_Register32 regstim_1F;
        private ASIC_Test_Interface.E3_Register32 regstim_1C;
        private ASIC_Test_Interface.E3_Register32 regstim_1E;
        private ASIC_Test_Interface.E3_Register32 regstim_00;
        private ASIC_Test_Interface.E3_Register32 regstim_1D;
        private ASIC_Test_Interface.E3_Register32 regstim_01;
        private ASIC_Test_Interface.E3_Register32 regstim_02;
        private ASIC_Test_Interface.E3_Register32 regstim_1B;
        private ASIC_Test_Interface.E3_Register32 regstim_03;
        private ASIC_Test_Interface.E3_Register32 regstim_1A;
        private ASIC_Test_Interface.E3_Register32 regstim_04;
        private ASIC_Test_Interface.E3_Register32 regstim_19;
        private ASIC_Test_Interface.E3_Register32 regstim_05;
        private ASIC_Test_Interface.E3_Register32 regstim_18;
        private ASIC_Test_Interface.E3_Register32 regstim_06;
        private ASIC_Test_Interface.E3_Register32 regstim_17;
        private ASIC_Test_Interface.E3_Register32 regstim_07;
        private ASIC_Test_Interface.E3_Register32 regstim_16;
        private ASIC_Test_Interface.E3_Register32 regstim_08;
        private ASIC_Test_Interface.E3_Register32 regstim_15;
        private ASIC_Test_Interface.E3_Register32 regstim_09;
        private ASIC_Test_Interface.E3_Register32 regstim_14;
        private ASIC_Test_Interface.E3_Register32 regstim_0A;
        private ASIC_Test_Interface.E3_Register32 regstim_13;
        private ASIC_Test_Interface.E3_Register32 regstim_0B;
        private ASIC_Test_Interface.E3_Register32 regstim_12;
        private ASIC_Test_Interface.E3_Register32 regstim_0C;
        private ASIC_Test_Interface.E3_Register32 regstim_11;
        private ASIC_Test_Interface.E3_Register32 regstim_0D;
        private ASIC_Test_Interface.E3_Register32 regstim_10;
        private ASIC_Test_Interface.E3_Register32 regstim_0E;
        private ASIC_Test_Interface.E3_Register32 regstim_0F;
        private System.Windows.Forms.TabControl tabStimRegisters;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnRefreshImplantStatus;
        private System.Windows.Forms.Label lblBatteryVoltage;
        private System.Windows.Forms.Label lblBatteryTemperature1;
        private System.Windows.Forms.Label lblBatteryTemperature2;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.Button btnTemperatureSensorsRun;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTempSensors;
        private System.Windows.Forms.Button btnDisableLoop;
        private System.Windows.Forms.Label lblTemp2MSE;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblTemp2Mean;
        private System.Windows.Forms.Label lblTemp2Meana;
        private System.Windows.Forms.Label lblTemp1MSE;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblTemp1Mean;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
    }
}