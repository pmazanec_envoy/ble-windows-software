﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACCLAIM_BLE_SERIAL;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using System.Windows.Forms.DataVisualization.Charting;

namespace BLE_SPI_TEST
{
    public partial class Test_SPI : Form
    {
        public BLE_SPI spi = new BLE_SPI();
        public Test_SPI()
        {
            InitializeComponent();

            lstCommPorts.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames());
            lstCommPorts.SelectedIndex = 0;

            reg_00.Visible = false;
            reg_01.Visible = false;
            reg_02.Visible = false;
            reg_03.Visible = false;
            reg_04.Visible = false;
            reg_05.Visible = false;
            reg_06.Visible = false;
            reg_07.Visible = false;
            reg_08.Visible = false;
            reg_09.Visible = false;
            reg_0A.Visible = false;
            reg_0B.Visible = false;
            reg_0C.Visible = false;
            reg_0D.Visible = false;
            reg_0E.Visible = false;
            reg_0F.Visible = false;
            reg_10.Visible = false;
            reg_11.Visible = false;
            reg_12.Visible = false;
            reg_13.Visible = false;
            reg_14.Visible = false;
            reg_15.Visible = false;
            reg_16.Visible = false;
            reg_17.Visible = false;
            reg_18.Visible = false;
            reg_19.Visible = false;
            reg_1A.Visible = false;
            reg_1B.Visible = false;
            reg_1C.Visible = false;
            reg_1D.Visible = false;
            reg_1E.Visible = false;
            reg_1F.Visible = false;

            regstim_00.Visible = false;
            regstim_01.Visible = false;
            regstim_02.Visible = false;
            regstim_03.Visible = false;
            regstim_04.Visible = false;
            regstim_05.Visible = false;
            regstim_06.Visible = false;
            regstim_07.Visible = false;
            regstim_08.Visible = false;
            regstim_09.Visible = false;
            regstim_0A.Visible = false;
            regstim_0B.Visible = false;
            regstim_0C.Visible = false;
            regstim_0D.Visible = false;
            regstim_0E.Visible = false;
            regstim_0F.Visible = false;
            regstim_10.Visible = false;
            regstim_11.Visible = false;
            regstim_12.Visible = false;
            regstim_13.Visible = false;
            regstim_14.Visible = false;
            regstim_15.Visible = false;
            regstim_16.Visible = false;
            regstim_17.Visible = false;
            regstim_18.Visible = false;
            regstim_19.Visible = false;
            regstim_1A.Visible = false;
            regstim_1B.Visible = false;
            regstim_1C.Visible = false;
            regstim_1D.Visible = false;
            regstim_1E.Visible = false;
            regstim_1F.Visible = false;


        }

        //private void btnReadASICReg_Click(object sender, EventArgs e)
        //{
        //    //byte[] addr = { 0x00, 0x00, 0x00, 0x20 };
        //    byte[] data = { 0x00, 0x00, 0x00, 0x00 }; ;

        //    byte[] addr_array = StringToByteArray(txtReadAddress.Text);

        //    byte[] addr = addr_array.Reverse().ToArray();



        //    txtReadResponse.Text = "";
        //    uint response = BLE_SPI.BLE_SPI_READ_32(addr, ref data);
        //    if (response == 1) {
        //        txtReadResponse.Text = "Success: " + BitConverter.ToString(data);
        //        txtReadData.Text = BitConverter.ToString(data);

        //    } else {
        //        txtReadResponse.Text = "Fail: ";
        //        spi.CloseSerialPort();
        //        spi.InitSerialPort("COM3");

        //    };



        //}

        //private void btnWriteASICReg_Click(object sender, EventArgs e)
        //{
        //    //byte[] addr = { 0x00, 0x00, 0x00, 0x20 };
        //    //byte[] data = { 0x00, 0x00, 0x00, 0x00 }; ;

        //    byte[] addr_array = StringToByteArray(txtWriteAddress.Text);

        //    byte[] addr = addr_array.Reverse().ToArray();

        //    byte[] data_array = StringToByteArray(txtWriteData.Text);

        //    byte[] data = data_array.Reverse().ToArray();


        //    txtWriteResponse.Text = "";
        //    uint response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);
        //    if (response == 1)
        //    {
        //        txtWriteResponse.Text = "Success: " + BitConverter.ToString(data);
        //        txtWriteData.Text = "";

        //    }
        //    else
        //    {
        //        txtWriteResponse.Text = "Fail: ";
        //        //spi.CloseSerialPort();
        //        //spi.InitSerialPort("COM3");

        //    };
        //}

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int startAddress = int.Parse(txtReadStart.Text.Replace("_", ""), System.Globalization.NumberStyles.HexNumber);
            int stopAddress = int.Parse(txtReadStop.Text.Replace("_", ""), System.Globalization.NumberStyles.HexNumber);
            uint response = 0;
            string dataString = "";


            //byte[] addr = { 0x00, 0x00, 0x00, 0x20 };
            byte[] data = { 0x00, 0x00, 0x00, 0x00 };

            byte[] addr = StringToByteArray(txtReadStart.Text.Replace("_", "")).Reverse().ToArray();

            int j = 0;
            for (int i = startAddress; i <= stopAddress; i++) {
                Array.Clear(data, 0, data.Length);
                addr = StringToByteArray((i).ToString("X")).Reverse().ToArray();
                //BLE_SPI.sp.DiscardInBuffer();
                response = BLE_SPI.BLE_SPI_READ_32(addr, ref data);

                if (response == 1)
                {

                    dataString = ByteArrayToString(data);
                    dataString = dataString.Substring(8, 8);
                    DataGridRead.Rows.Add(i.ToString("X").Insert(4, "_"), dataString.Insert(4, "_"));
                }
                else
                {
                    //txtReadResponse.Text = "Fail: ";
                    spi.CloseSerialPort();
                    spi.InitSerialPort("COM3");

                };

                //System.Threading.Thread.Sleep(1000);

                j++;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataGridRead.Rows.Clear();
            DataGridRead.Refresh();
        }

        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;



        }


        private void btnWriteAll_Click(object sender, EventArgs e)
        {
            string addressString;
            string dataString;

            byte[] addr_array = StringToByteArray("20000000");

            byte[] addr = addr_array.Reverse().ToArray();

            byte[] data_array = StringToByteArray("00000000");

            byte[] data = data_array.Reverse().ToArray();
            uint response = 0;


            foreach (DataGridViewRow row in DataGridRead.Rows)
            {
                if (row.Cells["Address"].Value != null)
                {
                    addressString = row.Cells["Address"].Value.ToString().Replace("_", "");
                    dataString = row.Cells["Data"].Value.ToString().Replace("_", "");

                    addr_array = StringToByteArray(addressString);
                    data_array = StringToByteArray(dataString);

                    addr = addr_array.Reverse().ToArray();
                    data = data_array.Reverse().ToArray();



                    response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


                }
            }


        }


        private void btnResetComm_Click(object sender, EventArgs e)
        {
            spi.CloseSerialPort();
            lstCommPorts.ClearSelected();
        }

        private void btnCommSet_Click(object sender, EventArgs e)
        {

            spi.InitSerialPort(lstCommPorts.SelectedItem.ToString());
            byte[] address = StringToByteArray(txtMACAddress.Text);

            BLE_SPI.BLE_SPI_WRITE_MAC(address.Reverse().ToArray());
        }

        public DataTable ReadCSV(string fileName, bool firstRowContainsFieldNames = true)
        {
            DataTable readCSV = GenerateDataTable(fileName, firstRowContainsFieldNames);
            return readCSV;
        }

        private static DataTable GenerateDataTable(string fileName, bool firstRowContainsFieldNames = true)
        {
            DataTable result = new DataTable();

            if (fileName == "")
            {
                return result;
            }

            string delimiters = ",";
            string extension = Path.GetExtension(fileName);

            if (extension.ToLower() == "txt")
                delimiters = "\t";
            else if (extension.ToLower() == "csv")
                delimiters = ",";

            using (TextFieldParser tfp = new TextFieldParser(fileName))
            {
                tfp.SetDelimiters(delimiters);

                // Get The Column Names
                if (!tfp.EndOfData)
                {
                    string[] fields = tfp.ReadFields();

                    for (int i = 0; i < fields.Count(); i++)
                    {
                        if (firstRowContainsFieldNames)
                            result.Columns.Add(fields[i]);
                        else
                            result.Columns.Add("Col" + i);
                    }

                    // If first line is data then add it
                    if (!firstRowContainsFieldNames)
                        result.Rows.Add(fields);
                }

                // Get Remaining Rows from the CSV
                while (!tfp.EndOfData)
                    result.Rows.Add(tfp.ReadFields());
            }

            return result;
        }

        private void btnSaveData_Click(object sender, EventArgs e)
        {
            {
                string CsvFpath = txtFilePath.Text;
                try
                {
                    System.IO.StreamWriter csvFileWriter = new StreamWriter(CsvFpath, false);

                    string columnHeaderText = "";

                    int countColumn = DataGridRead.ColumnCount - 1;

                    if (countColumn >= 0)
                    {
                        columnHeaderText = DataGridRead.Columns[0].HeaderText;
                    }

                    for (int i = 1; i <= countColumn; i++)
                    {
                        columnHeaderText = columnHeaderText + ',' + DataGridRead.Columns[i].HeaderText;
                    }


                    //csvFileWriter.WriteLine(columnHeaderText);

                    foreach (DataGridViewRow dataRowObject in DataGridRead.Rows)
                    {
                        if (!dataRowObject.IsNewRow)
                        {
                            string dataFromGrid = "";

                            dataFromGrid = dataRowObject.Cells[0].Value.ToString();

                            for (int i = 1; i <= countColumn; i++)
                            {
                                dataFromGrid = dataFromGrid + ',' + dataRowObject.Cells[i].Value.ToString();

                                csvFileWriter.WriteLine(dataFromGrid);
                            }
                        }
                    }


                    csvFileWriter.Flush();
                    csvFileWriter.Close();
                }
                catch (Exception exceptionObject)
                {
                    MessageBox.Show(exceptionObject.ToString());
                }
            }
        }



        private void btnReadASIC_Click(object sender, EventArgs e)
        {
            int startAddress = int.Parse(txtReadStart.Text.Replace("_", ""), System.Globalization.NumberStyles.HexNumber);
            int stopAddress = int.Parse(txtReadStop.Text.Replace("_", ""), System.Globalization.NumberStyles.HexNumber);
            uint response = 0;
            string dataString = "";


            //byte[] addr = { 0x00, 0x00, 0x00, 0x20 };
            byte[] data = { 0x00, 0x00, 0x00, 0x00 };

            byte[] addr = StringToByteArray(txtReadStart.Text.Replace("_", "")).Reverse().ToArray();

            int j = 0;
            for (int i = startAddress; i <= stopAddress; i++)
            {
                Array.Clear(data, 0, data.Length);
                addr = StringToByteArray((i).ToString("X")).Reverse().ToArray();
                //BLE_SPI.sp.DiscardInBuffer();
                response = BLE_SPI.BLE_SPI_READ_32(addr, ref data);

                if (response == 1)
                {

                    dataString = ByteArrayToString(data);
                    dataString = dataString.Substring(8, 8);
                    DataGridRead.Rows.Add(i.ToString("X").Insert(4, "_"), dataString.Insert(4, "_"));
                }
                else
                {
                    //txtReadResponse.Text = "Fail: ";
                    spi.CloseSerialPort();
                    spi.InitSerialPort("COM3");

                };

                //System.Threading.Thread.Sleep(1000);

                j++;

            }
        }



        private void button1_Click_1(object sender, EventArgs e)
        {
            DataGridRead.Rows.Clear();
            DataGridRead.Refresh();
        }


        private void tabRegistersUpdate(Object sender, EventArgs e)
        {

            if (tabRegisters.SelectedIndex == 0)
            {
                reg_00.Address = "20000000";
                reg_00.RegisterName = "Telem_Slv_Msg_Word_0";
                reg_00.Enabled = true;
                reg_00.Reset();
                //
                //
                reg_01.Address = "20000001";
                reg_01.RegisterName = "Telem_Slv_Msg_Word_1";
                reg_01.Enabled = true;
                reg_01.Reset();
                //
                //
                reg_02.Address = "20000002";
                reg_02.RegisterName = "Telem_Slv_Msg_Word_2";
                reg_02.Enabled = true;
                reg_02.Reset();
                //
                //
                reg_03.Address = "20000003";
                reg_03.RegisterName = "Telem_Slv_Msg_Word_3";
                reg_03.Enabled = true;
                reg_03.Reset();
                //
                //
                reg_04.Address = "20000004";
                reg_04.RegisterName = "Telem_Slv_Msg_Word_4";
                reg_04.Enabled = true;
                reg_04.Reset();
                //
                //
                reg_05.Address = "20000005";
                reg_05.RegisterName = "Telem_Slv_Msg_Word_5";
                reg_05.Enabled = true;
                reg_05.Reset();
                //
                //
                reg_06.Address = "20000006";
                reg_06.RegisterName = "Telem_Slv_Msg_Word_6";
                reg_06.Enabled = true;
                reg_06.Reset();
                //
                //
                reg_07.Address = "20000007";
                reg_07.RegisterName = "Telem_Slv_Msg_Word_7";
                reg_07.Enabled = true;
                reg_07.Reset();
                //
                //
                reg_08.Address = "20000008";
                reg_08.RegisterName = "";
                reg_08.Enabled = false;
                reg_08.Reset();
                //
                //
                reg_09.Address = "20000009";
                reg_09.RegisterName = "";
                reg_09.Enabled = false;
                reg_09.Reset();
                //
                //
                reg_0A.Address = "2000000A";
                reg_0A.RegisterName = "";
                reg_0A.Enabled = false;
                reg_0A.Reset();
                //
                //
                reg_0B.Address = "2000000B";
                reg_0B.RegisterName = "";
                reg_0B.Enabled = false;
                reg_0B.Reset();
                //
                //
                reg_0C.Address = "2000000C";
                reg_0C.RegisterName = "";
                reg_0C.Enabled = false;
                reg_0C.Reset();
                //
                //
                reg_0D.Address = "2000000D";
                reg_0D.RegisterName = "";
                reg_0D.Enabled = false;
                reg_0D.Reset();
                //
                //
                reg_0E.Address = "2000000E";
                reg_0E.RegisterName = "";
                reg_0E.Enabled = false;
                reg_0E.Reset();
                //
                //
                reg_0F.Address = "2000000F";
                reg_0F.RegisterName = "f";
                reg_0F.Enabled = false;
                reg_0F.Reset();
                //
                //
                reg_10.Address = "20000010";
                reg_10.RegisterName = "Telem_Slv_Msg_Request";
                reg_10.Enabled = true;
                reg_10.Reset();
                //
                //
                reg_11.Address = "20000011";
                reg_11.RegisterName = "Telem_Slv_Msg_Response";
                reg_11.Enabled = true;
                reg_11.Reset();
                //
                //
                reg_12.Address = "20000012";
                reg_12.RegisterName = "Telem_Slv_Serial_State";
                reg_12.Enabled = true;
                reg_12.Reset();
                //
                //
                reg_13.Address = "20000013";
                reg_13.RegisterName = "Telem_Slv_Link_State";
                reg_13.Enabled = true;
                reg_13.Reset();
                //
                //
                reg_14.Address = "20000014";
                reg_14.RegisterName = "Telem_Slv_Timer_Ctrl";
                reg_14.Enabled = true;
                reg_14.Reset();
                //
                //
                reg_15.Address = "20000015";
                reg_15.RegisterName = "Telem_Slv_Control";
                reg_15.Enabled = true;
                reg_15.Reset();
                //
                //
                reg_16.Address = "20000016";
                reg_16.RegisterName = "Telem_Slv_Status";
                reg_16.Enabled = true;
                reg_16.Reset();
                //
                //
                reg_17.Address = "20000017";
                reg_17.RegisterName = "Telem_Slv_Rechg_Status";
                reg_17.Enabled = true;
                reg_17.Reset();
                //
                //
                reg_18.Address = "20000018";
                reg_18.RegisterName = "";
                reg_18.Enabled = false;
                reg_18.Reset();
                //
                //
                reg_19.Address = "20000019";
                reg_19.RegisterName = "Telem_Slv_PRX_Adj_Tx_1";
                reg_19.Enabled = true;
                reg_19.Reset();
                //
                //
                reg_1A.Address = "2000001A";
                reg_1A.RegisterName = "Telem_Slv_PRX_Adj_VINHI_Tx_0";
                reg_1A.Enabled = true;
                reg_1A.Reset();
                //
                //
                reg_1B.Address = "2000001B";
                reg_1B.RegisterName = "Telem_Slv_PRX_Adj_VINHI_Tx_1";
                reg_1B.Enabled = true;
                reg_1B.Reset();
                //
                //
                reg_1C.Address = "2000001C";
                reg_1C.RegisterName = "Telem_Slv_PRX_Adj_VINOV_Tx_0";
                reg_1C.Enabled = true;
                reg_1C.Reset();
                //
                //
                reg_1D.Address = "2000001D";
                reg_1D.RegisterName = "Telem_Slv_PRX_Adj_VINOV_Tx_1";
                reg_1D.Enabled = true;
                reg_1D.Reset();
                //
                //
                reg_1E.Address = "2000001E";
                reg_1E.RegisterName = "Telem_Slv_PRX_Adj_Inactive";
                reg_1E.Enabled = true;
                reg_1E.Reset();
                //
                //
                reg_1F.Address = "2000001F";
                reg_1F.RegisterName = "Telem_CRX_Adj";
                reg_1F.Enabled = true;
                reg_1F.Reset();
                //
                //




            }

            if (tabRegisters.SelectedIndex == 1)
            {
                reg_00.Address = "30000000";
                reg_00.RegisterName = "ADC3_Scale";
                reg_00.Enabled = true;
                reg_00.Reset();
                //
                //
                reg_01.Address = "30000001";
                reg_01.RegisterName = "ADC3_Control";
                reg_01.Enabled = true;
                reg_01.Reset();
                //
                //
                reg_02.Address = "30000002";
                reg_02.RegisterName = "ADC3_Results";
                reg_02.Enabled = true;
                reg_02.Reset();
                //
                //
                reg_03.Address = "30000003";
                reg_03.RegisterName = "AMUX3 Test Reg 1";
                reg_03.Enabled = true;
                reg_03.Reset();
                //
                //
                reg_04.Address = "30000004";
                reg_04.RegisterName = "AMUX3 Test Reg 2";
                reg_04.Enabled = true;
                reg_04.Reset();
                //
                //
                reg_05.Address = "30000005";
                reg_05.RegisterName = "AMUX3 Test Reg 3";
                reg_05.Enabled = true;
                reg_05.Reset();
                //
                //
                reg_06.Address = "30000006";
                reg_06.RegisterName = "AMUX3 Test Reg 4";
                reg_06.Enabled = true;
                reg_06.Reset();
                //
                //
                reg_07.Address = "30000007";
                reg_07.RegisterName = "";
                reg_07.Enabled = false;
                reg_07.Reset();
                //
                //
                reg_08.Address = "30000008";
                reg_08.RegisterName = "Temp Sensor Ctrl";
                reg_08.Enabled = true;
                reg_08.Reset();
                //
                //
                reg_09.Address = "30000009";
                reg_09.RegisterName = "";
                reg_09.Enabled = false;
                reg_09.Reset();
                //
                //
                reg_0A.Address = "3000000A";
                reg_0A.RegisterName = "";
                reg_0A.Enabled = false;
                reg_0A.Reset();
                //
                //
                reg_0B.Address = "3000000B";
                reg_0B.RegisterName = "";
                reg_0B.Enabled = false;
                reg_0B.Reset();
                //
                //
                reg_0C.Address = "3000000C";
                reg_0C.RegisterName = "";
                reg_0C.Enabled = false;
                reg_0C.Reset();
                //
                //
                reg_0D.Address = "3000000D";
                reg_0D.RegisterName = "";
                reg_0D.Enabled = false;
                reg_0D.Reset();
                //
                //
                reg_0E.Address = "3000000E";
                reg_0E.RegisterName = "";
                reg_0E.Enabled = false;
                reg_0E.Reset();
                //
                //
                reg_0F.Address = "3000000F";
                reg_0F.RegisterName = "";
                reg_0F.Enabled = false;
                reg_0F.Reset();
                //
                //
                reg_10.Address = "30000010";
                reg_10.RegisterName = "Periph_SPI_Pwr_Ctrl";
                reg_10.Enabled = true;
                reg_10.Reset();
                //
                //
                reg_11.Address = "30000011";
                reg_11.RegisterName = "Ext_Pwr_On_Time_4Wr";
                reg_11.Enabled = true;
                reg_11.Reset();
                //
                //
                reg_12.Address = "30000012";
                reg_12.RegisterName = "Ext_Pwr_On_Time_4Rd";
                reg_12.Enabled = true;
                reg_12.Reset();
                //
                //
                reg_13.Address = "30000013";
                reg_13.RegisterName = "Periph_SPI_Control";
                reg_13.Enabled = true;
                reg_13.Reset();
                //
                //
                reg_14.Address = "30000014";
                reg_14.RegisterName = "Periph_SPI_Status";
                reg_14.Enabled = true;
                reg_14.Reset();
                //
                //
                reg_15.Address = "30000015";
                reg_15.RegisterName = "Flash_Ctrl_Status";
                reg_15.Enabled = true;
                reg_15.Reset();
                //
                //
                reg_16.Address = "30000016";
                reg_16.RegisterName = "Periph_SPI_Mem_Cmd";
                reg_16.Enabled = true;
                reg_16.Reset();
                //
                //
                reg_17.Address = "30000017";
                reg_17.RegisterName = "Periph_SPI_Mem_Page";
                reg_17.Enabled = true;
                reg_17.Reset();
                //
                //
                reg_18.Address = "30000018";
                reg_18.RegisterName = "Periph_SPI_Mem_Adrs";
                reg_18.Enabled = true;
                reg_18.Reset();
                //
                //
                reg_19.Address = "30000019";
                reg_19.RegisterName = "Periph_SPI_Wr_Data";
                reg_19.Enabled = true;
                reg_19.Reset();
                //
                //
                reg_1A.Address = "3000001A";
                reg_1A.RegisterName = "Periph_SPI_Rd_Data";
                reg_1A.Enabled = true;
                reg_1A.Reset();
                //
                //
                reg_1B.Address = "3000001B";
                reg_1B.RegisterName = "";
                reg_1B.Enabled = false;
                reg_1B.Reset();
                //
                //
                reg_1C.Address = "3000001C";
                reg_1C.RegisterName = "Assist_Control";
                reg_1C.Enabled = true;
                reg_1C.Reset();
                //
                //
                reg_1D.Address = "3000001D";
                reg_1D.RegisterName = "";
                reg_1D.Enabled = false;
                reg_1D.Reset();
                //
                //
                reg_1E.Address = "3000001E";
                reg_1E.RegisterName = "";
                reg_1E.Enabled = false;
                reg_1E.Reset();
                //
                //
                reg_1F.Address = "3000001F";
                reg_1F.RegisterName = "";
                reg_1F.Enabled = false;
                reg_1F.Reset();
                //
                //




            }

            if (tabRegisters.SelectedIndex == 2)
            {
                reg_00.Address = "40000000";
                reg_00.RegisterName = "Temp_Sensor_Voltage";
                reg_00.Enabled = true;
                reg_00.Reset();
                //
                //
                reg_01.Address = "40000001";
                reg_01.RegisterName = "Temp_Sensor_Tdie";
                reg_01.Enabled = true;
                reg_01.Reset();
                //
                //
                reg_02.Address = "40000002";
                reg_02.RegisterName = "Temp_Sensor_Config";
                reg_02.Enabled = true;
                reg_02.Reset();
                //
                //
                reg_03.Address = "40000003";
                reg_03.RegisterName = "Temp_Sensor_Tobj";
                reg_03.Enabled = true;
                reg_03.Reset();
                //
                //
                reg_04.Address = "40000004";
                reg_04.RegisterName = "Temp_Sensor_Status";
                reg_04.Enabled = true;
                reg_04.Reset();
                //
                //
                reg_05.Address = "40000005";
                reg_05.RegisterName = "Temp_Sensor_Enable";
                reg_05.Enabled = true;
                reg_05.Reset();
                //
                //
                reg_06.Address = "40000006";
                reg_06.RegisterName = "Temp_Sensor_Tobj_Hi";
                reg_06.Enabled = true;
                reg_06.Reset();
                //
                //
                reg_07.Address = "40000007";
                reg_07.RegisterName = "Temp_Sensor_Tobj_Lo";
                reg_07.Enabled = true;
                reg_07.Reset();
                //
                //
                reg_08.Address = "40000008";
                reg_08.RegisterName = "Temp_Sensor_Tdie_Hi";
                reg_08.Enabled = true;
                reg_08.Reset();
                //
                //
                reg_09.Address = "40000009";
                reg_09.RegisterName = "Temp_Sensor_Tdie_Lo";
                reg_09.Enabled = true;
                reg_09.Reset();
                //
                //
                reg_0A.Address = "4000000A";
                reg_0A.RegisterName = "Temp_Sensor_S0_Coef";
                reg_0A.Enabled = true;
                reg_0A.Reset();
                //
                //
                reg_0B.Address = "4000000B";
                reg_0B.RegisterName = "Temp_Sensor_A1_Coef";
                reg_0B.Enabled = true;
                reg_0B.Reset();
                //
                //
                reg_0C.Address = "4000000C";
                reg_0C.RegisterName = "Temp_Sensor_A2_Coef";
                reg_0C.Enabled = true;
                reg_0C.Reset();
                //
                //
                reg_0D.Address = "4000000D";
                reg_0D.RegisterName = "Temp_Sensor_B0_Coef";
                reg_0D.Enabled = true;
                reg_0D.Reset();
                //
                //
                reg_0E.Address = "4000000E";
                reg_0E.RegisterName = "Temp_Sensor_B1_Coef";
                reg_0E.Enabled = true;
                reg_0E.Reset();
                //
                //
                reg_0F.Address = "4000000F";
                reg_0F.RegisterName = "Temp_Sensor_B2_Coef";
                reg_0F.Enabled = true;
                reg_0F.Reset();
                //
                //
                reg_10.Address = "40000010";
                reg_10.RegisterName = "Temp_Sensor_C2_Coef";
                reg_10.Enabled = true;
                reg_10.Reset();
                //
                //
                reg_11.Address = "40000011";
                reg_11.RegisterName = "Temp_Sensor_TC0_Coef";
                reg_11.Enabled = true;
                reg_11.Reset();
                //
                //
                reg_12.Address = "40000012";
                reg_12.RegisterName = "Temp_Sensor_TC1_Coef";
                reg_12.Enabled = true;
                reg_12.Reset();
                //
                //
                reg_13.Address = "40000013";
                reg_13.RegisterName = "";
                reg_13.Enabled = false;
                reg_13.Reset();
                //
                //
                reg_14.Address = "40000014";
                reg_14.RegisterName = "";
                reg_14.Enabled = false;
                reg_14.Reset();
                //
                //
                reg_15.Address = "40000015";
                reg_15.RegisterName = "";
                reg_15.Enabled = false;
                reg_15.Reset();
                //
                //
                reg_16.Address = "40000016";
                reg_16.RegisterName = "";
                reg_16.Enabled = false;
                reg_16.Reset();
                //
                //
                reg_17.Address = "40000017";
                reg_17.RegisterName = "";
                reg_17.Enabled = false;
                reg_17.Reset();
                //
                //
                reg_18.Address = "40000018";
                reg_18.RegisterName = "";
                reg_18.Enabled = false;
                reg_18.Reset();
                //
                //
                reg_19.Address = "40000019";
                reg_19.RegisterName = "";
                reg_19.Enabled = false;
                reg_19.Reset();
                //
                //
                reg_1A.Address = "4000001A";
                reg_1A.RegisterName = "";
                reg_1A.Enabled = false;
                reg_1A.Reset();
                //
                //
                reg_1B.Address = "4000001B";
                reg_1B.RegisterName = "";
                reg_1B.Enabled = false;
                reg_1B.Reset();
                //
                //
                reg_1C.Address = "4000001C";
                reg_1C.RegisterName = "";
                reg_1C.Enabled = false;
                reg_1C.Reset();
                //
                //
                reg_1D.Address = "4000001E";
                reg_1D.RegisterName = "Temp_Sensor_Mfg_Id";
                reg_1D.Enabled = true;
                reg_1D.Reset();
                //
                //
                reg_1E.Address = "4000001F";
                reg_1E.RegisterName = "Temp_Sensor_Device_Id";
                reg_1E.Enabled = true;
                reg_1E.Reset();
                //
                //
                reg_1F.Address = "4000002A";
                reg_1F.RegisterName = "Temp_Sensor_Mem_Access";
                reg_1F.Enabled = true;
                reg_1F.Reset();
                //
                //




            }

            if (tabRegisters.SelectedIndex == 3)
            {
                reg_00.Address = "60000000";
                reg_00.RegisterName = "PMIC_Clk_Ref_Trim";
                reg_00.Enabled = true;

                reg_00.Reset();
                //
                //
                reg_01.Address = "60000001";
                reg_01.RegisterName = "PMIC_Clk_RTC_Trim";
                reg_01.Enabled = true;
                reg_01.Reset();
                //
                //
                reg_02.Address = "60000002";
                reg_02.RegisterName = "PMIC_BLE_Port_Ctrl";
                reg_02.Enabled = true;
                reg_02.Reset();
                //
                //
                reg_03.Address = "60000003";
                reg_03.RegisterName = "PMIC_Clk_Ref_Off_Cntr";
                reg_03.Enabled = true;
                reg_03.Reset();
                //
                //
                reg_04.Address = "60000004";
                reg_04.RegisterName = "Global6_Status";
                reg_04.Enabled = true;
                reg_04.Reset();
                //
                //
                reg_05.Address = "60000005";
                reg_05.RegisterName = "Global6_Clock_Ctrl";
                reg_05.Enabled = true;
                reg_05.Reset();
                //
                //
                reg_06.Address = "60000006";
                reg_06.RegisterName = "Telemetry_Status";
                reg_06.Enabled = true;
                reg_06.Reset();
                //
                //
                reg_07.Address = "60000007";
                reg_07.RegisterName = "PMIC_Prog_Reset";
                reg_07.Enabled = true;
                reg_07.Reset();
                //
                //
                reg_08.Address = "60000008";
                reg_08.RegisterName = "PMIC_Reset_Status";
                reg_08.Enabled = true;
                reg_08.Reset();
                //
                //
                reg_09.Address = "60000009";
                reg_09.RegisterName = "PMIC_Charging_Control";
                reg_09.Enabled = true;
                reg_09.Reset();
                //
                //
                reg_0A.Address = "6000000A";
                reg_0A.RegisterName = "PMIC_Battery_Status";
                reg_0A.Enabled = true;
                reg_0A.Reset();
                //
                //
                reg_0B.Address = "6000000B";
                reg_0B.RegisterName = "PMIC_Charging_Status";
                reg_0B.Enabled = true;
                reg_0B.Reset();
                //
                //
                reg_0C.Address = "6000000C";
                reg_0C.RegisterName = "PMIC_Pwr_Chrg_State";
                reg_0C.Enabled = true;
                reg_0C.Reset();
                //
                //
                reg_0D.Address = "6000000D";
                reg_0D.RegisterName = "PMIC_Pwr_VIN_State";
                reg_0D.Enabled = true;
                reg_0D.Reset();
                //
                //
                reg_0E.Address = "6000000E";
                reg_0E.RegisterName = "PMIC_Pwr_Batt_State";
                reg_0E.Enabled = true;
                reg_0E.Reset();
                //
                //
                reg_0F.Address = "6000000F";
                reg_0F.RegisterName = "PMIC_ASIC_Revision";
                reg_0F.Enabled = true;
                reg_0F.Reset();
                //
                //
                reg_10.Address = "60000010";
                reg_10.RegisterName = "PMIC_VREG_Adj";
                reg_10.Enabled = true;
                reg_10.Reset();
                //
                //
                reg_11.Address = "60000011";
                reg_11.RegisterName = "PMIC_VBG_Adj";
                reg_11.Enabled = true;
                reg_11.Reset();
                //
                //
                reg_12.Address = "60000012";
                reg_12.RegisterName = "PMIC_Vin_UV_Adj";
                reg_12.Enabled = true;
                reg_12.Reset();
                //
                //
                reg_13.Address = "60000013";
                reg_13.RegisterName = "PMIC_Vin_OV_Adj";
                reg_13.Enabled = true;
                reg_13.Reset();
                //
                //
                reg_14.Address = "60000014";
                reg_14.RegisterName = "PMIC_Vin_Hi_Adj";
                reg_14.Enabled = true;
                reg_14.Reset();
                //
                //
                reg_15.Address = "60000015";
                reg_15.RegisterName = "PMIC_Vin_Reg_Adj";
                reg_15.Enabled = true;
                reg_15.Reset();
                //
                //
                reg_16.Address = "60000016";
                reg_16.RegisterName = "";
                reg_16.Enabled = true;
                reg_16.Reset();
                //
                //
                reg_17.Address = "60000017";
                reg_17.RegisterName = "";
                reg_17.Enabled = true;
                reg_17.Reset();
                //
                //
                reg_18.Address = "60000018";
                reg_18.RegisterName = "PMIC_VBAT_Trkl_Adj";
                reg_18.Enabled = true;
                reg_18.Reset();
                //
                //
                reg_19.Address = "60000019";
                reg_19.RegisterName = "PMIC_VBAT_OV_Adj";
                reg_19.Enabled = true;
                reg_19.Reset();
                //
                //
                reg_1A.Address = "6000001A";
                reg_1A.RegisterName = "PMIC_VBAT_Reg_Adj";
                reg_1A.Enabled = true;
                reg_1A.Reset();
                //
                //
                reg_1B.Address = "6000001B";
                reg_1B.RegisterName = "PMIC_VBAT_UV_Adj";
                reg_1B.Enabled = false;
                reg_1B.Reset();
                //
                //
                reg_1C.Address = "6000001C";
                reg_1C.RegisterName = "PMIC_VBAT_LO_Adj";
                reg_1C.Enabled = true;
                reg_1C.Reset();
                //
                //
                reg_1D.Address = "6000001D";
                reg_1D.RegisterName = "";
                reg_1D.Enabled = false;
                reg_1D.Reset();
                //
                //
                reg_1E.Address = "6000001E";
                reg_1E.RegisterName = "";
                reg_1E.Enabled = false;
                reg_1E.Reset();
                //
                //
                reg_1F.Address = "6000001F";
                reg_1F.RegisterName = "";
                reg_1F.Enabled = false;
                reg_1F.Reset();
                //
                //




            }

            if (tabRegisters.SelectedIndex == 4)
            {
                reg_00.Address = "60000020";
                reg_00.RegisterName = "PMIC_BCK1_Freq_Adj";
                reg_00.Enabled = true;
                reg_00.Reset();
                //
                //
                reg_01.Address = "60000021";
                reg_01.RegisterName = "PMIC_BCK1_Ref_Adj";
                reg_01.Enabled = true;
                reg_01.Reset();
                //
                //
                reg_02.Address = "60000022";
                reg_02.RegisterName = "PMIC_BCK1_Ctrl_Adj";
                reg_02.Enabled = true;
                reg_02.Reset();
                //
                //
                reg_03.Address = "60000023";
                reg_03.RegisterName = "PMIC_BCK1_Err_Amp_Adj";
                reg_03.Enabled = true;
                reg_03.Reset();
                //
                //
                reg_04.Address = "60000024";
                reg_04.RegisterName = "PMIC_BCK1_Boot_Adj";
                reg_04.Enabled = true;
                reg_04.Reset();
                //
                //
                reg_05.Address = "60000025";
                reg_05.RegisterName = "PMIC_BCK1_Time_Adj";
                reg_05.Enabled = true;
                reg_05.Reset();
                //
                //
                reg_06.Address = "60000026";
                reg_06.RegisterName = "PMIC_TSD_Adj_0";
                reg_06.Enabled = true;
                reg_06.Reset();
                //
                //
                reg_07.Address = "60000027";
                reg_07.RegisterName = "PMIC_TSD_Adj_1";
                reg_07.Enabled = true;
                reg_07.Reset();
                //
                //
                reg_08.Address = "60000028";
                reg_08.RegisterName = "PMIC_TSD_Adj_2";
                reg_08.Enabled = true;
                reg_08.Reset();
                //
                //
                reg_09.Address = "60000029";
                reg_09.RegisterName = "";
                reg_09.Enabled = false;
                reg_09.Reset();
                //
                //
                reg_0A.Address = "6000002A";
                reg_0A.RegisterName = "";
                reg_0A.Enabled = false;
                reg_0A.Reset();
                //
                //
                reg_0B.Address = "6000002B";
                reg_0B.RegisterName = "";
                reg_0B.Enabled = false;
                reg_0B.Reset();
                //
                //
                reg_0C.Address = "6000002C";
                reg_0C.RegisterName = "";
                reg_0C.Enabled = false;
                reg_0C.Reset();
                //
                //
                reg_0D.Address = "6000002D";
                reg_0D.RegisterName = "";
                reg_0D.Enabled = false;
                reg_0D.Reset();
                //
                //
                reg_0E.Address = "6000002E";
                reg_0E.RegisterName = "";
                reg_0E.Enabled = false;
                reg_0E.Reset();
                //
                //
                reg_0F.Address = "6000002F";
                reg_0F.RegisterName = "";
                reg_0F.Enabled = false;
                reg_0F.Reset();
                //
                //
                reg_10.Address = "60000030";
                reg_10.RegisterName = "PMIC_RTC_Ctrl";
                reg_10.Enabled = true;
                reg_10.Reset();
                //
                //
                reg_11.Address = "60000031";
                reg_11.RegisterName = "PMIC_RTC_Status";
                reg_11.Enabled = true;
                reg_11.Reset();
                //
                //
                reg_12.Address = "60000032";
                reg_12.RegisterName = "PMIC_RTC_Years";
                reg_12.Enabled = true;
                reg_12.Reset();
                //
                //
                reg_13.Address = "60000033";
                reg_13.RegisterName = "PMIC_RTC_Days";
                reg_13.Enabled = true;
                reg_13.Reset();
                //
                //
                reg_14.Address = "60000034";
                reg_14.RegisterName = "PMIC_RTC_Hours";
                reg_14.Enabled = true;
                reg_14.Reset();
                //
                //
                reg_15.Address = "60000035";
                reg_15.RegisterName = "PMIC_RTC_Minutes";
                reg_15.Enabled = true;
                reg_15.Reset();
                //
                //
                reg_16.Address = "60000036";
                reg_16.RegisterName = "PMIC_RTC_Seconds";
                reg_16.Enabled = true;
                reg_16.Reset();
                //
                //
                reg_17.Address = "60000037";
                reg_17.RegisterName = "";
                reg_17.Enabled = false;
                reg_17.Reset();
                //
                //
                reg_18.Address = "60000038";
                reg_18.RegisterName = "";
                reg_18.Enabled = false;
                reg_18.Reset();
                //
                //
                reg_19.Address = "60000039";
                reg_19.RegisterName = "";
                reg_19.Enabled = false;
                reg_19.Reset();
                //
                //
                reg_1A.Address = "6000003A";
                reg_1A.RegisterName = "";
                reg_1A.Enabled = false;
                reg_1A.Reset();
                //
                //
                reg_1B.Address = "6000003B";
                reg_1B.RegisterName = "";
                reg_1B.Enabled = false;
                reg_1B.Reset();
                //
                //
                reg_1C.Address = "6000003C";
                reg_1C.RegisterName = "";
                reg_1C.Enabled = false;
                reg_1C.Reset();
                //
                //
                reg_1D.Address = "6000003D";
                reg_1D.RegisterName = "";
                reg_1D.Enabled = false;
                reg_1D.Reset();
                //
                //
                reg_1E.Address = "6000003E";
                reg_1E.RegisterName = "";
                reg_1E.Enabled = false;
                reg_1E.Reset();
                //
                //
                reg_1F.Address = "6000003F";
                reg_1F.RegisterName = "";
                reg_1F.Enabled = false;
                reg_1F.Reset();
                //
                //




            }

            if (tabRegisters.SelectedIndex == 5)
            {
                reg_00.Address = "70000000";
                reg_00.RegisterName = "Serial_Comm7_Control";
                reg_00.Enabled = true;
                reg_00.Reset();
                //
                //
                reg_01.Address = "70000001";
                reg_01.RegisterName = "Serial_Comm7_Status";
                reg_01.Enabled = true;
                reg_01.Reset();
                //
                //
                reg_02.Address = "70000002";
                reg_02.RegisterName = "Serial_Comm7_Tx_Clock_Cntr";
                reg_02.Enabled = true;
                reg_02.Reset();
                //
                //
                reg_03.Address = "70000003";
                reg_03.RegisterName = "Serial_Comm7_Rx_Clock_Cycle_Time";
                reg_03.Enabled = true;
                reg_03.Reset();
                //
                //
                reg_04.Address = "70000004";
                reg_04.RegisterName = "Serial_Comm7_Bus_Opn_Timer";
                reg_04.Enabled = true;
                reg_04.Reset();
                //
                //
                reg_05.Address = "70000005";
                reg_05.RegisterName = "Serial_Comm7_Stim_Num_Seqs (N)";
                reg_05.Enabled = true;
                reg_05.Reset();
                //
                //
                reg_06.Address = "70000006";
                reg_06.RegisterName = "Serial_Comm7_Slew_Rate";
                reg_06.Enabled = true;
                reg_06.Reset();
                //
                //
                reg_07.Address = "70000007";
                reg_07.RegisterName = "Serial_Comm7_Term_Adj";
                reg_07.Enabled = true;
                reg_07.Reset();
                //
                //
                reg_08.Address = "70000008";
                reg_08.RegisterName = "Serial_Comm7_Prechrg_Timer";
                reg_08.Enabled = true;
                reg_08.Reset();
                //
                //
                reg_09.Address = "70000009";
                reg_09.RegisterName = "";
                reg_09.Enabled = false;
                reg_09.Reset();
                //
                //
                reg_0A.Address = "7000000A";
                reg_0A.RegisterName = "";
                reg_0A.Enabled = false;
                reg_0A.Reset();
                //
                //
                reg_0B.Address = "7000000B";
                reg_0B.RegisterName = "";
                reg_0B.Enabled = true;
                reg_0B.Reset();
                //
                //
                reg_0C.Address = "7000000C";
                reg_0C.RegisterName = "";
                reg_0C.Enabled = true;
                reg_0C.Reset();
                //
                //
                reg_0D.Address = "7000000D";
                reg_0D.RegisterName = "";
                reg_0D.Enabled = true;
                reg_0D.Reset();
                //
                //
                reg_0E.Address = "7000000E";
                reg_0E.RegisterName = "";
                reg_0E.Enabled = true;
                reg_0E.Reset();
                //
                //
                reg_0F.Address = "7000000F";
                reg_0F.RegisterName = "";
                reg_0F.Enabled = true;
                reg_0F.Reset();
                //
                //
                reg_10.Address = "70000010";
                reg_10.RegisterName = "Serial_Comm7_Pwr_Control";
                reg_10.Enabled = true;
                reg_10.Reset();
                //
                //
                reg_11.Address = "70000011";
                reg_11.RegisterName = "Serial_Comm7_Pwr_Status";
                reg_11.Enabled = true;
                reg_11.Reset();
                //
                //
                reg_12.Address = "70000012";
                reg_12.RegisterName = "Serial_Comm7_Pwr_Tx_Clock_Cntr";
                reg_12.Enabled = true;
                reg_12.Reset();
                //
                //
                reg_13.Address = "70000013";
                reg_13.RegisterName = "PMIC_Vin_OV_Adj";
                reg_13.Enabled = true;
                reg_13.Reset();
                //
                //
                reg_14.Address = "70000014";
                reg_14.RegisterName = "PMIC_Vin_Hi_Adj";
                reg_14.Enabled = true;
                reg_14.Reset();
                //
                //
                reg_15.Address = "70000015";
                reg_15.RegisterName = "PMIC_Vin_Reg_Adj";
                reg_15.Enabled = true;
                reg_15.Reset();
                //
                //
                reg_16.Address = "70000016";
                reg_16.RegisterName = "";
                reg_16.Enabled = true;
                reg_16.Reset();
                //
                //
                reg_17.Address = "70000017";
                reg_17.RegisterName = "";
                reg_17.Enabled = true;
                reg_17.Reset();
                //
                //
                reg_18.Address = "70000018";
                reg_18.RegisterName = "PMIC_VBAT_Trkl_Adj";
                reg_18.Enabled = true;
                reg_18.Reset();
                //
                //
                reg_19.Address = "70000019";
                reg_19.RegisterName = "PMIC_VBAT_OV_Adj";
                reg_19.Enabled = true;
                reg_19.Reset();
                //
                //
                reg_1A.Address = "7000001A";
                reg_1A.RegisterName = "PMIC_VBAT_Reg_Adj";
                reg_1A.Enabled = true;
                reg_1A.Reset();
                //
                //
                reg_1B.Address = "7000001B";
                reg_1B.RegisterName = "PMIC_VBAT_UV_Adj";
                reg_1B.Enabled = false;
                reg_1B.Reset();
                //
                //
                reg_1C.Address = "7000001C";
                reg_1C.RegisterName = "PMIC_VBAT_LO_Adj";
                reg_1C.Enabled = true;
                reg_1C.Reset();
                //
                //
                reg_1D.Address = "7000001D";
                reg_1D.RegisterName = "";
                reg_1D.Enabled = false;
                reg_1D.Reset();
                //
                //
                reg_1E.Address = "7000001E";
                reg_1E.RegisterName = "";
                reg_1E.Enabled = false;
                reg_1E.Reset();
                //
                //
                reg_1F.Address = "7000001F";
                reg_1F.RegisterName = "";
                reg_1F.Enabled = false;
                reg_1F.Reset();
                //
                //




            }

        }


        private void tabStimRegistersUpdate(Object sender, EventArgs e)
        {

            if (tabStimRegisters.SelectedIndex == 0)
            {
                regstim_00.Address = "80000000";
                regstim_00.RegisterName = "ADC Next Delay";
                regstim_00.Enabled = true;
                regstim_00.Reset();
                //
                //
                regstim_01.Address = "80000001";
                regstim_01.RegisterName = "Stim Num Seqs (N)";
                regstim_01.Enabled = true;
                regstim_01.Reset();
                //
                //
                regstim_02.Address = "80000002";
                regstim_02.RegisterName = "ECAP Reset Off Time (µsec)";
                regstim_02.Enabled = true;
                regstim_02.Reset();
                //
                //
                regstim_03.Address = "80000003";
                regstim_03.RegisterName = "ECAP Reset Off Time (µsec)";
                regstim_03.Enabled = true;
                regstim_03.Reset();
                //
                //
                regstim_04.Address = "80000004";
                regstim_04.RegisterName = "Stim Mask Pulse Width (µsec)";
                regstim_04.Enabled = true;
                regstim_04.Reset();
                //
                //
                regstim_05.Address = "80000005";
                regstim_05.RegisterName = "Stim Mask Step Width (µsec)";
                regstim_05.Enabled = true;
                regstim_05.Reset();
                //
                //
                regstim_06.Address = "80000006";
                regstim_06.RegisterName = "Stim Mask Next Delay (µsec)";
                regstim_06.Enabled = true;
                regstim_06.Reset();
                //
                //
                regstim_07.Address = "80000007";
                regstim_07.RegisterName = "Programmable_Reset";
                regstim_07.Enabled = true;
                regstim_07.Reset();
                //
                //
                regstim_08.Address = "80000008";
                regstim_08.RegisterName = "Stim Pulse Width (µsec)";
                regstim_08.Enabled = true;
                regstim_08.Reset();
                //
                //
                regstim_09.Address = "80000009";
                regstim_09.RegisterName = "Stim Step Width (µsec)";
                regstim_09.Enabled = true;
                regstim_09.Reset();
                //
                //
                regstim_0A.Address = "8000000A";
                regstim_0A.RegisterName = "Stim Next Delay (µsec)";
                regstim_0A.Enabled = true;
                regstim_0A.Reset();
                //
                //
                regstim_0B.Address = "8000000B";
                regstim_0B.RegisterName = "Stim Event Repeat Count (R)";
                regstim_0B.Enabled = true;
                regstim_0B.Reset();
                //
                //
                regstim_0C.Address = "8000000C";
                regstim_0C.RegisterName = "IDAC Setup Time (µsec)";
                regstim_0C.Enabled = true;
                regstim_0C.Reset();
                //
                //
                regstim_0D.Address = "8000000D";
                regstim_0D.RegisterName = "Stim Control";
                regstim_0D.Enabled = true;
                regstim_0D.Reset();
                //
                //
                regstim_0E.Address = "8000000E";
                regstim_0E.RegisterName = "Stim Power Control";
                regstim_0E.Enabled = true;
                regstim_0E.Reset();
                //
                //
                regstim_0F.Address = "8000000F";
                regstim_0F.RegisterName = "ECAP Control";
                regstim_0F.Enabled = false;
                regstim_0F.Reset();
                //
                //
                regstim_10.Address = "80000010";
                regstim_10.RegisterName = "Battery Thresholds";
                regstim_10.Enabled = true;
                regstim_10.Reset();
                //
                //
                regstim_11.Address = "80000011";
                regstim_11.RegisterName = "Battery Status";
                regstim_11.Enabled = true;
                regstim_11.Reset();
                //
                //
                regstim_12.Address = "80000012";
                regstim_12.RegisterName = "EEPROM Test Control";
                regstim_12.Enabled = true;
                regstim_12.Reset();
                //
                //
                regstim_13.Address = "80000013";
                regstim_13.RegisterName = "EEPROM Status";
                regstim_13.Enabled = true;
                regstim_13.Reset();
                //
                //
                regstim_14.Address = "80000014";
                regstim_14.RegisterName = "";
                regstim_14.Enabled = false;
                regstim_14.Reset();
                //
                //
                regstim_15.Address = "80000015";
                regstim_15.RegisterName = "";
                regstim_15.Enabled = false;
                regstim_15.Reset();
                //
                //
                regstim_16.Address = "80000016";
                regstim_16.RegisterName = "";
                regstim_16.Enabled = false;
                regstim_16.Reset();
                //
                //
                regstim_17.Address = "80000017";
                regstim_17.RegisterName = "";
                regstim_17.Enabled = false;
                regstim_17.Reset();
                //
                //
                regstim_18.Address = "80000018";
                regstim_18.RegisterName = "Stim Vref Calibration Ctrl";
                regstim_18.Enabled = true;
                regstim_18.Reset();
                //
                //
                regstim_19.Address = "80000019";
                regstim_19.RegisterName = "Stim 4 MHz Osc Clock Trim";
                regstim_19.Enabled = true;
                regstim_19.Reset();
                //
                //
                regstim_1A.Address = "8000001A";
                regstim_1A.RegisterName = "Clock_Cal_Ctrl";
                regstim_1A.Enabled = true;
                regstim_1A.Reset();
                //
                //
                regstim_1B.Address = "8000001B";
                regstim_1B.RegisterName = "ADC8 Scale";
                regstim_1B.Enabled = true;
                regstim_1B.Reset();
                //
                //
                regstim_1C.Address = "8000001C";
                regstim_1C.RegisterName = "ADC8 Control";
                regstim_1C.Enabled = true;
                regstim_1C.Reset();
                //
                //
                regstim_1D.Address = "8000001D";
                regstim_1D.RegisterName = "ADC Results";
                regstim_1D.Enabled = true;
                regstim_1D.Reset();
                //
                //
                regstim_1E.Address = "8000001E";
                regstim_1E.RegisterName = "Sense Mux Control P";
                regstim_1E.Enabled = true;
                regstim_1E.Reset();
                //
                //
                regstim_1F.Address = "8000001F";
                regstim_1F.RegisterName = "Sense Mux Control N";
                regstim_1F.Enabled = true;
                regstim_1F.Reset();
                //
                //




            }

            if (tabStimRegisters.SelectedIndex == 1)
            {
                regstim_00.Address = "80000020";
                regstim_00.RegisterName = "Amux Test Reg 1";
                regstim_00.Enabled = true;
                regstim_00.Reset();
                //
                //
                regstim_01.Address = "80000021";
                regstim_01.RegisterName = "Amux Test Reg 2";
                regstim_01.Enabled = true;
                regstim_01.Reset();
                //
                //
                regstim_02.Address = "80000022";
                regstim_02.RegisterName = "Amux Test Reg 3";
                regstim_02.Enabled = true;
                regstim_02.Reset();
                //
                //
                regstim_03.Address = "80000023";
                regstim_03.RegisterName = "";
                regstim_03.Enabled = false;
                regstim_03.Reset();
                //
                //
                regstim_04.Address = "80000024";
                regstim_04.RegisterName = "Global Clock Control";
                regstim_04.Enabled = true;
                regstim_04.Reset();
                //
                //
                regstim_05.Address = "80000025";
                regstim_05.RegisterName = "";
                regstim_05.Enabled = false;
                regstim_05.Reset();
                //
                //
                regstim_06.Address = "80000026";
                regstim_06.RegisterName = "";
                regstim_06.Enabled = false;
                regstim_06.Reset();
                //
                //
                regstim_07.Address = "80000027";
                regstim_07.RegisterName = "Stim ASIC Revision";
                regstim_07.Enabled = true;
                regstim_07.Reset();
                //
                //
                regstim_08.Address = "80000028";
                regstim_08.RegisterName = "";
                regstim_08.Enabled = false;
                regstim_08.Reset();
                //
                //
                regstim_09.Address = "80000029";
                regstim_09.RegisterName = "";
                regstim_09.Enabled = false;
                regstim_09.Reset();
                //
                //
                regstim_0A.Address = "8000002A";
                regstim_0A.RegisterName = "";
                regstim_0A.Enabled = false;
                regstim_0A.Reset();
                //
                //
                regstim_0B.Address = "8000002B";
                regstim_0B.RegisterName = "";
                regstim_0B.Enabled = false;
                regstim_0B.Reset();
                //
                //
                regstim_0C.Address = "8000002C";
                regstim_0C.RegisterName = "";
                regstim_0C.Enabled = false;
                regstim_0C.Reset();
                //
                //
                regstim_0D.Address = "8000002D";
                regstim_0D.RegisterName = "";
                regstim_0D.Enabled = false;
                regstim_0D.Reset();
                //
                //
                regstim_0E.Address = "8000002E";
                regstim_0E.RegisterName = "";
                regstim_0E.Enabled = false;
                regstim_0E.Reset();
                //
                //
                regstim_0F.Address = "8000002F";
                regstim_0F.RegisterName = "";
                regstim_0F.Enabled = false;
                regstim_0F.Reset();
                //
                //
                regstim_10.Address = "30000030";
                regstim_10.RegisterName = "Stim_VREG_Adj";
                regstim_10.Enabled = true;
                regstim_10.Reset();
                //
                //
                regstim_11.Address = "30000031";
                regstim_11.RegisterName = "Stim_VBAT_UV_Adj";
                regstim_11.Enabled = true;
                regstim_11.Reset();
                //
                //
                regstim_12.Address = "30000032";
                regstim_12.RegisterName = "Stim_VBAT_LO_Adj";
                regstim_12.Enabled = true;
                regstim_12.Reset();
                //
                //
                regstim_13.Address = "30000033";
                regstim_13.RegisterName = "Stim Power Status";
                regstim_13.Enabled = true;
                regstim_13.Reset();
                //
                //
                regstim_14.Address = "30000034";
                regstim_14.RegisterName = "VStim_LO_Adj";
                regstim_14.Enabled = true;
                regstim_14.Reset();
                //
                //
                regstim_15.Address = "30000035";
                regstim_15.RegisterName = "Vstim_Hi_Adj";
                regstim_15.Enabled = true;
                regstim_15.Reset();
                //
                //
                regstim_16.Address = "30000036";
                regstim_16.RegisterName = "Vstim_VMID_OV_Adj";
                regstim_16.Enabled = true;
                regstim_16.Reset();
                //
                //
                regstim_17.Address = "30000037";
                regstim_17.RegisterName = "Stim_ICPL_Adj";
                regstim_17.Enabled = true;
                regstim_17.Reset();
                //
                //
                regstim_18.Address = "30000038";
                regstim_18.RegisterName = "Stim_ICPL_Adj";
                regstim_18.Enabled = true;
                regstim_18.Reset();
                //
                //
                regstim_19.Address = "30000039";
                regstim_19.RegisterName = "";
                regstim_19.Enabled = false;
                regstim_19.Reset();
                //
                //
                regstim_1A.Address = "3000003A";
                regstim_1A.RegisterName = "";
                regstim_1A.Enabled = false;
                regstim_1A.Reset();
                //
                //
                regstim_1B.Address = "3000003B";
                regstim_1B.RegisterName = "";
                regstim_1B.Enabled = false;
                regstim_1B.Reset();
                //
                //
                regstim_1C.Address = "3000003C";
                regstim_1C.RegisterName = "";
                regstim_1C.Enabled = false;
                regstim_1C.Reset();
                //
                //
                regstim_1D.Address = "3000003D";
                regstim_1D.RegisterName = "";
                regstim_1D.Enabled = false;
                regstim_1D.Reset();
                //
                //
                regstim_1E.Address = "3000003E";
                regstim_1E.RegisterName = "";
                regstim_1E.Enabled = false;
                regstim_1E.Reset();
                //
                //
                regstim_1F.Address = "3000003F";
                regstim_1F.RegisterName = "";
                regstim_1F.Enabled = false;
                regstim_1F.Reset();
                //
                //




            }

            if (tabStimRegisters.SelectedIndex == 2)
            {
                regstim_00.Address = "80000040";
                regstim_00.RegisterName = "Stim_DAC_0_Adj";
                regstim_00.Enabled = true;
                regstim_00.Reset();
                //
                //
                regstim_01.Address = "80000041";
                regstim_01.RegisterName = "Stim_DAC_1_Adj";
                regstim_01.Enabled = true;
                regstim_01.Reset();
                //
                //
                regstim_02.Address = "80000042";
                regstim_02.RegisterName = "Stim_DAC_2_Adj";
                regstim_02.Enabled = true;
                regstim_02.Reset();
                //
                //
                regstim_03.Address = "80000043";
                regstim_03.RegisterName = "Stim_DAC_3_Adj";
                regstim_03.Enabled = true;
                regstim_03.Reset();
                //
                //
                regstim_04.Address = "80000044";
                regstim_04.RegisterName = "Stim_DAC_4_Adj";
                regstim_04.Enabled = true;
                regstim_04.Reset();
                //
                //
                regstim_05.Address = "80000045";
                regstim_05.RegisterName = "Stim_DAC_5_Adj";
                regstim_05.Enabled = true;
                regstim_05.Reset();
                //
                //
                regstim_06.Address = "80000046";
                regstim_06.RegisterName = "Stim_DAC_6_Adj";
                regstim_06.Enabled = true;
                regstim_06.Reset();
                //
                //
                regstim_07.Address = "80000047";
                regstim_07.RegisterName = "Stim_DAC_7_Adj";
                regstim_07.Enabled = true;
                regstim_07.Reset();
                //
                //
                regstim_08.Address = "80000048";
                regstim_08.RegisterName = "Stim_DAC_8_Adj";
                regstim_08.Enabled = true;
                regstim_08.Reset();
                //
                //
                regstim_09.Address = "80000049";
                regstim_09.RegisterName = "Stim_DAC_9_Adj";
                regstim_09.Enabled = true;
                regstim_09.Reset();
                //
                //
                regstim_0A.Address = "8000004A";
                regstim_0A.RegisterName = "Stim_DAC_10_Adj";
                regstim_0A.Enabled = true;
                regstim_0A.Reset();
                //
                //
                regstim_0B.Address = "8000004B";
                regstim_0B.RegisterName = "Stim_DAC_11_Adj";
                regstim_0B.Enabled = true;
                regstim_0B.Reset();
                //
                //
                regstim_0C.Address = "8000004C";
                regstim_0C.RegisterName = "Stim_DAC_12_Adj";
                regstim_0C.Enabled = true;
                regstim_0C.Reset();
                //
                //
                regstim_0D.Address = "8000004D";
                regstim_0D.RegisterName = "Stim_DAC_13_Adj";
                regstim_0D.Enabled = true;
                regstim_0D.Reset();
                //
                //
                regstim_0E.Address = "8000004E";
                regstim_0E.RegisterName = "Stim_DAC_14_Adj";
                regstim_0E.Enabled = true;
                regstim_0E.Reset();
                //
                //
                regstim_0F.Address = "8000004F";
                regstim_0F.RegisterName = "Stim_DAC_15_Adj";
                regstim_0F.Enabled = true;
                regstim_0F.Reset();
                //
                //
                regstim_10.Address = "80000050";
                regstim_10.RegisterName = "Stim_DAC_16_Adj";
                regstim_10.Enabled = true;
                regstim_10.Reset();
                //
                //
                regstim_11.Address = "80000051";
                regstim_11.RegisterName = "Stim_DAC_17_Adj";
                regstim_11.Enabled = true;
                regstim_11.Reset();
                //
                //
                regstim_12.Address = "80000052";
                regstim_12.RegisterName = "";
                regstim_12.Enabled = false;
                regstim_12.Reset();
                //
                //
                regstim_13.Address = "80000053";
                regstim_13.RegisterName = "";
                regstim_13.Enabled = false;
                regstim_13.Reset();
                //
                //
                regstim_14.Address = "80000054";
                regstim_14.RegisterName = "";
                regstim_14.Enabled = false;
                regstim_14.Reset();
                //
                //
                regstim_15.Address = "80000055";
                regstim_15.RegisterName = "";
                regstim_15.Enabled = false;
                regstim_15.Reset();
                //
                //
                regstim_16.Address = "80000056";
                regstim_16.RegisterName = "";
                regstim_16.Enabled = false;
                regstim_16.Reset();
                //
                //
                regstim_17.Address = "80000057";
                regstim_17.RegisterName = "";
                regstim_17.Enabled = false;
                regstim_17.Reset();
                //
                //
                regstim_18.Address = "80000058";
                regstim_18.RegisterName = "";
                regstim_18.Enabled = false;
                regstim_18.Reset();
                //
                //
                regstim_19.Address = "80000059";
                regstim_19.RegisterName = "";
                regstim_19.Enabled = false;
                regstim_19.Reset();
                //
                //
                regstim_1A.Address = "8000005A";
                regstim_1A.RegisterName = "";
                regstim_1A.Enabled = false;
                regstim_1A.Reset();
                //
                //
                regstim_1B.Address = "8000005B";
                regstim_1B.RegisterName = "";
                regstim_1B.Enabled = false;
                regstim_1B.Reset();
                //
                //
                regstim_1C.Address = "8000005C";
                regstim_1C.RegisterName = "";
                regstim_1C.Enabled = false;
                regstim_1C.Reset();
                //
                //
                regstim_1D.Address = "8000005D";
                regstim_1D.RegisterName = "";
                regstim_1D.Enabled = false;
                regstim_1D.Reset();
                //
                //
                regstim_1E.Address = "8000005E";
                regstim_1E.RegisterName = "";
                regstim_1E.Enabled = false;
                regstim_1E.Reset();
                //
                //
                regstim_1F.Address = "8000005F";
                regstim_1F.RegisterName = "";
                regstim_1F.Enabled = false;
                regstim_1F.Reset();
                //
                //




            }

            if (tabStimRegisters.SelectedIndex == 3)
            {
                regstim_00.Address = "90000000";
                regstim_00.RegisterName = "Electrode_Active_En_00";
                regstim_00.Enabled = true;

                regstim_00.Reset();
                //
                //
                regstim_01.Address = "90000001";
                regstim_01.RegisterName = "Electrode_Active_En_01";
                regstim_01.Enabled = true;
                regstim_01.Reset();
                //
                //
                regstim_02.Address = "90000002";
                regstim_02.RegisterName = "Electrode_Active_En_02";
                regstim_02.Enabled = true;
                regstim_02.Reset();
                //
                //
                regstim_03.Address = "90000003";
                regstim_03.RegisterName = "Electrode_Active_En_03";
                regstim_03.Enabled = true;
                regstim_03.Reset();
                //
                //
                regstim_04.Address = "90000004";
                regstim_04.RegisterName = "Electrode_Active_En_04";
                regstim_04.Enabled = true;
                regstim_04.Reset();
                //
                //
                regstim_05.Address = "90000005";
                regstim_05.RegisterName = "Electrode_Active_En_05";
                regstim_05.Enabled = true;
                regstim_05.Reset();
                //
                //
                regstim_06.Address = "90000006";
                regstim_06.RegisterName = "Electrode_Active_En_06";
                regstim_06.Enabled = true;
                regstim_06.Reset();
                //
                //
                regstim_07.Address = "90000007";
                regstim_07.RegisterName = "Electrode_Active_En_07";
                regstim_07.Enabled = true;
                regstim_07.Reset();
                //
                //
                regstim_08.Address = "90000008";
                regstim_08.RegisterName = "Electrode_Active_En_08";
                regstim_08.Enabled = true;
                regstim_08.Reset();
                //
                //
                regstim_09.Address = "90000009";
                regstim_09.RegisterName = "Electrode_Active_En_09";
                regstim_09.Enabled = true;
                regstim_09.Reset();
                //
                //
                regstim_0A.Address = "9000000A";
                regstim_0A.RegisterName = "Electrode_Active_En_10";
                regstim_0A.Enabled = true;
                regstim_0A.Reset();
                //
                //
                regstim_0B.Address = "9000000B";
                regstim_0B.RegisterName = "Electrode_Active_En_11";
                regstim_0B.Enabled = true;
                regstim_0B.Reset();
                //
                //
                regstim_0C.Address = "9000000C";
                regstim_0C.RegisterName = "Electrode_Active_En_12";
                regstim_0C.Enabled = true;
                regstim_0C.Reset();
                //
                //
                regstim_0D.Address = "9000000D";
                regstim_0D.RegisterName = "Electrode_Active_En_13";
                regstim_0D.Enabled = true;
                regstim_0D.Reset();
                //
                //
                regstim_0E.Address = "9000000E";
                regstim_0E.RegisterName = "Electrode_Active_En_14";
                regstim_0E.Enabled = true;
                regstim_0E.Reset();
                //
                //
                regstim_0F.Address = "9000000F";
                regstim_0F.RegisterName = "Electrode_Active_En_15";
                regstim_0F.Enabled = true;
                regstim_0F.Reset();
                //
                //
                regstim_10.Address = "90000010";
                regstim_10.RegisterName = "Electrode_Active_En_16";
                regstim_10.Enabled = true;
                regstim_10.Reset();
                //
                //
                regstim_11.Address = "90000011";
                regstim_11.RegisterName = "Electrode_Active_En_17";
                regstim_11.Enabled = true;
                regstim_11.Reset();
                //
                //
                regstim_12.Address = "90000012";
                regstim_12.RegisterName = "Electrode_Active_En_18";
                regstim_12.Enabled = true;
                regstim_12.Reset();
                //
                //
                regstim_13.Address = "90000013";
                regstim_13.RegisterName = "Electrode_Active_En_19";
                regstim_13.Enabled = true;
                regstim_13.Reset();
                //
                //
                regstim_14.Address = "90000014";
                regstim_14.RegisterName = "Electrode_Active_En_20";
                regstim_14.Enabled = true;
                regstim_14.Reset();
                //
                //
                regstim_15.Address = "90000015";
                regstim_15.RegisterName = "Electrode_Active_En_21";
                regstim_15.Enabled = true;
                regstim_15.Reset();
                //
                //
                regstim_16.Address = "90000016";
                regstim_16.RegisterName = "Electrode_Active_En_22";
                regstim_16.Enabled = true;
                regstim_16.Reset();
                //
                //
                regstim_17.Address = "90000017";
                regstim_17.RegisterName = "Electrode_Active_En_23";
                regstim_17.Enabled = true;
                regstim_17.Reset();
                //
                //
                regstim_18.Address = "90000018";
                regstim_18.RegisterName = "Electrode_Active_En_24";
                regstim_18.Enabled = true;
                regstim_18.Reset();
                //
                //
                regstim_19.Address = "90000019";
                regstim_19.RegisterName = "Electrode_Active_En_25";
                regstim_19.Enabled = true;
                regstim_19.Reset();
                //
                //
                regstim_1A.Address = "9000001A";
                regstim_1A.RegisterName = "Electrode_Active_En_26";
                regstim_1A.Enabled = true;
                regstim_1A.Reset();
                //
                //
                regstim_1B.Address = "9000001B";
                regstim_1B.RegisterName = "Electrode_Active_En_27";
                regstim_1B.Enabled = true;
                regstim_1B.Reset();
                //
                //
                regstim_1C.Address = "9000001C";
                regstim_1C.RegisterName = "Electrode_Active_En_28";
                regstim_1C.Enabled = true;
                regstim_1C.Reset();
                //
                //
                regstim_1D.Address = "9000001D";
                regstim_1D.RegisterName = "Electrode_Active_En_29";
                regstim_1D.Enabled = true;
                regstim_1D.Reset();
                //
                //
                regstim_1E.Address = "9000001E";
                regstim_1E.RegisterName = "Electrode_Active_En_30";
                regstim_1E.Enabled = true;
                regstim_1E.Reset();
                //
                //
                regstim_1F.Address = "9000001F";
                regstim_1F.RegisterName = "Electrode_Active_En_31";
                regstim_1F.Enabled = true;
                regstim_1F.Reset();
                //
                //




            }

            if (tabStimRegisters.SelectedIndex == 4)
            {
                regstim_00.Address = "90000100";
                regstim_00.RegisterName = "Electrode_Return_En_00";
                regstim_00.Enabled = true;

                regstim_00.Reset();
                //
                //
                regstim_01.Address = "90000101";
                regstim_01.RegisterName = "Electrode_Return_En_01";
                regstim_01.Enabled = true;
                regstim_01.Reset();
                //
                //
                regstim_02.Address = "90000102";
                regstim_02.RegisterName = "Electrode_Return_En_02";
                regstim_02.Enabled = true;
                regstim_02.Reset();
                //
                //
                regstim_03.Address = "90000103";
                regstim_03.RegisterName = "Electrode_Return_En_03";
                regstim_03.Enabled = true;
                regstim_03.Reset();
                //
                //
                regstim_04.Address = "90000104";
                regstim_04.RegisterName = "Electrode_Return_En_04";
                regstim_04.Enabled = true;
                regstim_04.Reset();
                //
                //
                regstim_05.Address = "90000105";
                regstim_05.RegisterName = "Electrode_Return_En_05";
                regstim_05.Enabled = true;
                regstim_05.Reset();
                //
                //
                regstim_06.Address = "90000106";
                regstim_06.RegisterName = "Electrode_Return_En_06";
                regstim_06.Enabled = true;
                regstim_06.Reset();
                //
                //
                regstim_07.Address = "90000107";
                regstim_07.RegisterName = "Electrode_Return_En_07";
                regstim_07.Enabled = true;
                regstim_07.Reset();
                //
                //
                regstim_08.Address = "90000108";
                regstim_08.RegisterName = "Electrode_Return_En_08";
                regstim_08.Enabled = true;
                regstim_08.Reset();
                //
                //
                regstim_09.Address = "90000109";
                regstim_09.RegisterName = "Electrode_Return_En_09";
                regstim_09.Enabled = true;
                regstim_09.Reset();
                //
                //
                regstim_0A.Address = "9000010A";
                regstim_0A.RegisterName = "Electrode_Return_En_10";
                regstim_0A.Enabled = true;
                regstim_0A.Reset();
                //
                //
                regstim_0B.Address = "9000010B";
                regstim_0B.RegisterName = "Electrode_Return_En_11";
                regstim_0B.Enabled = true;
                regstim_0B.Reset();
                //
                //
                regstim_0C.Address = "9000010C";
                regstim_0C.RegisterName = "Electrode_Return_En_12";
                regstim_0C.Enabled = true;
                regstim_0C.Reset();
                //
                //
                regstim_0D.Address = "9000010D";
                regstim_0D.RegisterName = "Electrode_Return_En_13";
                regstim_0D.Enabled = true;
                regstim_0D.Reset();
                //
                //
                regstim_0E.Address = "9000010E";
                regstim_0E.RegisterName = "Electrode_Return_En_14";
                regstim_0E.Enabled = true;
                regstim_0E.Reset();
                //
                //
                regstim_0F.Address = "9000010F";
                regstim_0F.RegisterName = "Electrode_Return_En_15";
                regstim_0F.Enabled = true;
                regstim_0F.Reset();
                //
                //
                regstim_10.Address = "90000110";
                regstim_10.RegisterName = "Electrode_Return_En_16";
                regstim_10.Enabled = true;
                regstim_10.Reset();
                //
                //
                regstim_11.Address = "90000111";
                regstim_11.RegisterName = "Electrode_Return_En_17";
                regstim_11.Enabled = true;
                regstim_11.Reset();
                //
                //
                regstim_12.Address = "90000112";
                regstim_12.RegisterName = "Electrode_Return_En_18";
                regstim_12.Enabled = true;
                regstim_12.Reset();
                //
                //
                regstim_13.Address = "90000113";
                regstim_13.RegisterName = "Electrode_Return_En_19";
                regstim_13.Enabled = true;
                regstim_13.Reset();
                //
                //
                regstim_14.Address = "90000114";
                regstim_14.RegisterName = "Electrode_Return_En_20";
                regstim_14.Enabled = true;
                regstim_14.Reset();
                //
                //
                regstim_15.Address = "90000115";
                regstim_15.RegisterName = "Electrode_Return_En_21";
                regstim_15.Enabled = true;
                regstim_15.Reset();
                //
                //
                regstim_16.Address = "90000116";
                regstim_16.RegisterName = "Electrode_Return_En_22";
                regstim_16.Enabled = true;
                regstim_16.Reset();
                //
                //
                regstim_17.Address = "90000117";
                regstim_17.RegisterName = "Electrode_Return_En_23";
                regstim_17.Enabled = true;
                regstim_17.Reset();
                //
                //
                regstim_18.Address = "90000118";
                regstim_18.RegisterName = "Electrode_Return_En_24";
                regstim_18.Enabled = true;
                regstim_18.Reset();
                //
                //
                regstim_19.Address = "90000119";
                regstim_19.RegisterName = "Electrode_Return_En_25";
                regstim_19.Enabled = true;
                regstim_19.Reset();
                //
                //
                regstim_1A.Address = "9000011A";
                regstim_1A.RegisterName = "Electrode_Return_En_26";
                regstim_1A.Enabled = true;
                regstim_1A.Reset();
                //
                //
                regstim_1B.Address = "9000011B";
                regstim_1B.RegisterName = "Electrode_Return_En_27";
                regstim_1B.Enabled = true;
                regstim_1B.Reset();
                //
                //
                regstim_1C.Address = "9000011C";
                regstim_1C.RegisterName = "Electrode_Return_En_28";
                regstim_1C.Enabled = true;
                regstim_1C.Reset();
                //
                //
                regstim_1D.Address = "9000011D";
                regstim_1D.RegisterName = "Electrode_Return_En_29";
                regstim_1D.Enabled = true;
                regstim_1D.Reset();
                //
                //
                regstim_1E.Address = "9000011E";
                regstim_1E.RegisterName = "Electrode_Return_En_30";
                regstim_1E.Enabled = true;
                regstim_1E.Reset();
                //
                //
                regstim_1F.Address = "9000011F";
                regstim_1F.RegisterName = "Electrode_Return_En_31";
                regstim_1F.Enabled = true;
                regstim_1F.Reset();
                //
                //




            }

            if (tabStimRegisters.SelectedIndex == 5)
            {
                regstim_00.Address = "90000200";
                regstim_00.RegisterName = "Electrode Channel Assignment 00";
                regstim_00.Enabled = true;

                regstim_00.Reset();
                //
                //
                regstim_01.Address = "90000201";
                regstim_01.RegisterName = "Electrode Channel Assignment 01";
                regstim_01.Enabled = true;
                regstim_01.Reset();
                //
                //
                regstim_02.Address = "90000202";
                regstim_02.RegisterName = "Electrode Channel Assignment 02";
                regstim_02.Enabled = true;
                regstim_02.Reset();
                //
                //
                regstim_03.Address = "90000203";
                regstim_03.RegisterName = "Electrode Channel Assignment 03";
                regstim_03.Enabled = true;
                regstim_03.Reset();
                //
                //
                regstim_04.Address = "90000204";
                regstim_04.RegisterName = "Electrode Channel Assignment 04";
                regstim_04.Enabled = true;
                regstim_04.Reset();
                //
                //
                regstim_05.Address = "90000205";
                regstim_05.RegisterName = "Electrode Channel Assignment 05";
                regstim_05.Enabled = true;
                regstim_05.Reset();
                //
                //
                regstim_06.Address = "90000206";
                regstim_06.RegisterName = "Electrode Channel Assignment 06";
                regstim_06.Enabled = true;
                regstim_06.Reset();
                //
                //
                regstim_07.Address = "90000207";
                regstim_07.RegisterName = "Electrode Channel Assignment 07";
                regstim_07.Enabled = true;
                regstim_07.Reset();
                //
                //
                regstim_08.Address = "90000208";
                regstim_08.RegisterName = "Electrode Channel Assignment 08";
                regstim_08.Enabled = true;
                regstim_08.Reset();
                //
                //
                regstim_09.Address = "90000209";
                regstim_09.RegisterName = "Electrode Channel Assignment 09";
                regstim_09.Enabled = true;
                regstim_09.Reset();
                //
                //
                regstim_0A.Address = "9000020A";
                regstim_0A.RegisterName = "Electrode Channel Assignment 10";
                regstim_0A.Enabled = true;
                regstim_0A.Reset();
                //
                //
                regstim_0B.Address = "9000020B";
                regstim_0B.RegisterName = "Electrode Channel Assignment 11";
                regstim_0B.Enabled = true;
                regstim_0B.Reset();
                //
                //
                regstim_0C.Address = "9000020C";
                regstim_0C.RegisterName = "Electrode Channel Assignment 12";
                regstim_0C.Enabled = true;
                regstim_0C.Reset();
                //
                //
                regstim_0D.Address = "9000020D";
                regstim_0D.RegisterName = "Electrode Channel Assignment 13";
                regstim_0D.Enabled = true;
                regstim_0D.Reset();
                //
                //
                regstim_0E.Address = "9000020E";
                regstim_0E.RegisterName = "Electrode Channel Assignment 14";
                regstim_0E.Enabled = true;
                regstim_0E.Reset();
                //
                //
                regstim_0F.Address = "9000020F";
                regstim_0F.RegisterName = "Electrode Channel Assignment 15";
                regstim_0F.Enabled = true;
                regstim_0F.Reset();
                //
                //
                regstim_10.Address = "90000210";
                regstim_10.RegisterName = "";
                regstim_10.Enabled = false;
                regstim_10.Reset();
                //
                //
                regstim_11.Address = "90000211";
                regstim_11.RegisterName = "";
                regstim_11.Enabled = false;
                regstim_11.Reset();
                //
                //
                regstim_12.Address = "90000212";
                regstim_12.RegisterName = "";
                regstim_12.Enabled = false;
                regstim_12.Reset();
                //
                //
                regstim_13.Address = "90000213";
                regstim_13.RegisterName = "";
                regstim_13.Enabled = false;
                regstim_13.Reset();
                //
                //
                regstim_14.Address = "90000214";
                regstim_14.RegisterName = "";
                regstim_14.Enabled = false;
                regstim_14.Reset();
                //
                //
                regstim_15.Address = "90000215";
                regstim_15.RegisterName = "";
                regstim_15.Enabled = false;
                regstim_15.Reset();
                //
                //
                regstim_16.Address = "90000216";
                regstim_16.RegisterName = "";
                regstim_16.Enabled = false;
                regstim_16.Reset();
                //
                //
                regstim_17.Address = "90000217";
                regstim_17.RegisterName = "";
                regstim_17.Enabled = false;
                regstim_17.Reset();
                //
                //
                regstim_18.Address = "90000218";
                regstim_18.RegisterName = "";
                regstim_18.Enabled = false;
                regstim_18.Reset();
                //
                //
                regstim_19.Address = "90000219";
                regstim_19.RegisterName = "";
                regstim_19.Enabled = false;
                regstim_19.Reset();
                //
                //
                regstim_1A.Address = "9000021A";
                regstim_1A.RegisterName = "";
                regstim_1A.Enabled = false;
                regstim_1A.Reset();
                //
                //
                regstim_1B.Address = "9000021B";
                regstim_1B.RegisterName = "";
                regstim_1B.Enabled = false;
                regstim_1B.Reset();
                //
                //
                regstim_1C.Address = "9000021C";
                regstim_1C.RegisterName = "";
                regstim_1C.Enabled = false;
                regstim_1C.Reset();
                //
                //
                regstim_1D.Address = "9000021D";
                regstim_1D.RegisterName = "";
                regstim_1D.Enabled = false;
                regstim_1D.Reset();
                //
                //
                regstim_1E.Address = "9000021E";
                regstim_1E.RegisterName = "";
                regstim_1E.Enabled = false;
                regstim_1E.Reset();
                //
                //
                regstim_1F.Address = "9000021F";
                regstim_1F.RegisterName = "";
                regstim_1F.Enabled = false;
                regstim_1F.Reset();
                //
                //




            }

            if (tabStimRegisters.SelectedIndex == 6)
            {
                regstim_00.Address = "C0000000";
                regstim_00.RegisterName = "SP_I2C_Control";
                regstim_00.Enabled = true;

                regstim_00.Reset();
                //
                //
                regstim_01.Address = "C0000001";
                regstim_01.RegisterName = "SP_I2C_Command";
                regstim_01.Enabled = true;
                regstim_01.Reset();
                //
                //
                regstim_02.Address = "C0000002";
                regstim_02.RegisterName = "SP_CFX_Mem_Xfer_Mode";
                regstim_02.Enabled = true;
                regstim_02.Reset();
                //
                //
                regstim_03.Address = "C0000003";
                regstim_03.RegisterName = "SP_ARM_Mem_Xfer_Mode";
                regstim_03.Enabled = true;
                regstim_03.Reset();
                //
                //
                regstim_04.Address = "C0000004";
                regstim_04.RegisterName = "SP_Reg_Xfer_Mode";
                regstim_04.Enabled = true;
                regstim_04.Reset();
                //
                //
                regstim_05.Address = "C0000005";
                regstim_05.RegisterName = "SP_I2C_Status";
                regstim_05.Enabled = true;
                regstim_05.Reset();
                //
                //
                regstim_06.Address = "C0000006";
                regstim_06.RegisterName = "SP_I2C_Dest_Adrs";
                regstim_06.Enabled = true;
                regstim_06.Reset();
                //
                //
                regstim_07.Address = "C0000007";
                regstim_07.RegisterName = "SP_Memory_Adrs";
                regstim_07.Enabled = true;
                regstim_07.Reset();
                //
                //
                regstim_08.Address = "C0000008";
                regstim_08.RegisterName = "SP_Xfer_Count";
                regstim_08.Enabled = true;
                regstim_08.Reset();
                //
                //
                regstim_09.Address = "C0000009";
                regstim_09.RegisterName = "SP_I2C_Write_Data";
                regstim_09.Enabled = true;
                regstim_09.Reset();
                //
                //
                regstim_0A.Address = "C000000A";
                regstim_0A.RegisterName = "SP_I2C_Read_Data_Wd_0";
                regstim_0A.Enabled = true;
                regstim_0A.Reset();
                //
                //
                regstim_0B.Address = "C000000B";
                regstim_0B.RegisterName = "SP_I2C_Read_Data_Wd_1";
                regstim_0B.Enabled = true;
                regstim_0B.Reset();
                //
                //
                regstim_0C.Address = "C000000C";
                regstim_0C.RegisterName = "SP_I2C_Port_Status";
                regstim_0C.Enabled = true;
                regstim_0C.Reset();
                //
                //
                regstim_0D.Address = "C000000D";
                regstim_0D.RegisterName = "SP_I2C_FSM_State";
                regstim_0D.Enabled = true;
                regstim_0D.Reset();
                //
                //
                regstim_0E.Address = "C000000E";
                regstim_0E.RegisterName = "SP_I2C_FSM_State_Err";
                regstim_0E.Enabled = true;
                regstim_0E.Reset();
                //
                //
                regstim_0F.Address = "C000000F";
                regstim_0F.RegisterName = "";
                regstim_0F.Enabled = false;
                regstim_0F.Reset();
                //
                //
                regstim_10.Address = "C0000010";
                regstim_10.RegisterName = "VDD_EZ_Control";
                regstim_10.Enabled = true;
                regstim_10.Reset();
                //
                //
                regstim_11.Address = "C0000011";
                regstim_11.RegisterName = "VDD_EZ_Status";
                regstim_11.Enabled = true;
                regstim_11.Reset();
                //
                //
                regstim_12.Address = "C0000012";
                regstim_12.RegisterName = "VDD_EZ_UV_Adj";
                regstim_12.Enabled = true;
                regstim_12.Reset();
                //
                //
                regstim_13.Address = "C0000013";
                regstim_13.RegisterName = "VDD_EZ_LO_Adj";
                regstim_13.Enabled = true;
                regstim_13.Reset();
                //
                //
                regstim_14.Address = "C0000014";
                regstim_14.RegisterName = "VDD_EZ_HI_Adj";
                regstim_14.Enabled = true;
                regstim_14.Reset();
                //
                //
                regstim_15.Address = "C0000015";
                regstim_15.RegisterName = "";
                regstim_15.Enabled = false;
                regstim_15.Reset();
                //
                //
                regstim_16.Address = "C0000016";
                regstim_16.RegisterName = "";
                regstim_16.Enabled = false;
                regstim_16.Reset();
                //
                //
                regstim_17.Address = "C0000017";
                regstim_17.RegisterName = "";
                regstim_17.Enabled = false;
                regstim_17.Reset();
                //
                //
                regstim_18.Address = "C0000018";
                regstim_18.RegisterName = "Audio_Imp_Control";
                regstim_18.Enabled = true;
                regstim_18.Reset();
                //
                //
                regstim_19.Address = "C0000019";
                regstim_19.RegisterName = "Audio_Imp_Status";
                regstim_19.Enabled = true;
                regstim_19.Reset();
                //
                //
                regstim_1A.Address = "C000001A";
                regstim_1A.RegisterName = "Audio_LNA_Control";
                regstim_1A.Enabled = true;
                regstim_1A.Reset();
                //
                //
                regstim_1B.Address = "C000001B";
                regstim_1B.RegisterName = "Audio_LNA_Bias_In";
                regstim_1B.Enabled = true;
                regstim_1B.Reset();
                //
                //
                regstim_1C.Address = "C000001C";
                regstim_1C.RegisterName = "Audio_SCGC_Adj";
                regstim_1C.Enabled = true;
                regstim_1C.Reset();
                //
                //
                regstim_1D.Address = "C000001D";
                regstim_1D.RegisterName = "Audio_Mux_Select";
                regstim_1D.Enabled = true;
                regstim_1D.Reset();
                //
                //
                regstim_1E.Address = "C000001E";
                regstim_1E.RegisterName = "Audio_Mic_Adj";
                regstim_1E.Enabled = true;
                regstim_1E.Reset();
                //
                //
                regstim_1F.Address = "C000001F";
                regstim_1F.RegisterName = "Audio_Current_Bias";
                regstim_1F.Enabled = true;
                regstim_1F.Reset();
                //
                //




            }


            if (tabStimRegisters.SelectedIndex == 7)
            {
                regstim_00.Address = "C0000020";
                regstim_00.RegisterName = "Tone_Filter_Freq";
                regstim_00.Enabled = true;

                regstim_00.Reset();
                //
                //
                regstim_01.Address = "C0000040";
                regstim_01.RegisterName = "Audio_Control";
                regstim_01.Enabled = true;
                regstim_01.Reset();
                //
                //
                regstim_02.Address = "C0000041";
                regstim_02.RegisterName = "Audio_Status";
                regstim_02.Enabled = true;
                regstim_02.Reset();
                //
                //
                regstim_03.Address = "C0000042";
                regstim_03.RegisterName = "Tone_Control";
                regstim_03.Enabled = true;
                regstim_03.Reset();
                //
                //
                regstim_04.Address = "C0000043";
                regstim_04.RegisterName = "Tone_Volume";
                regstim_04.Enabled = true;
                regstim_04.Reset();
                //
                //
                regstim_05.Address = "C0000044";
                regstim_05.RegisterName = "Tone_Freq_1";
                regstim_05.Enabled = true;
                regstim_05.Reset();
                //
                //
                regstim_06.Address = "C0000045";
                regstim_06.RegisterName = "Tone_Freq_2";
                regstim_06.Enabled = true;
                regstim_06.Reset();
                //
                //
                regstim_07.Address = "C0000046";
                regstim_07.RegisterName = "Tone_DAC_Test";
                regstim_07.Enabled = true;
                regstim_07.Reset();
                //
                //
                regstim_08.Address = "C0000047";
                regstim_08.RegisterName = "Tone_Duration";
                regstim_08.Enabled = true;
                regstim_08.Reset();
                //
                //
                regstim_09.Address = "C0000048";
                regstim_09.RegisterName = "Confirmation_Tone_Status";
                regstim_09.Enabled = true;
                regstim_09.Reset();
                //
                //
                regstim_0A.Address = "D0000000";
                regstim_0A.RegisterName = "Serial_Comm13_Control";
                regstim_0A.Enabled = true;
                regstim_0A.Reset();
                //
                //
                regstim_0B.Address = "D0000001";
                regstim_0B.RegisterName = "Serial_Comm13_Status";
                regstim_0B.Enabled = true;
                regstim_0B.Reset();
                //
                //
                regstim_0C.Address = "D0000002";
                regstim_0C.RegisterName = "Serial_Comm13_Tx_Clock_Cntr";
                regstim_0C.Enabled = true;
                regstim_0C.Reset();
                //
                //
                regstim_0D.Address = "D0000003";
                regstim_0D.RegisterName = "Serial_Comm13_Rx_Clock_Cycle_Time";
                regstim_0D.Enabled = true;
                regstim_0D.Reset();
                //
                //
                regstim_0E.Address = "D0000004";
                regstim_0E.RegisterName = "Serial_Comm13_Stim_Num_Seqs (N)";
                regstim_0E.Enabled = true;
                regstim_0E.Reset();
                //
                //
                regstim_0F.Address = "E0000000";
                regstim_0F.RegisterName = "Serial_Comm14_Control";
                regstim_0F.Enabled = true;
                regstim_0F.Reset();
                //
                //
                regstim_10.Address = "E0000001";
                regstim_10.RegisterName = "Serial_Comm14_Status";
                regstim_10.Enabled = true;
                regstim_10.Reset();
                //
                //
                regstim_11.Address = "E0000002";
                regstim_11.RegisterName = "Serial_Comm14_Tx_Clock_Cntr";
                regstim_11.Enabled = true;
                regstim_11.Reset();
                //
                //
                regstim_12.Address = "E0000003";
                regstim_12.RegisterName = "Serial_Comm14_Rx_Clock_Cycle_Time";
                regstim_12.Enabled = true;
                regstim_12.Reset();
                //
                //
                regstim_13.Address = "E0000004";
                regstim_13.RegisterName = "Serial_Comm14_Bus_Operation_Timer_Value";
                regstim_13.Enabled = true;
                regstim_13.Reset();
                //
                //
                regstim_14.Address = "E0000010";
                regstim_14.RegisterName = "Serial_Comm14_Pwr_Control";
                regstim_14.Enabled = true;
                regstim_14.Reset();
                //
                //
                regstim_15.Address = "E0000011";
                regstim_15.RegisterName = "Serial_Comm14_Pwr_Status";
                regstim_15.Enabled = true;
                regstim_15.Reset();
                //
                //
                regstim_16.Address = "E0000012";
                regstim_16.RegisterName = "Serial_Comm14_Pwr_Tx_Clock_Cntr";
                regstim_16.Enabled = true;
                regstim_16.Reset();
                //
                //
                regstim_17.Address = "C0000013";
                regstim_17.RegisterName = "Serial_Comm14_Pwr_Rx_Clock_Cycle_Time";
                regstim_17.Enabled = true;
                regstim_17.Reset();
                //
                //
                regstim_18.Address = "E0000014";
                regstim_18.RegisterName = "Serial_Comm14_Pwr_Bus_Operation_Timer_Value";
                regstim_18.Enabled = true;
                regstim_18.Reset();
                //
                //
                regstim_19.Address = "";
                regstim_19.RegisterName = "";
                regstim_19.Enabled = false;
                regstim_19.Reset();
                //
                //
                regstim_1A.Address = "";
                regstim_1A.RegisterName = "";
                regstim_1A.Enabled = false;
                regstim_1A.Reset();
                //
                //
                regstim_1B.Address = "";
                regstim_1B.RegisterName = "";
                regstim_1B.Enabled = false;
                regstim_1B.Reset();
                //
                //
                regstim_1C.Address = "";
                regstim_1C.RegisterName = "";
                regstim_1C.Enabled = false;
                regstim_1C.Reset();
                //
                //
                regstim_1D.Address = "";
                regstim_1D.RegisterName = "";
                regstim_1D.Enabled = false;
                regstim_1D.Reset();
                //
                //
                regstim_1E.Address = "";
                regstim_1E.RegisterName = "";
                regstim_1E.Enabled = false;
                regstim_1E.Reset();
                //
                //
                regstim_1F.Address = "";
                regstim_1F.RegisterName = "";
                regstim_1F.Enabled = false;
                regstim_1F.Reset();
                //
                //




            }


            if (tabStimRegisters.SelectedIndex == 8)
            {
                regstim_00.Address = "F0000000";
                regstim_00.RegisterName = "Serial_Comm15_Control";
                regstim_00.Enabled = true;

                regstim_00.Reset();
                //
                //
                regstim_01.Address = "F0000001";
                regstim_01.RegisterName = "Serial_Comm15_Status";
                regstim_01.Enabled = true;
                regstim_01.Reset();
                //
                //
                regstim_02.Address = "F0000002";
                regstim_02.RegisterName = "Serial_Comm15_Tx_Clock_Cntr";
                regstim_02.Enabled = true;
                regstim_02.Reset();
                //
                //
                regstim_03.Address = "F0000003";
                regstim_03.RegisterName = "Serial_Comm15_Rx_Clock_Cycle_Time";
                regstim_03.Enabled = true;
                regstim_03.Reset();
                //
                //
                regstim_04.Address = "F0000004";
                regstim_04.RegisterName = "Serial_Comm15_Bus_Operation_Timer_Value";
                regstim_04.Enabled = true;
                regstim_04.Reset();
                //
                //
                regstim_05.Address = "F0000005";
                regstim_05.RegisterName = "Serial_Comm15_Stim_Num_Seqs";
                regstim_05.Enabled = true;
                regstim_05.Reset();
                //
                //
                regstim_06.Address = "F0000006";
                regstim_06.RegisterName = "Serial_Comm15_Slew_Rate";
                regstim_06.Enabled = true;
                regstim_06.Reset();
                //
                //
                regstim_07.Address = "F0000007";
                regstim_07.RegisterName = "Serial_Comm15_Term_Adj";
                regstim_07.Enabled = true;
                regstim_07.Reset();
                //
                //
                regstim_08.Address = "F0000008";
                regstim_08.RegisterName = "Serial_Comm15_Prechrg_Timer";
                regstim_08.Enabled = true;
                regstim_08.Reset();
                //
                //
                regstim_09.Address = "F0000009";
                regstim_09.RegisterName = "";
                regstim_09.Enabled = false;
                regstim_09.Reset();
                //
                //
                regstim_0A.Address = "F000000A";
                regstim_0A.RegisterName = "";
                regstim_0A.Enabled = false;
                regstim_0A.Reset();
                //
                //
                regstim_0B.Address = "F000000B";
                regstim_0B.RegisterName = "";
                regstim_0B.Enabled = false;
                regstim_0B.Reset();
                //
                //
                regstim_0C.Address = "F000000C";
                regstim_0C.RegisterName = "";
                regstim_0C.Enabled = false;
                regstim_0C.Reset();
                //
                //
                regstim_0D.Address = "F000000D";
                regstim_0D.RegisterName = "";
                regstim_0D.Enabled = false;
                regstim_0D.Reset();
                //
                //
                regstim_0E.Address = "F000000E";
                regstim_0E.RegisterName = "";
                regstim_0E.Enabled = false;
                regstim_0E.Reset();
                //
                //
                regstim_0F.Address = "F000000F";
                regstim_0F.RegisterName = "";
                regstim_0F.Enabled = false;
                regstim_0F.Reset();
                //
                //
                regstim_10.Address = "F0000010";
                regstim_10.RegisterName = "Serial_Comm15_Pwr_Control";
                regstim_10.Enabled = true;
                regstim_10.Reset();
                //
                //
                regstim_11.Address = "F0000011";
                regstim_11.RegisterName = "Serial_Comm15_Pwr_Status";
                regstim_11.Enabled = true;
                regstim_11.Reset();
                //
                //
                regstim_12.Address = "F0000012";
                regstim_12.RegisterName = "Serial_Comm15_Pwr_Tx_Clock_Cntr";
                regstim_12.Enabled = true;
                regstim_12.Reset();
                //
                //
                regstim_13.Address = "F0000013";
                regstim_13.RegisterName = "Serial_Comm15_Pwr_Rx_Clock_Cycle_Time";
                regstim_13.Enabled = true;
                regstim_13.Reset();
                //
                //
                regstim_14.Address = "F0000014";
                regstim_14.RegisterName = "Serial_Comm15_Pwr_Bus_Operation_Timer_Value";
                regstim_14.Enabled = true;
                regstim_14.Reset();
                //
                //
                regstim_15.Address = "F0000015";
                regstim_15.RegisterName = "Serial_Comm15_Pwr_Adj";
                regstim_15.Enabled = true;
                regstim_15.Reset();
                //
                //
                regstim_16.Address = "";
                regstim_16.RegisterName = "";
                regstim_16.Enabled = false;
                regstim_16.Reset();
                //
                //
                regstim_17.Address = "";
                regstim_17.RegisterName = "";
                regstim_17.Enabled = false;
                regstim_17.Reset();
                //
                //
                regstim_18.Address = "";
                regstim_18.RegisterName = "";
                regstim_18.Enabled = true;
                regstim_18.Reset();
                //
                //
                regstim_19.Address = "";
                regstim_19.RegisterName = "";
                regstim_19.Enabled = true;
                regstim_19.Reset();
                //
                //
                regstim_1A.Address = "";
                regstim_1A.RegisterName = "";
                regstim_1A.Enabled = true;
                regstim_1A.Reset();
                //
                //
                regstim_1B.Address = "";
                regstim_1B.RegisterName = "";
                regstim_1B.Enabled = true;
                regstim_1B.Reset();
                //
                //
                regstim_1C.Address = "";
                regstim_1C.RegisterName = "";
                regstim_1C.Enabled = true;
                regstim_1C.Reset();
                //
                //
                regstim_1D.Address = "";
                regstim_1D.RegisterName = "";
                regstim_1D.Enabled = true;
                regstim_1D.Reset();
                //
                //
                regstim_1E.Address = "";
                regstim_1E.RegisterName = "";
                regstim_1E.Enabled = true;
                regstim_1E.Reset();
                //
                //
                regstim_1F.Address = "";
                regstim_1F.RegisterName = "";
                regstim_1F.Enabled = true;
                regstim_1F.Reset();
                //
                //




            }


        }




        private void tabRegistersFocus(Object sender, EventArgs e)
            {
            tabRegisters.SelectedIndex = 1;
            tabRegisters.SelectedIndex = 0;
            reg_00.Visible = true;
            reg_01.Visible = true;
            reg_02.Visible = true;
            reg_03.Visible = true;
            reg_04.Visible = true;
            reg_05.Visible = true;
            reg_06.Visible = true;
            reg_07.Visible = true;
            reg_08.Visible = true;
            reg_09.Visible = true;
            reg_0A.Visible = true;
            reg_0B.Visible = true;
            reg_0C.Visible = true;
            reg_0D.Visible = true;
            reg_0E.Visible = true;
            reg_0F.Visible = true;
            reg_10.Visible = true;
            reg_11.Visible = true;
            reg_12.Visible = true;
            reg_13.Visible = true;
            reg_14.Visible = true;
            reg_15.Visible = true;
            reg_16.Visible = true;
            reg_17.Visible = true;
            reg_18.Visible = true;
            reg_19.Visible = true;
            reg_1A.Visible = true;
            reg_1B.Visible = true;
            reg_1C.Visible = true;
            reg_1D.Visible = true;
            reg_1E.Visible = true;
            reg_1F.Visible = true;

        }
        private void tabStimRegistersFocus(Object sender, EventArgs e)
        {
            tabStimRegisters.SelectedIndex = 1;
            tabStimRegisters.SelectedIndex = 0;
            regstim_00.Visible = true;
            regstim_01.Visible = true;
            regstim_02.Visible = true;
            regstim_03.Visible = true;
            regstim_04.Visible = true;
            regstim_05.Visible = true;
            regstim_06.Visible = true;
            regstim_07.Visible = true;
            regstim_08.Visible = true;
            regstim_09.Visible = true;
            regstim_0A.Visible = true;
            regstim_0B.Visible = true;
            regstim_0C.Visible = true;
            regstim_0D.Visible = true;
            regstim_0E.Visible = true;
            regstim_0F.Visible = true;
            regstim_10.Visible = true;
            regstim_11.Visible = true;
            regstim_12.Visible = true;
            regstim_13.Visible = true;
            regstim_14.Visible = true;
            regstim_15.Visible = true;
            regstim_16.Visible = true;
            regstim_17.Visible = true;
            regstim_18.Visible = true;
            regstim_19.Visible = true;
            regstim_1A.Visible = true;
            regstim_1B.Visible = true;
            regstim_1C.Visible = true;
            regstim_1D.Visible = true;
            regstim_1E.Visible = true;
            regstim_1F.Visible = true;

        }

        //private void btnReadModelNumber_Click(object sender, EventArgs e)
        //{
        //    //byte[] addr = { 0x00, 0x00, 0x00, 0x20 };
        //    byte[] data = { 0x00, 0x00, 0x00, 0x00 }; ;

        //    //byte[] addr_array = StringToByteArray(txtReadAddress.Text);

        //    //byte[] addr = addr_array.Reverse().ToArray();

        //    byte addr = 0x0E;

        //    txtReadResponse.Text = "";
        //    uint response = BLE_SPI.BLE_SPI_READ_8(addr, ref data);
        //    if (response == 1)
        //    {
        //        txtReadResponse.Text = "Success: " + BitConverter.ToString(data);
        //        txtReadData.Text = BitConverter.ToString(data);

        //    }
        //    else
        //    {
        //        txtReadResponse.Text = "Fail: ";
        //        spi.CloseSerialPort();
        //        spi.InitSerialPort("COM3");

        //    };
        //}

        private void button1_Click_2(object sender, EventArgs e)
        {
            uint response = 0;
            byte[] addr_array = StringToByteArray("30000011");
            byte[] addr = addr_array.Reverse().ToArray();
            byte[] data_array = StringToByteArray("00000800");
            byte[] data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            
            addr_array = StringToByteArray("30000012");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000800");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            addr_array = StringToByteArray("30000016");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000006");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            addr_array = StringToByteArray("30000013");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000000B");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            //System.Threading.Thread.Sleep(500);


            addr_array = StringToByteArray("30000016");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000005");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            addr_array = StringToByteArray("30000013");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000000B");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            //System.Threading.Thread.Sleep(500);


            addr_array = StringToByteArray("3000001A");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000000");
            data = data_array.Reverse().ToArray();

            byte[] dataread = { 0x00, 0x00, 0x00, 0x00 };
            BLE_SPI.BLE_SPI_READ_32(addr, ref dataread);


            addr_array = StringToByteArray("30000017");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000004");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000018");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00001004");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            addr_array = StringToByteArray("30000019");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("000089AB");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            addr_array = StringToByteArray("30000016");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000002");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            addr_array = StringToByteArray("30000013");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000000B");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            //System.Threading.Thread.Sleep(500);

            addr_array = StringToByteArray("30000016");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000005");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            addr_array = StringToByteArray("30000013");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000000B");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            //System.Threading.Thread.Sleep(500);


        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            uint response = 0;
            byte[] addr_array = StringToByteArray("30000011");
            byte[] addr = addr_array.Reverse().ToArray();
            byte[] data_array = StringToByteArray("00000800");
            byte[] data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000012");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000800");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000017");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000004");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            addr_array = StringToByteArray("30000018");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00001004");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000016");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000003");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000013");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000000B");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            System.Threading.Thread.Sleep(500);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            uint response = 0;
            byte[] addr_array = StringToByteArray("30000016");
            byte[] addr = addr_array.Reverse().ToArray();
            byte[] data_array = StringToByteArray("00000006");
            byte[] data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            System.Threading.Thread.Sleep(500);

            addr_array = StringToByteArray("30000013");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000000B");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            System.Threading.Thread.Sleep(500);


            addr_array = StringToByteArray("30000017");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000004");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            System.Threading.Thread.Sleep(500);


            addr_array = StringToByteArray("30000018");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00001000");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            System.Threading.Thread.Sleep(500);


            addr_array = StringToByteArray("30000016");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000020");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            System.Threading.Thread.Sleep(500);


            addr_array = StringToByteArray("30000013");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000000B");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            System.Threading.Thread.Sleep(500);


            addr_array = StringToByteArray("30000016");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000005");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000013");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000000B");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            System.Threading.Thread.Sleep(500);
        }

        private void btnReadFlashStatus_Click(object sender, EventArgs e)
        {
            uint response = 0;
            byte[] addr_array = StringToByteArray("30000011");
            byte[] addr = addr_array.Reverse().ToArray();
            byte[] data_array = StringToByteArray("00000083");
            byte[] data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000012");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000FFF");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000016");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000005");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000013");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000000B");
            data = data_array.Reverse().ToArray();

            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("3000001A");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000000");
            data = data_array.Reverse().ToArray();

            byte[] dataread = { 0x00, 0x00, 0x00, 0x00 };
            BLE_SPI.BLE_SPI_READ_32(addr, ref dataread);

            txtFlashStatus.Text = ByteArrayToString(dataread.Reverse().ToArray());

        }



        private void Test_SPI_Load(object sender, EventArgs e)
        {
            //txtMACAddress.Text = "CFD4C220DA4C";
            txtMACAddress.Text = "E8CD11BC7B77";

        }

        private void btnReadConfigEEPROM_Click(object sender, EventArgs e)
        {
            int startAddress = int.Parse("60000500", System.Globalization.NumberStyles.HexNumber);
            int stopAddress = int.Parse("6000057F", System.Globalization.NumberStyles.HexNumber);
            uint response = 0;
            string dataString = "";

            dataGridEEPROM.Rows.Clear();
            dataGridEEPROM.Refresh();


            //byte[] addr = { 0x00, 0x00, 0x00, 0x20 };
            byte[] data = { 0x00, 0x00, 0x00, 0x00 };

            byte[] addr = StringToByteArray("60000500").Reverse().ToArray();

            int j = 0;
            for (int i = startAddress; i <= stopAddress; i++)
            {
                Array.Clear(data, 0, data.Length);
                addr = StringToByteArray((i).ToString("X")).Reverse().ToArray();
                //BLE_SPI.sp.DiscardInBuffer();
                response = BLE_SPI.BLE_SPI_READ_32(addr, ref data);

                if (response == 1)
                {

                    dataString = ByteArrayToString(data);
                    dataString = dataString.Substring(8, 8);
                    dataGridEEPROM.Rows.Add(i.ToString("X").Insert(4, "_"), dataString.Insert(4, "_"));
                }
                else
                {
                    //txtReadResponse.Text = "Fail: ";
                    //spi.CloseSerialPort();
                    //spi.InitSerialPort("COM3");

                };

                //System.Threading.Thread.Sleep(1000);

                j++;

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridEEPROM.Rows.Clear();
            dataGridEEPROM.Refresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string addressString;
            string dataString;

            byte[] addr_array = StringToByteArray("20000000");

            byte[] addr = addr_array.Reverse().ToArray();

            byte[] data_array = StringToByteArray("00000000");

            byte[] data = data_array.Reverse().ToArray();
            uint response = 0;


            foreach (DataGridViewRow row in dataGridEEPROM.Rows)
            {
                if (row.Cells["AddressEE"].Value != null)
                {
                    addressString = row.Cells["AddressEE"].Value.ToString().Replace("_", "");
                    dataString = row.Cells["DataEE"].Value.ToString().Replace("_", "");

                    addr_array = StringToByteArray(addressString);
                    data_array = StringToByteArray(dataString);

                    addr = addr_array.Reverse().ToArray();
                    data = data_array.Reverse().ToArray();
                    response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


                }
            }
        }

        private void btnLoadCheckSum_Click(object sender, EventArgs e)
        {
            int startAddress = int.Parse("60000500", System.Globalization.NumberStyles.HexNumber);
            int stopAddress = int.Parse("6000057E", System.Globalization.NumberStyles.HexNumber);
            uint response = 0;
            string dataString = "";

            dataGridEEPROM.Rows.Clear();
            dataGridEEPROM.Refresh();

            int sum = 0;
            int temp;
            //byte[] addr = { 0x00, 0x00, 0x00, 0x20 };
            byte[] data = { 0x00, 0x00, 0x00, 0x00 };

            byte[] addrsum = StringToByteArray("6000057F").Reverse().ToArray();
            byte[] addr = StringToByteArray("60000500").Reverse().ToArray();
            int j = 0;
            for (int i = startAddress; i <= stopAddress; i++)
            {
                Array.Clear(data, 0, data.Length);
                addr = StringToByteArray((i).ToString("X")).Reverse().ToArray();
                //BLE_SPI.sp.DiscardInBuffer();
                response = BLE_SPI.BLE_SPI_READ_32(addr, ref data);

                if (response == 1)
                {
                    
                    BitConverter.ToUInt32(data.Reverse().ToArray(),0);
                    temp = 256 *data[6] + data[7];
                    sum = sum + temp;
                    //MessageBox.Show(temp.ToString("X"));
                    dataString = ByteArrayToString(data);
                    dataString = dataString.Substring(8, 8);
                    dataGridEEPROM.Rows.Add(i.ToString("X").Insert(4, "_"), dataString.Insert(4, "_"));
                }
                else
                {
                    //txtReadResponse.Text = "Fail: ";
                    //spi.CloseSerialPort();
                    //spi.InitSerialPort("COM3");

                };

                //System.Threading.Thread.Sleep(1000);

                j++;

            }

            sum = ~sum &0x0000FFFF;
            byte btsum1 = (byte)((sum & 0x0000FF00) >> 8 );
            byte btsum0 = (byte)(sum & 0x000000FF);
            //MessageBox.Show(sum.ToString("X"));
            byte[] chksum = { btsum0, btsum1, 0x00, 0x00 };
            response = BLE_SPI.BLE_SPI_WRITE_32(addrsum, ref chksum);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            {
                string CsvFpath = txtPMICEEPROMFile.Text;
                try
                {
                    System.IO.StreamWriter csvFileWriter = new StreamWriter(CsvFpath, false);

                    string columnHeaderText = "";

                    int countColumn = dataGridEEPROM.ColumnCount - 1;

                    if (countColumn >= 0)
                    {
                        columnHeaderText = dataGridEEPROM.Columns[0].HeaderText;
                    }

                    for (int i = 1; i <= countColumn; i++)
                    {
                        columnHeaderText = columnHeaderText + ',' + dataGridEEPROM.Columns[i].HeaderText;
                    }


                    //csvFileWriter.WriteLine(columnHeaderText);

                    foreach (DataGridViewRow dataRowObject in dataGridEEPROM.Rows)
                    {
                        if (!dataRowObject.IsNewRow)
                        {
                            string dataFromGrid = "";

                            dataFromGrid = dataRowObject.Cells[0].Value.ToString();

                            for (int i = 1; i <= countColumn; i++)
                            {
                                dataFromGrid = dataFromGrid + ',' + dataRowObject.Cells[i].Value.ToString();

                                csvFileWriter.WriteLine(dataFromGrid);
                            }
                        }
                    }


                    csvFileWriter.Flush();
                    csvFileWriter.Close();
                }
                catch (Exception exceptionObject)
                {
                    MessageBox.Show(exceptionObject.ToString());
                }
            }
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtPMICEEPROMFile.Text = openFileDialog1.FileName;

            dataGridEEPROM.Rows.Clear();
            dataGridEEPROM.Refresh();

            using (var reader = new StreamReader(txtPMICEEPROMFile.Text))
            {
                List<string> listA = new List<string>();
                List<string> listB = new List<string>();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    dataGridEEPROM.Rows.Add(values[0], values[1]);

                }
            }

        }

        private void tabPage13_Click(object sender, EventArgs e)
        {

        }

        private void btnRefreshImplantStatus_Click(object sender, EventArgs e)
        {
            byte[] data = { 0x00, 0x00, 0x00, 0x00 };
           

            BLE_SPI.BLE_READ_BATTERY_VOLTAGE(ref data);

            byte[] voltage = { data[6], data[5], data[4], data[3] };

            Single voltagef = BitConverter.ToSingle(voltage, 0);
            lblBatteryVoltage.Text = voltagef.ToString("0.000") + " Volts";




            BLE_SPI.BLE_READ_IMPLANT_TEMP(ref data);

            byte[] temperature1 = { data[6], data[5], data[4], data[3] };
            byte[] temperature2 = { data[10], data[9], data[8], data[7] };
            Single temperature1f = BitConverter.ToSingle(temperature1, 0);
            Single temperature2f = BitConverter.ToSingle(temperature2, 0);
            lblBatteryTemperature1.Text = temperature1f.ToString("00.000") + " C";
            lblBatteryTemperature2.Text = temperature2f.ToString("00.000") + " C";
        }

        private void btnTemperatureSensorsRun_Click(object sender, EventArgs e)
        {
            var series = new Series("Sensor 1");
            var series2 = new Series("Sensor 2");
            series.ChartType = SeriesChartType.Line;
            series2.ChartType = SeriesChartType.Line;

            List<Single> t1 = new List<Single>();
            List<Single> t2 = new List<Single>();

            for (int i = 1; i <= 300; i++) { 
                byte[] data = { 0x00, 0x00, 0x00, 0x00 };
                // byte[] voltage = { 0x00, 0x00, 0x00, 0x00 };

                //BLE_SPI.BLE_READ_BATTERY_VOLTAGE(ref data);

                //byte[] voltage = { data[6], data[5], data[4], data[3] };

                //Single voltagef = BitConverter.ToSingle(voltage, 0);
                //lblBatteryVoltage.Text = voltagef.ToString("0.000") + " Volts";

                BLE_SPI.BLE_READ_IMPLANT_TEMP(ref data);

                byte[] temperature1 = { data[6], data[5], data[4], data[3] };
                byte[] temperature2 = { data[10], data[9], data[8], data[7] };
                Single temperature1f = BitConverter.ToSingle(temperature1, 0);
                Single temperature2f = BitConverter.ToSingle(temperature2, 0);
                lblBatteryTemperature1.Text = temperature1f.ToString("00.000") + " C";
                lblBatteryTemperature2.Text = temperature2f.ToString("00.000") + " C";

                System.Threading.Thread.Sleep(1000);
                series.Points.AddXY(i, temperature1f);
                series2.Points.AddXY(i, temperature2f);

                t1.Add(temperature1f);
                t2.Add(temperature2f);


            }
            chartTempSensors.ChartAreas[0].AxisX.Title = "Time (Seconds)";
            chartTempSensors.ChartAreas[0].AxisY.Title = "Degrees (Celcius)";
            
             


            chartTempSensors.Series.Clear();
            chartTempSensors.Series.Add(series);
            chartTempSensors.Series.Add(series2);
            chartTempSensors.Series[0].BorderWidth = 3;

            //MessageBox.Show(t1.Average().ToString());
            //MessageBox.Show(t2.Average().ToString());

            Single t1m = getStandardDeviation(t1);
            Single t2m = getStandardDeviation(t2);

            //MessageBox.Show(t1m.ToString());
            //MessageBox.Show(t2m.ToString());


            lblTemp1Mean.Text = t1.Average().ToString();
            lblTemp2Mean.Text = t2.Average().ToString();
            lblTemp1MSE.Text = t1m.ToString();
            lblTemp2MSE.Text = t2m.ToString();


        }

        private Single getStandardDeviation(List<Single> singleList)
        {
            Single average = singleList.Average();
            Single sumOfDerivation = 0;
            foreach (Single value in singleList)
            {
                sumOfDerivation += (value) * (value);
            }
            Single sumOfDerivationAverage = sumOfDerivation / (singleList.Count - 1);
            return (Single)Math.Sqrt(sumOfDerivationAverage - (average * average));
        }



        private void btnRunTempsSlow_Click(object sender, EventArgs e)
        {
            uint response = 0;
            byte[] addr_array = StringToByteArray("30000008");
            byte[] addr = addr_array.Reverse().ToArray();
            byte[] data_array = StringToByteArray("00000001");
            byte[] data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);


            addr_array = StringToByteArray("30000000");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000190");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000003");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000002D");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000004");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000003");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000001");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000001F");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000002");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000000");
            data = data_array.Reverse().ToArray();

            byte[] dataread = { 0x00, 0x00, 0x00, 0x00 };
            BLE_SPI.BLE_SPI_READ_32(addr, ref dataread);

            byte[] vdd = { dataread[7], dataread[6], dataread[5], dataread[4] };
            int vddint = BitConverter.ToInt32(vdd, 0);

            addr_array = StringToByteArray("30000000");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("000003F0");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000004");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000001");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000001");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000001F");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000002");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000000");
            data = data_array.Reverse().ToArray();

            byte[] dataread1 = { 0x00, 0x00, 0x00, 0x00 };
            BLE_SPI.BLE_SPI_READ_32(addr, ref dataread1);

            byte[] ntc0 = { dataread1[7], dataread1[6], dataread1[5], dataread1[4] };
            int ntc0int = BitConverter.ToInt32(ntc0, 0);

            addr_array = StringToByteArray("30000004");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000002");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000001");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("0000001F");
            data = data_array.Reverse().ToArray();
            response = BLE_SPI.BLE_SPI_WRITE_32(addr, ref data);

            addr_array = StringToByteArray("30000002");
            addr = addr_array.Reverse().ToArray();
            data_array = StringToByteArray("00000000");
            data = data_array.Reverse().ToArray();

            byte[] dataread2 = { 0x00, 0x00, 0x00, 0x00 };
            BLE_SPI.BLE_SPI_READ_32(addr, ref dataread2);

            byte[] ntc1 = { dataread2[7], dataread2[6], dataread2[5], dataread2[4] };
            int ntc1int = BitConverter.ToInt32(ntc1, 0);

            double temp0 = -365.8 * ((double)ntc0int / (double)vddint) + 94.9;
            double temp1 = -365.8 * ((double)ntc1int / (double)vddint) + 94.9;

            MessageBox.Show("asdf");

        }

        private void btnDisableLoop_Click(object sender, EventArgs e)
        {
            BLE_SPI.BLE_SPI_DISABLE_LOOP();
        }

        private void button1_Click_4(object sender, EventArgs e)
        {
            BLE_SPI.BLE_SPI_ENABLE_LOOP();
        }
    }

}










   

