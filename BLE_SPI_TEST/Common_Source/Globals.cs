﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Media;
using System.Threading;
using System.Windows.Forms;

namespace ASIC_Test_Interface
{
    class Globals
    {
        public static System.IntPtr FTDI_HANDLE;

        // Esteem 3 Diagnostic SPI Port Registers
        public const int E3_RdDataL = 0x00;
        public const int E3_RdDataH = 0x01;
        public const int E3_WrDataL = 0x02;
        public const int E3_WrDataH = 0x03;
        public const int E3_AddrL   = 0x04;
        public const int E3_AddrH   = 0x05;
        public const int E3_Stat    = 0x06;

        // Cochlear Diagnostic SPI Port Registers
        public const int FICI_RdData3 = 0x00;
        public const int FICI_RdData2 = 0x01;
        public const int FICI_RdData1 = 0x02;
        public const int FICI_RdData0 = 0x03;

        public const int FICI_WrData3 = 0x04;
        public const int FICI_WrData2 = 0x05;
        public const int FICI_WrData1 = 0x06;
        public const int FICI_WrData0 = 0x07;

        public const int FICI_Addr3   = 0x08;
        public const int FICI_Addr2   = 0x09;
        public const int FICI_Addr1   = 0x0A;
        public const int FICI_Addr0   = 0x0B;

        public const int FICI_Stat    = 0x0C;

        public const int FICI_ModelNum_Reg = 0x0E;       // 14 dec = 0x0E = SPI Port Adrs of Model # reg

        // Diagnostic SPI Port Commands
        public const int DIAG_RD_CMD = 0x10;
        public const int DIAG_WR_CMD = 0x20;

        // Other definitions
        public const uint FTC_SUCCESS = 0;

        public static bool BleRemoteMode = false;        // true = running BLE RF Link  (in E3, this is similar to RfRemoteMode)

        //public static FTDI.FTDI ftdi = new FTDI.FTDI();

        private const byte E3_MODEL_NUMBER   = 0xC9;     // 201 dec = 0xC9 = Esteem III
        private const byte FICI_MODEL_NUMBER = 0xCC;     // 204 dec = 0xCC = Esteem Cochlear



        // Esteem 3 Diagnostic SPI Port - READ Esteem 3 Configuration Register
        public static uint ReadE3Reg(uint addr, ref uint data)
        {
            uint ftStatus = 99;
            Byte addrl, addrh, datal, datah, tmpb;
            bool pending = true;

            addrl = (Byte)addr;
            addrh = (Byte)(addr >> 8);
            addrh |= DIAG_RD_CMD;

            //ftStatus = ftdi.E3_write_diag(FTDI_HANDLE, (Byte) E3_AddrL, addrl);
            if (ftStatus == FTC_SUCCESS)
             // ftStatus = ftdi.E3_write_diag(FTDI_HANDLE, (Byte) E3_AddrH, addrh);

            while ((ftStatus == FTC_SUCCESS) && pending)
                 {
                  //Thread.Sleep(100);
                  tmpb = 1;

                  // Read Diagnostic Port Status Reg
                //  ftStatus = ftdi.E3_read_diag(FTDI_HANDLE, (Byte)E3_Stat, ref tmpb);

                  if ((tmpb & 0x01) == 0x00)
                    pending = false;
                 }

            //Response ready
            if (ftStatus == FTC_SUCCESS)
              {
               datal = 0;
               datah = 0;
               //ftStatus = ftdi.E3_read_diag(FTDI_HANDLE, (Byte)E3_RdDataL, ref datal);
               //ftStatus = ftdi.E3_read_diag(FTDI_HANDLE, (Byte)E3_RdDataH, ref datah);
               data = (UInt16)((UInt16)((UInt16)datah << 8) + (UInt16)datal);
              }
            else {
                  data = 0;
                  // reconnect();
                 }


            //MessageBox.Show(data.ToString("X"));


            return ftStatus;
        }


        // Cochlear Diagnostic SPI Port - READ Cochlear Configuration Register
        public static  uint  ReadFICIReg(UInt32 addr, ref UInt32 data)
        {
            uint ftStatus = 99;
            Byte addrl, addrh, datal, datah, tmpb;
            bool pending = true;

            addrl = (Byte) addr;
            addrh = (Byte)(addr >> 8);
            addrh |= DIAG_RD_CMD;

            byte[] AbyteArray = BitConverter.GetBytes(addr);
            byte[] DbyteArray = new byte[4];

            byte[] address = new byte[4];
            byte[] dData = new byte[4];



            address[0] = AbyteArray[3];
            address[1] = AbyteArray[2];
            address[2] = AbyteArray[1];
            address[3] = AbyteArray[0];


            // Write ASIC Adrs to Diagnostic Port
            //ftStatus = ftdi.FICI_write_diag(FTDI_HANDLE, (Byte) FICI_Addr3, address);
            System.Threading.Thread.Sleep(1);

            // Write Diagnostic Port Command
            if (ftStatus == FTC_SUCCESS)
              //ftStatus = ftdi.E3_write_diag(FTDI_HANDLE, (Byte) FICI_Stat, DIAG_RD_CMD);
            System.Threading.Thread.Sleep(1);

            // Wait for Diagnostic Port operation to complete
            int i = 0;

            while ((ftStatus == FTC_SUCCESS) && pending && (i < 100))
                 {
                  //Thread.Sleep(100);
                  tmpb = 1;

                  // Read Diagnostic Port Status Reg
                  //ftStatus = ftdi.E3_read_diag(FTDI_HANDLE, (Byte) FICI_Stat, ref tmpb);

                  System.Threading.Thread.Sleep(100);

                  if ((tmpb & 0x01) == 0x00)
                    pending = false;
                  i++;
                 }

            if (i == 100) MessageBox.Show("did not get a response");

            // Read Data is available in Diagnostic Port
            if (ftStatus == FTC_SUCCESS)
              {
               //ftStatus = ftdi.FICI_read_diag(FTDI_HANDLE, (Byte) FICI_RdData3, ref DbyteArray);

               System.Threading.Thread.Sleep(1);

               //ftStatus = ftdi.FICI_read_diag(FTDI_HANDLE, (Byte)E3_RdDataH, ref datah);
               //data = (UInt16)((UInt16)((UInt16)datah << 8) + (UInt16)datal);


               dData[0] = DbyteArray[3];
               dData[1] = DbyteArray[2];
               dData[2] = DbyteArray[1];
               dData[3] = DbyteArray[0];


               //MessageBox.Show(BitConverter.ToString(DbyteArray));


              }
            else {
                  data = 0;
                  // reconnect();
                 }


            //MessageBox.Show(data.ToString("X"));

            data = (uint)(dData[0] + 256*dData[1] + 65536*dData[2] + 16777216*dData[3]);

            return ftStatus;
        }


        // Esteem 3 Diagnostic SPI Port - WRITE Esteem 3 Configuration Register
        public static uint WriteE3Reg(UInt16 addr, UInt32 data)
        {
            uint ftStatus = 99;
            Byte addrl, addrh, datal, datah, tmpb;
            bool pending = true;

            addrl = (Byte)addr;
            addrh = (Byte)(addr >> 8);
            addrh |= DIAG_WR_CMD;
            datal = (Byte)data;
            datah = (Byte)(data >> 8);

            //MessageBox.Show(addrh.ToString());
            //MessageBox.Show(addrl.ToString());
            //MessageBox.Show(datah.ToString());
            //MessageBox.Show(datal.ToString());

            //ftStatus = ftdi.E3_write_diag(FTDI_HANDLE, (Byte)E3_WrDataL, datal);

            if (ftStatus == FTC_SUCCESS)
              //ftStatus = ftdi.E3_write_diag(FTDI_HANDLE, (Byte)E3_WrDataH, datah);

            if (ftStatus == FTC_SUCCESS)
              //ftStatus = ftdi.E3_write_diag(FTDI_HANDLE, (Byte)E3_AddrL, addrl);

            if (ftStatus == FTC_SUCCESS)
             // ftStatus = ftdi.E3_write_diag(FTDI_HANDLE, (Byte)E3_AddrH, addrh);

            while ((ftStatus == FTC_SUCCESS) && pending)
                 {
                  tmpb = 1;

                  // Read Diagnostic Port Status Reg
                  //ftStatus = ftdi.E3_read_diag(FTDI_HANDLE, (Byte)E3_Stat, ref tmpb);

                  if ((tmpb & 0x01) == 0x00)
                    pending = false;
                 }
            return ftStatus;
        }


        // Cochlear Diagnostic SPI Port - WRITE Cochlear Configuration Register
        public  static  uint  WriteFICIReg(UInt32  addr, UInt32  data)
        {
            uint ftStatus = 99;
            Byte addrl, addrh, datal, datah, tmpb;
            bool pending = true;

            addrl = (Byte)addr;
            addrh = (Byte)(addr >> 8);
            //addrh |= FICI_DIAG_WR_CMD;
            datal = (Byte)data;
            datah = (Byte)(data >> 8);

            byte[] AbyteArray = BitConverter.GetBytes(addr);
            byte[] DbyteArray = BitConverter.GetBytes(data);

            byte[] address = new byte[4];
            byte[] dData   = new byte[4];



            address[0] = AbyteArray[3];
            address[1] = AbyteArray[2];
            address[2] = AbyteArray[1];
            address[3] = AbyteArray[0];

            dData[0] = DbyteArray[3];
            dData[1] = DbyteArray[2];
            dData[2] = DbyteArray[1];
            dData[3] = DbyteArray[0];


            // Write ASIC Adrs to Diagnostic Port
            //ftStatus = ftdi.FICI_write_diag(FTDI_HANDLE, (Byte) FICI_Addr3, address);
            System.Threading.Thread.Sleep(100);

            // Write ASIC Wr Data to Diagnostic Port
            if (ftStatus == FTC_SUCCESS)
             // ftStatus = ftdi.FICI_write_diag(FTDI_HANDLE, (Byte) FICI_WrData3, dData);
            System.Threading.Thread.Sleep(100);

            // Write Diagnostic Port Command
            if (ftStatus == FTC_SUCCESS)
           //   ftStatus = ftdi.E3_write_diag(FTDI_HANDLE, (Byte) FICI_Stat, DIAG_WR_CMD);
            System.Threading.Thread.Sleep(100);

            // Wait for Diagnostic Port operation to complete
            if (ftStatus == FTC_SUCCESS)
           //   ftStatus = ftdi.E3_write_diag(FTDI_HANDLE, (Byte) FICI_Addr3, addrh);

            while ((ftStatus == FTC_SUCCESS) && pending)
                 {
                  tmpb = 1;

                  // Read Diagnostic Port Status Reg
                //  ftStatus = ftdi.E3_read_diag(FTDI_HANDLE, (Byte) FICI_Stat, ref tmpb);

                  if ((tmpb & 0x01) == 0x00)
                    pending = false;
                 }

            return ftStatus;
       }
    }
}
