﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ASIC_Test_Interface;

namespace ASIC_Test_Interface
{
    public partial class E3_Register8 : UserControl
    {
        public E3_Register8()
        {
            InitializeComponent();
        }
//        public E3_Register8(Form inForm)
//        {
//            InitializeComponent();
//        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            updateValue();
        }

        private void checkBoxHover1(object sender, EventArgs e)
        {
            lblBitName.Text = bit1_text;
        }
        private void checkBoxHover2(object sender, EventArgs e)
        {
            lblBitName.Text = bit2_text;
        }
        private void checkBoxHover3(object sender, EventArgs e)
        {
            lblBitName.Text = bit3_text;
        }
        private void checkBoxHover4(object sender, EventArgs e)
        {
            lblBitName.Text = bit4_text;
        }
        private void checkBoxHover5(object sender, EventArgs e)
        {
            lblBitName.Text = bit5_text;
        }
        private void checkBoxHover6(object sender, EventArgs e)
        {
            lblBitName.Text = bit6_text;
        }
        private void checkBoxHover7(object sender, EventArgs e)
        {
            lblBitName.Text = bit7_text;
        }
        private void checkBoxHover8(object sender, EventArgs e)
        {
            lblBitName.Text = bit8_text;
        }





        private void checkBoxLeave(object sender, EventArgs e)
        {
            lblBitName.Text = this.Name;
        }



        private void updateValue()
        {
            int chk0 = 0;
            int chk1 = 0;
            int chk2 = 0;
            int chk3 = 0;
            int chk4 = 0;
            int chk5 = 0;
            int chk6 = 0;
            int chk7 = 0;
            ushort value = 0;



            if (checkBox1.Checked.Equals(true) )
            {
                chk0 = 1;
            };
            if (checkBox2.Checked.Equals(true))
            {
                chk1 = 2;
            };
            if (checkBox3.Checked.Equals(true))
            {
                chk2 = 4;
            };
            if (checkBox4.Checked.Equals(true))
            {
                chk3 = 8;
            };
            if (checkBox5.Checked.Equals(true))
            {
                chk4 = 16;
            };
            if (checkBox6.Checked.Equals(true))
            {
                chk5 = 32;
            };
            if (checkBox7.Checked.Equals(true))
            {
                chk6 = 64;
            };
            if (checkBox8.Checked.Equals(true))
            {
                chk7 = 128;
            };

            value = (ushort)(chk0 + chk1 + chk2 + chk3 + chk4 + chk5 + chk6 + chk7);

            if (value < 16)
            {
                lblValue.Text = "0x0" + value.ToString("X");
            }
            else
            {
                lblValue.Text = "0x" + value.ToString("X");
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            uint value = 0x0000FFFF;
            uint ftstatus = 99;
            byte[] value_byte = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            //ftstatus = Globals.ReadFICIReg(this.Address, ref value);
            ACCLAIM_BLE_SERIAL.BLE_SPI.BLE_SPI_READ_8((byte)this.address, ref value_byte);



            value = (uint)(0x000000FF & (uint)value_byte[4]);

            checkBox1.BackColor = Color.DarkGray;
            checkBox2.BackColor = Color.DarkGray;
            checkBox3.BackColor = Color.DarkGray;
            checkBox4.BackColor = Color.DarkGray;
            checkBox5.BackColor = Color.DarkGray;
            checkBox6.BackColor = Color.DarkGray;
            checkBox7.BackColor = Color.DarkGray;
            checkBox8.BackColor = Color.DarkGray;



            if (value > 127)
            {
                value = (ushort)(value - 128);
                if (checkBox8.Checked == false)
                {
                    checkBox8.BackColor = Color.Red;
                    
                }
            } else { 
                if (checkBox8.Checked == true)
                {
                    checkBox8.BackColor = Color.Red;
                }
            }
            if (value > 63)
            {
                value = (ushort)(value - 64);
                if (checkBox7.Checked == false)
                {
                    checkBox7.BackColor = Color.Red;
                    
                }
            } else { 
                if (checkBox7.Checked == true)
                {
                    checkBox7.BackColor = Color.Red;
                }
            }
            if (value > 31)
            {
                value = (ushort)(value - 32);
                if (checkBox6.Checked == false)
                {
                    checkBox6.BackColor = Color.Red;
                    
                }
            } else { 
                if (checkBox6.Checked == true)
                {
                    checkBox6.BackColor = Color.Red;
                }
            }
            if (value > 15)
            {
                value = (ushort)(value - 16);
                if (checkBox5.Checked == false)
                {
                    checkBox5.BackColor = Color.Red;
                    
                }
            } else { 
                if (checkBox5.Checked == true)
                {
                    checkBox5.BackColor = Color.Red;
                }
            }
            if (value > 7)
            {
                value = (ushort)(value - 8);
                if (checkBox4.Checked == false)
                {
                    checkBox4.BackColor = Color.Red;
                    
                }
            } else { 
                if (checkBox4.Checked == true)
                {
                    checkBox4.BackColor = Color.Red;
                }
            }
            if (value > 3)
            {
                value = (ushort)(value - 4);
                if (checkBox3.Checked == false)
                {
                    checkBox3.BackColor = Color.Red;
                    
                }
            } else { 
                if (checkBox3.Checked == true)
                {
                    checkBox3.BackColor = Color.Red;
                }
            }
            if (value > 1)
            {
                value = (ushort)(value - 2);
                if (checkBox2.Checked == false)
                {
                    checkBox2.BackColor = Color.Red;
                    
                }
            } else { 
                if (checkBox2.Checked == true)
                {
                    checkBox2.BackColor = Color.Red;
                }
            }
            if (value > 0)
            {
                if (checkBox1.Checked == false)
                {
                    checkBox1.BackColor = Color.Red;
                    
                }
            } else { 
                if (checkBox1.Checked == true)
                {
                    checkBox1.BackColor = Color.Red;
                }
            }
            
        }

        private void btnWrite_Click(object sender, EventArgs e)
        {

            ushort value = 0;

            if (checkBox8.Checked == true)
            {
                value = 128;
            }
            if (checkBox7.Checked == true)
            {
                value = (ushort)(value + 64);
            }
            if (checkBox6.Checked == true)
            {
                value = (ushort)(value + 32);
            }
            if (checkBox5.Checked == true)
            {
                value = (ushort)(value + 16);
            }
            if (checkBox4.Checked == true)
            {
                value = (ushort)(value + 8);
            }
            if (checkBox3.Checked == true)
            {
                value = (ushort)(value + 4);
            }
            if (checkBox2.Checked == true)
            {
                value = (ushort)(value + 2);
            }
            if (checkBox1.Checked == true)
            {
                value = (ushort)(value + 1);
            }

            //Globals.WriteE3Reg(this.Address, value);


            //byte[] data = { 0x00, 0x00, 0x00, 0x00 }; ;

            byte data = (byte)value;

            //MessageBox.Show(data.ToString("X"));
            uint response = ACCLAIM_BLE_SERIAL.BLE_SPI.BLE_SPI_WRITE_8((byte)this.address, ref data);
            //if (response == 1)
            //{
            //    txtWriteResponse.Text = "Success: " + BitConverter.ToString(data);
            //    txtWriteData.Text = "";

            //}
            //else
            //{
            //    txtWriteResponse.Text = "Fail: ";
            //    //spi.CloseSerialPort();
            //    //spi.InitSerialPort("COM3");

            //};



        }

        private void E3_Register8_Load(object sender, EventArgs e)
        {
            this.lblBitName.Text = this.Name;
        }



    }
}