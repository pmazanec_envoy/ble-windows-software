﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ASIC_Test_Interface;

namespace ASIC_Test_Interface
{
    public partial class E3_Register16 : UserControl
    {
        Form Parent = new Form();
        public E3_Register16()
        {
            InitializeComponent();
        }
        public E3_Register16(Form inForm)
        {
            InitializeComponent();
            Parent = inForm;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            updateValue();
        }

        private void checkBoxHover1(object sender, EventArgs e)
        {
            lblBitName.Text = bit1_text;
        }
        private void checkBoxHover2(object sender, EventArgs e)
        {
            lblBitName.Text = bit2_text;
        }
        private void checkBoxHover3(object sender, EventArgs e)
        {
            lblBitName.Text = bit3_text;
        }
        private void checkBoxHover4(object sender, EventArgs e)
        {
            lblBitName.Text = bit4_text;
        }
        private void checkBoxHover5(object sender, EventArgs e)
        {
            lblBitName.Text = bit5_text;
        }
        private void checkBoxHover6(object sender, EventArgs e)
        {
            lblBitName.Text = bit6_text;
        }
        private void checkBoxHover7(object sender, EventArgs e)
        {
            lblBitName.Text = bit7_text;
        }
        private void checkBoxHover8(object sender, EventArgs e)
        {
            lblBitName.Text = bit8_text;
        }





        private void checkBoxLeave(object sender, EventArgs e)
        {
            lblBitName.Text = "";
        }



        private void updateValue()
        {
            int chk0 = 0;
            int chk1 = 0;
            int chk2 = 0;
            int chk3 = 0;
            int chk4 = 0;
            int chk5 = 0;
            int chk6 = 0;
            int chk7 = 0;
            ushort value = 0;



            if (checkBox1.Checked.Equals(true) )
            {
                chk0 = 1;
            };
            if (checkBox2.Checked.Equals(true))
            {
                chk1 = 2;
            };
            if (checkBox3.Checked.Equals(true))
            {
                chk2 = 4;
            };
            if (checkBox4.Checked.Equals(true))
            {
                chk3 = 8;
            };
            if (checkBox5.Checked.Equals(true))
            {
                chk4 = 16;
            };
            if (checkBox6.Checked.Equals(true))
            {
                chk5 = 32;
            };
            if (checkBox7.Checked.Equals(true))
            {
                chk6 = 64;
            };
            if (checkBox8.Checked.Equals(true))
            {
                chk7 = 128;
            };

            value = (ushort)(chk0 + chk1 + chk2 + chk3 + chk4 + chk5 + chk6 + chk7);

            if (value < 16)
            {
                lblValue.Text = "0x0" + value.ToString("X");
            }
            else
            {
                lblValue.Text = "0x" + value.ToString("X");
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            uint value = 0xFFFFFFFF;
            uint ftstatus = 99;

            //MessageBox.Show(Globals.FTDI_HANDLE.ToString("X"));

            ftstatus =  Globals.ReadFICIReg(this.Address, ref value);
            //ushort value = 0;
            //ReadE3Reg(0x00, ref test);
            MessageBox.Show(value.ToString("X"));
            
            
            
            
            
            //MessageBox.Show(ftstatus.ToString());
        }

        #region FTDI

        //FTDI Variables
        FTDI.FTDI ftdi = new FTDI.FTDI();
        private IntPtr FtdiHandle_ASIC = new IntPtr();
        private IntPtr FtdiHandle_PS = new IntPtr();
        private const uint FTC_SUCCESS = 0;
        private const uint FTC_DEVICE_IN_USE = 27;
        private const uint FTC_E3_MATCH = 0x99;
        private bool FTDI_Connected_ASIC = false;
        private bool FTDI_Connected_PS = false;



        private void findFTDI_ASIC()
        {
            String DLLVersion, DeviceName, DeviceType;
            uint ftStatus;

            Cursor = Cursors.WaitCursor;

            DLLVersion = "";
            DeviceName = "";
            DeviceType = "";
            ftdi.FTDI_Close(FtdiHandle_ASIC); //in case of a lockup
           ftStatus = ftdi.FTDI_Startup(ref DLLVersion, ref DeviceName, ref FtdiHandle_ASIC, ref DeviceType);
            //MessageBox.Show(FtdiHandle.ToString());
            if (ftStatus == FTC_SUCCESS)
            {
                FTDI_Connected_ASIC = true;

            }
            else
            {
                FTDI_Connected_ASIC = false;

            }
            Cursor = Cursors.Arrow;
        }

        private void qfindFTDI_ASIC()
        {
            String DLLVersion, DeviceName, DeviceType;
            uint ftStatus;

            Cursor = Cursors.WaitCursor;

            DLLVersion = "";
            DeviceName = "";
            DeviceType = "";
            ftStatus = ftdi.FTDI_QFind(ref DLLVersion, ref DeviceName, ref FtdiHandle_ASIC, ref DeviceType);
            if (ftStatus == FTC_SUCCESS)
            {
                FTDI_Connected_ASIC = true;

            }
            else
            {
                FTDI_Connected_ASIC = false;

            }
            Cursor = Cursors.Arrow;
        }
        #endregion

        #region Esteem_3_Comm

        //diag address
        const int E3_RdDataL = 0x00;
        const int E3_RdDataH = 0x01;
        const int E3_WrDataL = 0x02;
        const int E3_WrDataH = 0x03;
        const int E3_AddrL = 0x04;
        const int E3_AddrH = 0x05;
        const int E3_Stat = 0x06;

        const int ADDR_R_MASK = 0x10;
        const int ADDR_W_MASK = 0x20;
        //const int EEPROM_ADD_SPACE = 0x100;


        public uint ReadE3Reg(UInt16 addr, ref UInt16 data)
        {
            uint ftStatus = 99;
            Byte addrl, addrh, datal, datah, tmpb;
            bool pending = true;

            //if (!RdWrAll)
                //findE3(); //don't look for each read/write
            addrl = (Byte)addr;
            addrh = (Byte)(addr >> 8);
            addrh |= ADDR_R_MASK;


            //string test =Convert.FromBase64CharArray(addrl, 0, 4);
            //MessageBox.Show(test);


            ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_AddrL, addrl);
            if (ftStatus == FTC_SUCCESS)
                ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_AddrH, addrh);


            while ((ftStatus == FTC_SUCCESS) && pending)
            {
                //Thread.Sleep(100);
                tmpb = 1;
                ftStatus = ftdi.E3_read_diag(FtdiHandle_ASIC, (Byte)E3_Stat, ref tmpb);
                if (tmpb == 0x00)
                    pending = false;
            }



            //Response ready
            if (ftStatus == FTC_SUCCESS)
            {
                datal = 0;
                datah = 0;
                ftStatus = ftdi.E3_read_diag(FtdiHandle_ASIC, (Byte)E3_RdDataL, ref datal);
                ftStatus = ftdi.E3_read_diag(FtdiHandle_ASIC, (Byte)E3_RdDataH, ref datah);
                data = (UInt16)((UInt16)((UInt16)datah << 8) + (UInt16)datal);
            }
            else
            {
                data = 0;
                //reconnect();
            }

            //MessageBox.Show(data.ToString("X"));


            return ftStatus;
        }
        uint ReadE3EEPROM(UInt16 addr, ref UInt16 data)
        {
            uint ftStatus = 99;
            Byte addrl, addrh, datal, datah, tmpb;
            bool pending = true;

            //if (!RdWrAll)
           //     findE3(); //don't look for each read/write
            addrl = (Byte)addr;
            addrh = (Byte)(addr >> 8);
            addrh |= ADDR_R_MASK;



            ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_AddrL, addrl);
            if (ftStatus == FTC_SUCCESS)
                ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_AddrH, addrh);
            while ((ftStatus == FTC_SUCCESS) && pending)
            {
                tmpb = 1;
                ftStatus = ftdi.E3_read_diag(FtdiHandle_ASIC, (Byte)E3_Stat, ref tmpb);
                if (tmpb == 0x00)
                    pending = false;
            }
            //Response ready
            if (ftStatus == FTC_SUCCESS)
            {
                datal = 0;
                datah = 0;
                ftStatus = ftdi.E3_read_diag(FtdiHandle_ASIC, (Byte)E3_RdDataL, ref datal);
                ftStatus = ftdi.E3_read_diag(FtdiHandle_ASIC, (Byte)E3_RdDataH, ref datah);
                data = (UInt16)((UInt16)((UInt16)datah << 8) + (UInt16)datal);
            }
            else
            {
                data = 0;
                //reconnect();
            }
            return ftStatus;
        }
        uint WriteE3Reg(UInt16 addr, UInt16 data, ref UInt16 rtn)
        {
            uint ftStatus = 99;
            Byte addrl, addrh, datal, datah, tmpb;
            bool pending = true;

            //if (!RdWrAll)
             //   findE3(); //don't look for each read/write
            addrl = (Byte)addr;
            addrh = (Byte)(addr >> 8);
            addrh |= ADDR_W_MASK;
            datal = (Byte)data;
            datah = (Byte)(data >> 8);



            ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_WrDataL, datal);
            if (ftStatus == FTC_SUCCESS)
                ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_WrDataH, datah);
            if (ftStatus == FTC_SUCCESS)
                ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_AddrL, addrl);
            if (ftStatus == FTC_SUCCESS)
                ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_AddrH, addrh);
            while ((ftStatus == FTC_SUCCESS) && pending)
            {
                tmpb = 1;
                ftStatus = ftdi.E3_read_diag(FtdiHandle_ASIC, (Byte)E3_Stat, ref tmpb);
                if (tmpb == 0x00)
                    pending = false;
            }
            //Response ready
            if (ftStatus == FTC_SUCCESS)
            {
                ftStatus = ReadE3Reg(addr, ref rtn);
                if ((ftStatus == FTC_SUCCESS) && (rtn == data))
                    return ftStatus;
                else
                    return FTC_E3_MATCH;
            }
            else
            {
                rtn = 0;
                //reconnect();
            }
            return ftStatus;
        }
        uint WriteE3Reg(UInt16 addr, UInt16 data)
        {
            uint ftStatus = 99;
            Byte addrl, addrh, datal, datah, tmpb;
            bool pending = true;

            //if (!RdWrAll)
            //    findE3(); //don't look for each read/write
            addrl = (Byte)addr;
            addrh = (Byte)(addr >> 8);
            addrh |= ADDR_W_MASK;
            datal = (Byte)data;
            datah = (Byte)(data >> 8);

            //MessageBox.Show(addrh.ToString("X"));
            //MessageBox.Show(addrl.ToString("X"));
            //MessageBox.Show(datah.ToString("X"));
            //MessageBox.Show(datal.ToString("X"));

            ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_WrDataL, datal);
            if (ftStatus == FTC_SUCCESS)
                ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_WrDataH, datah);
            if (ftStatus == FTC_SUCCESS)
                ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_AddrL, addrl);
            if (ftStatus == FTC_SUCCESS)
                ftStatus = ftdi.E3_write_diag(FtdiHandle_ASIC, (Byte)E3_AddrH, addrh);
            while ((ftStatus == FTC_SUCCESS) && pending)
            {
                tmpb = 1;
                ftStatus = ftdi.E3_read_diag(FtdiHandle_ASIC, (Byte)E3_Stat, ref tmpb);
                if (tmpb == 0x00)
                    pending = false;
            }
            return ftStatus;
        }

        #endregion

        private void E3_Register16_Load(object sender, EventArgs e)
        {
            if (FTDI_Connected_ASIC == false)
            {
                //findFTDI_ASIC();
            }
        }





    }
}
