﻿namespace ASIC_Test_Interface
{
   partial class E3_Register32
      {
         public string[] bit_text = { "bit0",  "bit1",  "bit2",  "bit3",  "bit4",  "bit5",  "bit6",  "bit7",
                                      "bit8",  "bit9",  "bit10", "bit11", "bit12", "bit13", "bit14", "bit15",
                                      "bit16", "bit17", "bit18", "bit19", "bit20", "bit21", "bit22", "bit23",
                                      "bit24", "bit25", "bit26", "bit27", "bit28", "bit29", "bit30", "bit31"  };

         public string bit0
            {
               get   {
                        return bit_text[0];

                        //bit0 = bit0_text;
                        //return bit0;
                     }

               // Stores the selected value in the private variable colBColor, and
               // updates the backcolor of the label control lblDisplay.
               set   {
                        bit_text[0] = value;
                        lblBitName.Text = bit_text[0];
                     }
            }

         public string bit1
            {
               get   {
                        return bit_text[1];

                        //bit1 = bit1_text;
                        //return bit1;
                     }

               // Stores the selected value in the private variable colBColor, and
               // updates the backcolor of the label control lblDisplay.
               set   {
                        bit_text[1] = value;
                        lblBitName.Text = bit_text[1];
                     }
            }

         public string bit2
            {
               get   {
                        return bit_text[2];

                        //bit2 = bit2_text;
                        //return bit2;
                     }

               // Stores the selected value in the private variable colBColor, and
               // updates the backcolor of the label control lblDisplay.
               set   {
                        bit_text[2] = value;
                        lblBitName.Text = bit_text[2];
                     }
            }

         public string bit3
            {
               get   {
                        return bit_text[3];

                        //bit3 = bit3_text;
                        //return bit3;
                     }

               // Stores the selected value in the private variable colBColor, and
               // updates the backcolor of the label control lblDisplay.
               set   {
                        bit_text[3] = value;
                        lblBitName.Text = bit_text[3];
                     }
            }

         public string bit4
            {
               get   {
                        return bit_text[4];

                        //bit4 = bit4_text;
                        //return bit4;
                     }

               // Stores the selected value in the private variable colBColor, and
               // updates the backcolor of the label control lblDisplay.
               set   {
                        bit_text[4] = value;
                        lblBitName.Text = bit_text[4];
                     }
            }

         public string bit5
            {
               get   {
                        return bit_text[5];

                        //bit5 = bit5_text;
                        //return bit5;
                     }

               // Stores the selected value in the private variable colBColor, and
               // updates the backcolor of the label control lblDisplay.
               set   {
                        bit_text[5] = value;
                        lblBitName.Text = bit_text[5];
                     }
            }

         public string bit6
            {
               get   {
                        return bit_text[6];

                        //bit6 = bit6_text;
                        //return bit6;
                     }

               // Stores the selected value in the private variable colBColor, and
               // updates the backcolor of the label control lblDisplay.
               set   {
                        bit_text[6] = value;
                        lblBitName.Text = bit_text[6];
                     }
            }

         public string bit7
            {
               get   {
                        return bit_text[7];

                        //bit7 = bit7_text;
                        //return bit7;
                     }

               // Stores the selected value in the private variable colBColor, and
               // updates the backcolor of the label control lblDisplay.
               set   {
                        bit_text[7] = value;
                        lblBitName.Text = bit_text[7];
                     }
            }

         public string bit8
            {
               get   {
                        return bit_text[8];

                        //bit8 = bit8_text;
                        //return bit8;
                     }

               // Stores the selected value in the private variable colBColor, and
               // updates the backcolor of the label control lblDisplay.
               set   {
                        bit_text[8] = value;
                        lblBitName.Text = bit_text[8];
                     }
            }

         public string bit9
         {
             get
             {
                 return bit_text[9];

                 //bit9 = bit9_text;
                 //return bit9;
             }

             // Stores the selected value in the private variable colBColor, and
             // updates the backcolor of the label control lblDisplay.
             set
             {
                 bit_text[9] = value;
                 lblBitName.Text = bit_text[9];
             }
         }

         public string bit10
         {
             get
             {
                 return bit_text[10];

                 //bit10 = bit10_text;
                 //return bit10;
             }

             // Stores the selected value in the private variable colBColor, and
             // updates the backcolor of the label control lblDisplay.
             set
             {
                 bit_text[10] = value;
                 lblBitName.Text = bit_text[10];
             }
         }

         public string bit11
         {
             get
             {
                 return bit_text[11];

                 //bit11 = bit11_text;
                 //return bit11;
             }

             // Stores the selected value in the private variable colBColor, and
             // updates the backcolor of the label control lblDisplay.
             set
             {
                 bit_text[11] = value;
                 lblBitName.Text = bit_text[11];
             }
         }

         public string bit12
         {
             get
             {
                 return bit_text[12];

                 //bit12 = bit12_text;
                 //return bit12;
             }

             // Stores the selected value in the private variable colBColor, and
             // updates the backcolor of the label control lblDisplay.
             set
             {
                 bit_text[12] = value;
                 lblBitName.Text = bit_text[12];
             }
         }

         public string bit13
         {
             get
             {
                 return bit_text[13];

                 //bit13 = bit13_text;
                 //return bit13;
             }

             // Stores the selected value in the private variable colBColor, and
             // updates the backcolor of the label control lblDisplay.
             set
             {
                 bit_text[13] = value;
                 lblBitName.Text = bit_text[13];
             }
         }

         public string bit14
         {
             get
             {
                 return bit_text[14];

                 //bit14 = bit14_text;
                 //return bit14;
             }

             // Stores the selected value in the private variable colBColor, and
             // updates the backcolor of the label control lblDisplay.
             set
             {
                 bit_text[14] = value;
                 lblBitName.Text = bit_text[14];
             }
         }

         public string bit15
         {
             get
             {
                 return bit_text[15];

                 //bit15 = bit15_text;
                 //return bit15;
             }

             // Stores the selected value in the private variable colBColor, and
             // updates the backcolor of the label control lblDisplay.
             set
             {
                 bit_text[15] = value;
                 lblBitName.Text = bit_text[15];
             }
         }

         public string bit16
         {
             get
             {
                 return bit_text[16];

                 //bit16 = bit16_text;
                 //return bit16;
             }

             // Stores the selected value in the private variable colBColor, and
             // updates the backcolor of the label control lblDisplay.
             set
             {
                 bit_text[16] = value;
                 lblBitName.Text = bit_text[16];
             }
         }

         public string bit17
         {
             get
             {
                 return bit_text[17];

                 //bit17 = bit17_text;
                 //return bit17;
             }

             // Stores the selected value in the private variable colBColor, and
             // updates the backcolor of the label control lblDisplay.
             set
             {
                 bit_text[17] = value;
                 lblBitName.Text = bit_text[17];
             }
         }
       

         private string address;
         public  string Address
         // Retrieves the value of the private variable colBColor.
            {
                get  {
                        return address;
                     }

                // Stores the selected value in the private variable colBColor, and
                // updates the backcolor of the label control lblDisplay.
                set  {
                        address = value;
                        //lblAddress.Text = "0x" + address.ToString("X");
                        lblAddress.Text = "0x" + address;
                     }

            }

         private string regname;
         public string RegisterName
         // Retrieves the value of the private variable colBColor.
            {
                get  {
                        return regname;
                     }

                // Stores the selected value in the private variable colBColor, and
                // updates the backcolor of the label control lblDisplay.
                set  {
                        regname = value;
                        lblRegName.Text = regname;
                     }

            }



        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblBitName = new System.Windows.Forms.Label();
            this.lblValue = new System.Windows.Forms.Label();
            this.lblValue5 = new System.Windows.Forms.Label();
            this.btnRead = new System.Windows.Forms.Button();
            this.btnWrite = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox0 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.lblRegName = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(62, 2);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(77, 16);
            this.lblAddress.TabIndex = 0;
            this.lblAddress.Text = "0x00000000";
            // 
            // lblBitName
            // 
            this.lblBitName.AutoSize = true;
            this.lblBitName.Location = new System.Drawing.Point(201, 4);
            this.lblBitName.Name = "lblBitName";
            this.lblBitName.Size = new System.Drawing.Size(0, 13);
            this.lblBitName.TabIndex = 20;
            this.lblBitName.Visible = false;
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValue.Location = new System.Drawing.Point(2, 19);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(77, 16);
            this.lblValue.TabIndex = 21;
            this.lblValue.Text = "0x00000000";
            // 
            // lblValue5
            // 
            this.lblValue5.AutoSize = true;
            this.lblValue5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValue5.ForeColor = System.Drawing.Color.Red;
            this.lblValue5.Location = new System.Drawing.Point(423, 2);
            this.lblValue5.Name = "lblValue5";
            this.lblValue5.Size = new System.Drawing.Size(0, 16);
            this.lblValue5.TabIndex = 24;
            // 
            // btnRead
            // 
            this.btnRead.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRead.Location = new System.Drawing.Point(837, 10);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(21, 25);
            this.btnRead.TabIndex = 9;
            this.btnRead.Text = "R";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // btnWrite
            // 
            this.btnWrite.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWrite.Location = new System.Drawing.Point(861, 10);
            this.btnWrite.Name = "btnWrite";
            this.btnWrite.Size = new System.Drawing.Size(21, 25);
            this.btnWrite.TabIndex = 10;
            this.btnWrite.Text = "W";
            this.btnWrite.UseVisualStyleBackColor = true;
            this.btnWrite.Click += new System.EventHandler(this.btnWrite_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 16);
            this.label1.TabIndex = 22;
            this.label1.Text = "Address:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.lblRegName);
            this.panel1.Controls.Add(this.lblValue5);
            this.panel1.Controls.Add(this.lblValue);
            this.panel1.Controls.Add(this.checkBox25);
            this.panel1.Controls.Add(this.checkBox29);
            this.panel1.Controls.Add(this.checkBox26);
            this.panel1.Controls.Add(this.checkBox30);
            this.panel1.Controls.Add(this.checkBox27);
            this.panel1.Controls.Add(this.checkBox31);
            this.panel1.Controls.Add(this.checkBox28);
            this.panel1.Controls.Add(this.checkBox0);
            this.panel1.Controls.Add(this.checkBox17);
            this.panel1.Controls.Add(this.checkBox21);
            this.panel1.Controls.Add(this.checkBox18);
            this.panel1.Controls.Add(this.checkBox22);
            this.panel1.Controls.Add(this.checkBox19);
            this.panel1.Controls.Add(this.checkBox23);
            this.panel1.Controls.Add(this.checkBox20);
            this.panel1.Controls.Add(this.checkBox24);
            this.panel1.Controls.Add(this.checkBox9);
            this.panel1.Controls.Add(this.checkBox10);
            this.panel1.Controls.Add(this.checkBox11);
            this.panel1.Controls.Add(this.checkBox12);
            this.panel1.Controls.Add(this.checkBox13);
            this.panel1.Controls.Add(this.checkBox14);
            this.panel1.Controls.Add(this.checkBox15);
            this.panel1.Controls.Add(this.checkBox16);
            this.panel1.Controls.Add(this.btnWrite);
            this.panel1.Controls.Add(this.btnRead);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.checkBox5);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.checkBox6);
            this.panel1.Controls.Add(this.checkBox3);
            this.panel1.Controls.Add(this.checkBox7);
            this.panel1.Controls.Add(this.checkBox4);
            this.panel1.Controls.Add(this.checkBox8);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(899, 39);
            this.panel1.TabIndex = 23;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox25.Location = new System.Drawing.Point(236, 21);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(15, 14);
            this.checkBox25.TabIndex = 35;
            this.checkBox25.Tag = "25";
            this.checkBox25.UseVisualStyleBackColor = true;
            this.checkBox25.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox29.Location = new System.Drawing.Point(152, 21);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(15, 14);
            this.checkBox29.TabIndex = 42;
            this.checkBox29.Tag = "29";
            this.checkBox29.UseVisualStyleBackColor = true;
            this.checkBox29.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox26.Location = new System.Drawing.Point(215, 21);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(15, 14);
            this.checkBox26.TabIndex = 36;
            this.checkBox26.Tag = "26";
            this.checkBox26.UseVisualStyleBackColor = true;
            this.checkBox26.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox30.Location = new System.Drawing.Point(131, 21);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(15, 14);
            this.checkBox30.TabIndex = 41;
            this.checkBox30.Tag = "30";
            this.checkBox30.UseVisualStyleBackColor = true;
            this.checkBox30.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox27.Location = new System.Drawing.Point(194, 21);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(15, 14);
            this.checkBox27.TabIndex = 37;
            this.checkBox27.Tag = "27";
            this.checkBox27.UseVisualStyleBackColor = true;
            this.checkBox27.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox31.Location = new System.Drawing.Point(110, 21);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(15, 14);
            this.checkBox31.TabIndex = 40;
            this.checkBox31.Tag = "31";
            this.checkBox31.UseVisualStyleBackColor = true;
            this.checkBox31.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox28.Location = new System.Drawing.Point(173, 21);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(15, 14);
            this.checkBox28.TabIndex = 38;
            this.checkBox28.Tag = "28";
            this.checkBox28.UseVisualStyleBackColor = true;
            this.checkBox28.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox0
            // 
            this.checkBox0.AutoSize = true;
            this.checkBox0.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox0.Location = new System.Drawing.Point(804, 21);
            this.checkBox0.Name = "checkBox0";
            this.checkBox0.Size = new System.Drawing.Size(15, 14);
            this.checkBox0.TabIndex = 39;
            this.checkBox0.Tag = "0";
            this.checkBox0.UseVisualStyleBackColor = true;
            this.checkBox0.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            this.checkBox0.MouseLeave += new System.EventHandler(this.checkBoxLeave);
            this.checkBox0.MouseHover += new System.EventHandler(this.checkBoxHover);
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox17.Location = new System.Drawing.Point(416, 21);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(15, 14);
            this.checkBox17.TabIndex = 27;
            this.checkBox17.Tag = "17";
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox21.Location = new System.Drawing.Point(332, 21);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(15, 14);
            this.checkBox21.TabIndex = 34;
            this.checkBox21.Tag = "21";
            this.checkBox21.UseVisualStyleBackColor = true;
            this.checkBox21.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox18.Location = new System.Drawing.Point(395, 21);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(15, 14);
            this.checkBox18.TabIndex = 28;
            this.checkBox18.Tag = "18";
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox22.Location = new System.Drawing.Point(311, 21);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(15, 14);
            this.checkBox22.TabIndex = 33;
            this.checkBox22.Tag = "22";
            this.checkBox22.UseVisualStyleBackColor = true;
            this.checkBox22.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox19.Location = new System.Drawing.Point(374, 21);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(15, 14);
            this.checkBox19.TabIndex = 29;
            this.checkBox19.Tag = "19";
            this.checkBox19.UseVisualStyleBackColor = true;
            this.checkBox19.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox23.Location = new System.Drawing.Point(290, 21);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(15, 14);
            this.checkBox23.TabIndex = 32;
            this.checkBox23.Tag = "23";
            this.checkBox23.UseVisualStyleBackColor = true;
            this.checkBox23.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox20.Location = new System.Drawing.Point(353, 21);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(15, 14);
            this.checkBox20.TabIndex = 30;
            this.checkBox20.Tag = "20";
            this.checkBox20.UseVisualStyleBackColor = true;
            this.checkBox20.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox24.Location = new System.Drawing.Point(257, 21);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(15, 14);
            this.checkBox24.TabIndex = 31;
            this.checkBox24.Tag = "24";
            this.checkBox24.UseVisualStyleBackColor = true;
            this.checkBox24.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox9.Location = new System.Drawing.Point(602, 21);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 19;
            this.checkBox9.Tag = "9";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox10.Location = new System.Drawing.Point(581, 21);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 26;
            this.checkBox10.Tag = "10";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox11.Location = new System.Drawing.Point(560, 21);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 20;
            this.checkBox11.Tag = "11";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox12.Location = new System.Drawing.Point(539, 21);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 25;
            this.checkBox12.Tag = "12";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox13.Location = new System.Drawing.Point(518, 21);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(15, 14);
            this.checkBox13.TabIndex = 21;
            this.checkBox13.Tag = "13";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox14.Location = new System.Drawing.Point(497, 21);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(15, 14);
            this.checkBox14.TabIndex = 24;
            this.checkBox14.Tag = "14";
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox15.Location = new System.Drawing.Point(476, 21);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(15, 14);
            this.checkBox15.TabIndex = 22;
            this.checkBox15.Tag = "15";
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox16.Location = new System.Drawing.Point(437, 21);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(15, 14);
            this.checkBox16.TabIndex = 23;
            this.checkBox16.Tag = "16";
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox1.Location = new System.Drawing.Point(783, 21);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Tag = "1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            this.checkBox1.MouseLeave += new System.EventHandler(this.checkBoxLeave);
            this.checkBox1.MouseHover += new System.EventHandler(this.checkBoxHover);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox5.Location = new System.Drawing.Point(699, 21);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 18;
            this.checkBox5.Tag = "5";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            this.checkBox5.MouseLeave += new System.EventHandler(this.checkBoxLeave);
            this.checkBox5.MouseHover += new System.EventHandler(this.checkBoxHover);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox2.Location = new System.Drawing.Point(762, 21);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 12;
            this.checkBox2.Tag = "2";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            this.checkBox2.MouseLeave += new System.EventHandler(this.checkBoxLeave);
            this.checkBox2.MouseHover += new System.EventHandler(this.checkBoxHover);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox6.Location = new System.Drawing.Point(678, 21);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 17;
            this.checkBox6.Tag = "6";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            this.checkBox6.MouseLeave += new System.EventHandler(this.checkBoxLeave);
            this.checkBox6.MouseHover += new System.EventHandler(this.checkBoxHover);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox3.Location = new System.Drawing.Point(741, 21);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 13;
            this.checkBox3.Tag = "3";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            this.checkBox3.MouseLeave += new System.EventHandler(this.checkBoxLeave);
            this.checkBox3.MouseHover += new System.EventHandler(this.checkBoxHover);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox7.Location = new System.Drawing.Point(657, 21);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 16;
            this.checkBox7.Tag = "7";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            this.checkBox7.MouseLeave += new System.EventHandler(this.checkBoxLeave);
            this.checkBox7.MouseHover += new System.EventHandler(this.checkBoxHover);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox4.Location = new System.Drawing.Point(720, 21);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 14;
            this.checkBox4.Tag = "4";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            this.checkBox4.MouseLeave += new System.EventHandler(this.checkBoxLeave);
            this.checkBox4.MouseHover += new System.EventHandler(this.checkBoxHover);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.BackColor = System.Drawing.Color.DarkGray;
            this.checkBox8.Location = new System.Drawing.Point(623, 21);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 15;
            this.checkBox8.Tag = "8";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            this.checkBox8.MouseLeave += new System.EventHandler(this.checkBoxLeave);
            this.checkBox8.MouseHover += new System.EventHandler(this.checkBoxHover);
            // 
            // lblRegName
            // 
            this.lblRegName.AutoSize = true;
            this.lblRegName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblRegName.Location = new System.Drawing.Point(429, 1);
            this.lblRegName.Name = "lblRegName";
            this.lblRegName.Size = new System.Drawing.Size(0, 16);
            this.lblRegName.TabIndex = 24;
            // 
            // E3_Register32
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblBitName);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.panel1);
            this.Name = "E3_Register32";
            this.Size = new System.Drawing.Size(890, 38);
            this.Load += new System.EventHandler(this.E3_Register32_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblBitName;
        private System.Windows.Forms.Label lblValue;

        private System.Windows.Forms.Label lblValue5;

        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Button btnWrite;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;

        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox24;

        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox16;

        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;

        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox0;
        private System.Windows.Forms.Label lblRegName;
    }
}
